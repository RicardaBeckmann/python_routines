##########################################################################
# This file collections a number of utilities I frequently need when using
# yt.
#
#
#
#
##########################################################################
import numpy as np
import yt
from yt.fields.api import ValidateParameter
from yt import derived_field
import matplotlib as mpl
import matplotlib.pyplot as plt
import csv
import os
import re
from yt.data_objects.particle_unions import \
    ParticleUnion

from astropy import units as u
u.add_enabled_units([u.def_unit('dimensionless', u.dimensionless_unscaled)])

import yt
yt.set_log_level(50)


def rBondi(mass,c,ds):
    from yt.utilities.physical_constants import G
    G=link_to_ds(G,ds)
    radius=mass*G/c**2
    print("Rbondi",radius.in_units('pc'))
    return radius

def keplerian_velocity(mass,radius,ds):
    from yt.utilities.physical_constants import G
    print("radius",radius,'pc')
    radius=radius.in_units('cm')
    mass=mass.in_units('g')
    G=link_to_ds(G,ds)
    v=np.sqrt(2*G*mass/radius)
    print("Velocity",v.in_units('km/s'))
    return v

#############################
# TOPIC: SIMULATION PROPERTIES
# These functions calcualate
# a variety of simulation
# properties
#############################


# Calculate the ff time of a given halo contained in a box
def cooling_halo_fftime(ds):
    ad=ds.all_data()
    mass=ad['cell_mass'].sum()+ad['particle_mass'].sum()
    mass=mass/15*100
    radius=ds.length_unit/2.0
    t_ff=ff_time(mass=mass,radius=radius,ds=ds)
    t_unit=t_ff.in_units('Myr').d/1E3
    return t_unit

def ff_time(mass,radius,ds):
    from yt.utilities.physical_constants import G
    G=link_to_ds(G,ds)

    t_ff=np.pi/2*radius**(1.5)/(2*G*mass)**0.5
    return t_ff.in_units('s')

#Find the value at a given point, for when the inbuilt function doesn't work?
def find_value_at_point(myobject,point,field='density'):
    cell_size=myobject.ds.find_field_values_at_point('dx',point)
    region=myobject.ds.region(center=point,left_edge=point-cell_size/2,right_edge=point+cell_size/2,data_source=myobject,ds=myobject.ds)

    if not(len(region['dx'])==1):
        print("TOO MANY CELLS!")
        print(len(region['dx']))
        return
    else:
        return region[field]
    return

#Calculate volume of the refined region
def highres_volume(output,lvlmin=7):
    ds=load(output)
    ad=ds.all_data()
    vols=ad[('index','cell_volume')]
    print("Total number of cells:",len(vols))
    min_vol=((ds.length_unit/(2**lvlmin))**3)*1.0000001

    vols=vols[vols<min_vol]
    print("Number of refined cells:",len(vols))
    refined_vol=sum(np.array(vols.in_units('Mpc**3')))
    print("Total refined volume",refined_vol,"Mpc**3")
    return refined_vol


# Calculate density contrast in most refined cells
def density_contrast(output):
    ds=load(output)
    ad=ds.all_data()
    vol=ad[('index','cell_volume')]
    print("Loaded volumes")
    #dens=ad[('deposit','all_density')]
    dens=ad[('gas','density')]
    print("Loaded densities")
    print("There are",len(vol),"cells overall")
    max_lvl=dens[vol==vol.min()]
    print("There are",len(max_lvl),"cells in the highest level")
    print("Their density ranges from",max_lvl.min(),"to",max_lvl.max())
    print("The density contrast is",float(max_lvl.max())/np.average(max_lvl)-1)

# calculate size at z0
def size_z0(ds):
    size=ds.length_unit/ds.parameters['aexp']/3.08E24
    print("Size at z0: ",size," or ",np.array(size)*ds.hubble_constant," Mpc/h.")
    return size.in_units('pc')

def smallest_dx(first,last,file=False):
    plt.clf()
    filename='smallest_dx_{0}-{1}'.format(first,last)
    if file:
        with open(filename) as f:
            reader=csv.reader(f,delimiter='\t')
            z=[]
            dx=[]
            for row in reader:
                z.append(float(row[0]))
                dx.append(float(row[1]))
    else:
        ts=[load(out) for out in range(first,last+1)]
        dx=[float(ds.index.get_smallest_dx().in_units('pc')) for ds in ts]
        z=[ds.current_redshift for ds in ts]
        filename='smallest_dx_{0}-{1}'.format(first,last)
        with  open(filename,'w+') as f:
            writer = csv.writer(f, delimiter='\t')
            writer.writerows(zip(z,dx))

    plt.plot(z,dx,color='g')
    plt.xlabel('redshift')
    plt.ylabel('size of smallest leaf cells [pc]')
    plt.gca().invert_xaxis()
    plt.savefig('smallest_dx_{0}-{1}'.format(first,last))


# calculate jeans line for phase plots
def jeans(ds,Ts,high_ref=2,njeans=8):
    from yt.utilities.physical_constants import clight,G,mp,kb

    kb=kb.in_units("g*cm**2/s**2/K")
    box=ds.length_unit/ds.parameters['aexp']
    print("Box size: ",box.in_units('Mpc'))
    L_J=njeans*box*0.5**(ds.max_level+ds.min_level+1-high_ref)
    print("Jeans length: ",L_J.in_units('pc'),"at level ", \
       ds.max_level+ds.min_level+1-high_ref, "and ",njeans,"cells per jeans lenght.")

    C=2*np.pi*G*mp*L_J**2/(5*ds.gamma*kb)
    rho=Ts/C
    return rho,Ts

# Plot star formation history for all stars within a sphere
def get_sfr(sphere,bins=''):

    star_birth_times = sphere[("star", "particle_birth_time")].in_units('Gyr')
    star_masses = sphere[("star", "particle_mass")].in_units('Msun')

    # Convert birth times to age or time format if necessary (depends on your dataset's time unit)
    # Assume birth times are given in code units

    # Define time bins (you might need to convert these units)
    if bins=='':
        bins = np.linspace(star_birth_times.min(), star_birth_times.max(), 500)

    # Compute star formation rate as mass formed per time bin
    sfr, bin_edges = np.histogram(star_birth_times, bins=bins, weights=star_masses)
    time_centers = 0.5 * (bin_edges[1:] + bin_edges[:-1])
    bin_widths = np.diff(bin_edges)

    # Star formation rate in solar masses per year (SFR unit depends on your data)
    sfr_rate = sfr / (bin_widths*1E9)
    return time_centers,sfr_rate



#############################
# TOPIC: OUTPUTS
############################

#Find outputs at a given time or redshift
def find_output_at_redshift(redshift,guess=20,folder='.',silent=False):
    outputs=find_outputs(folder=folder)
    z=100
    i=0
    output=outputs.min()
    while z>redshift and output<outputs.max():
        output=outputs[i]
        ds=load(output,folder=folder,silent=silent)
        z=ds.current_redshift
        i+=1
    return output

def find_output_at_time(time,guess=1,folder='.',time_unit='Myr'):
    outputs=find_outputs(folder=folder)
    t=0
    output=outputs.min()
    i=0
    while t<time and output<outputs.max():
        output=outputs[i]
        ds=load(output,folder=folder,silent=True)
        if type(time_unit)==str:
            t=ds.current_time.in_units(time_unit)
        else:
            t=ds.curent_time.in_units('code_time')/time_unit
        i+=1
    return output

# Returns a list of all outputs
def find_outputs(folder='.'):
    outputs=np.array([int(name[-5:]) for name in os.listdir(folder) if name[:8]=='output_0'])
    outputs.sort()
    return outputs

def find_last_output(folder):
    return find_outputs(folder=folder)[-1]

# Finds first or last output
def find_last_ds(folder='.',verbose=True):
    try:
        ds=load(find_outputs(folder=folder)[-1],folder,silent=not(verbose))
        if verbose:
            print("Last output loaded")
    except:
        try:
            ds=load(find_outputs(folder=folder)[-1],folder,silent=not(verbose))
            if verbose:
                print("Second to last output loaded")
        except:
            print("Could not load the last outputs:",find_outputs(folder=folder))
    return ds

def find_first_ds(folder='.',silent=False):
    ds=load(find_outputs(folder=folder)[0],folder,silent=silent)
    return ds

#Takes a unit or quantity, such as G, and links it to a dataset so code_units can be used
def link_to_ds(data,ds):
    data=data.in_cgs()
    try:
        data_new=ds.array(data.value,data.units)
    except:
        data_new=ds.quan(data.value,data.units)
    return data_new

#Loads a dataset
def load(output,folder='.',silent=False,bbox=None):
    # Load and add filters

    try:
        ds=yt.load(info_file(output,folder),bbox=bbox)
        if not(silent):
            print("Opening",output,"in",folder)
    except:
        ds=None
        if not(silent):
            print(output,"in",folder,'not found')

    return ds

################################
# TOPIC: ADD CUSTOM FIELDS AND FILTERS
###############################

def add_non_tracers(ds):
    u = ParticleUnion("non_tracers", ['io','sink'])
    ds.add_particle_union(u)
    return ds


########################
# TOPIC: UNITS
#######################

# set plot units
class Units(object):
    def __init__(self):
        unit={
            "mass":"Msun",
            "radius":"kpc",
            "temperature":"K",
            "density":"amu/cm**3",
            "surf_dens":"amu/cm**2",
            "stars_density":"Msun/cm**3",
            "velocity_magnitude":"km/s",
            "cell_mass":"Msun",
            "speed":"cm/s",
            "pressure":"dyne/cm**2",
            "number":None,
            "magnitude":"cm/s",
            "metallicity":"Zsun",
            "velocity":"km/s",
            "absolute":"km/s",
	    "magnetic":"gauss",
	    'radial':'km/s',
	    'ratio':'dimensionless',
            'cool':'Gyr'
            }

        for item in unit.keys():
            setattr(self,item,unit[item])


###########################
# TOPIC: NAMES AND NUMEBERS
###########################

    # Returns number of output from ds name
    # -------------------------------------------------------
def get_number(ds):
    return int(str(ds)[-5:])

    # point from directory of run to the info file of an output
    # -------------------------------------------------------
def info_file(output,folder='.'):
    return "{1}/output_{0}/info_{0}.txt".format(str(output).zfill(5),folder)

    # point from directory of run to directory containing the HaloCatalog
    # -------------------------------------------------------
def halo_path(output,add=None):
    return addendum("halos/{0}".format(output),add)

    # point from directory of run to HaloCatalog file
    # -------------------------------------------------------
def halo_file(output,add=None):
    return "{0}/{1}.0.h5".format(halo_path(output,add),output)

    # adds addendum to file name
    # -------------------------------------------------------
def addendum(plotname,add=None,pdf=False):
    if add:
        plotname="{0}_{1}".format(plotname,add)
    if pdf:
        plotname+='.pdf'
    else:
        plotname+='.png'
    return plotname

###################################
# TOPIC: DEFINE CUSTOM FIELDS AND FILTERS
###################################

###################################
# TOPIC: RAMSES output files
###################################

def plot_runtime(folder):
    tag='slurm'
    runs=[name for name in os.listdir(folder) if name[:len(tag)]==tag]
    if len(runs)==0:
        tag='irene'
        runs=[name for name in os.listdir(folder) if name[:len(tag)]==tag]
    if len(runs)==0:
        print("No output file found:",folder)
    [runs.remove(r) for r in runs if r[-2:]=='.e']
    order=np.array([re.sub('[^0-9]','', n) for n in runs]).astype('int')
    runs=[r for _, r in sorted(zip(order,runs), key=lambda pair: pair[0])]
    wall_times=[]
    code_times=[]
    main=1
    for run in runs:
        print(run)
        use_next=False
        with(open(folder+'/'+run)) as f:
            reader=csv.reader(f)
            for row in reader:
                try:
                    if use_next:
                        if(row[0].find('Fine',1,5)>0):
                            if main<len(code_times):
                                code_times[main]=float(row[0].split()[4])
                            else:
                                code_times.append(float(row[0].split()[4]))
                            use_next=False
                    if(row[0].find('Main',1,5)>0):
                        #Identify Main steps, but t is recorded in next fine timestep
                        main=int(row[0].split()[2])
                        use_next=True
                    elif(row[0].find('coarse',25,31))>0:
                        if main<len(wall_times):
                            wall_times[main]=float(row[0].split()[6])
                        else:
                            wall_times.append(float(row[0].split()[6]))
                except:
                    continue
    ds=find_first_ds(folder)

    code_times=np.array(code_times)*ds.time_unit.in_units('Myr').v  #in Myr
    wall_times=np.cumsum(np.array(wall_times))/60/60 #in hours
    print('--------')
    if len(code_times)>len(wall_times):
        return wall_times,code_times[:len(wall_times)]
    elif len(code_times)<len(wall_times):
        return wall_times[:len(code_times)],code_times
    else:
        return wall_times,code_times
