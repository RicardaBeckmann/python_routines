# Contains functiones needed in the iAnitial conditions and halo profiles notebook

#Import modules
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.lines as mlines
import csv
from utils import *

#Constants
#General model parameters
delta_vir=200
Omega_m=0.29
Omega_l=0.71
Omega_k=1-Omega_m-Omega_l    #Assuming flatness
f_gas=0.15
h=0.7  #
mu=0.5882359400399545

#Constants, all in cgs
G=6.67e-8    #cm3 / g / s^2
mp=1.6737352238051868e-24
kb=1.3806488e-16
kpc=3.08e21
Msun=1.99e33
Myr=60*60*24*365*1e6
keV=8.61732814974056E-08

#Slope of density falloff outside profile
alpha=5

#Other predefined quantities
colours=['#d73027','#fc8d59','#fee090','#91bfdb','#4575b4']

def rho_crit(z=0):
    '''In g/cm**3'''
    #dens=1.8788E-26*h**2*1E3/1E6*(1+z)**(3.0/2) # Assuming the univese is flat
    dens=9.2585e-30*(1+z)**(3.0/2) # Assuming the univese is flat, based on RAMSES value
    #dens=5.544E-06*mp  # from DICE for a flat cosmology at z=0  !
    return dens

#Mass and density profiles
# Mass and density profiles

#For all functions:
# - c: [1] concentration parameter, dimensionless. Can be an array but usually a float
# - v: [km/s] v200,
# - radii: [r_s], default 1. Can be an int,float,array or list. Converted to list for use in formula
# - rcore: [r_s], default 0

def f(x): #Commonly needed function
    '''From VdB, slide 8
    !!! THE SLIDESHOW HAS A SIGN ERROR IN IT!!!
    http://www.astro.yale.edu/vdbosch/jerusalem_lecture3.pdf
    '''
    return np.log(1+x)-x/(1+x)
    #return np.log(1+x)+x/(1+x)

def v_vir(m_vir,delta_vir=200,z=0):  #input in Msun, output in km/s
    z=0
    v=np.sqrt(G*m_vir*Msun/r_vir_fromm(m_vir,delta_vir=delta_vir,z=z)/kpc)/1E5
    return v

def m_vir(v,z=0,delta_vir=200):   #in Msun
    '''Calculates the Virial mass in [Msun], using V_vir=sqrt(GMvir/rvir) and Delta*rho_crit=3*Mvir/4*pi*r_vir^3
    from the virial theorem, and common sense
    '''
    mass=(v*1E5)**3/G**(3/2)*(3/4/np.pi/delta_vir/rho_crit(z=z))**0.5
    return np.array(mass/Msun)

def T_vir(v):
    '''Calculate the virial temperature in [K], based on http://www.astro.yale.edu/vdbosch/astro610_lecture15.pdf'''
    temp=3.6E5 * (v/100)**2
    return temp


def m_nfw(c,radii=[1],rcore=0,rcut=None):   #In units of Mvir
    ''' Cumulative mass profile with radius, in units of [Mvir], VdB slide 8,
    http://www.astro.yale.edu/vdbosch/jerusalem_lecture3.pdf
    '''
    rs=1    #Given the units chosen here
    c=np.array(c)

    radii_loc=np.array(radii)
    if rcore:
        mass=1/((rcore-rs)**2*(radii+rs)*f(c)) * (radii*(rcore-rs)*rs + (radii+rs)*(-rcore**2*np.log(rcore)+rcore**2*np.log(radii+rcore)+rs*(-2*rcore+rs)*(-np.log(rs)+np.log(radii+rs))))
    else:
        mass=f(radii)/f(c)  #(np.log(1+radii)-radii/(1+radii))/(np.log(1+c)-c/(1+c))
    if rcut:
        try:
            edge_mass=mass[np.argmin(np.abs(radii-rcut))]
            mass[radii>rcut]=edge_mass
        except:
            #catch cases when radii is only a number, such as for integration
            if radii>rcut:
                mass=m_nfw(c=c,radii=rcut,rcore=rcore,rcut=None)
    return mass

def m_halo(c,v,radii=[1],rcore=0,rcut=0,z=0,delta_vir=200,gentle=True): #In units of Msun
    '''Cumulative mass profile of the halo in Msun, with radius, in [Msun]'''
    if gentle:
        r_mask=np.ones(len(radii))
        if rcut:
            r_mask[r_mask>rcut]=0
        return m_vir(v,z=z,delta_vir=delta_vir)*m_nfw(c,radii,rcore=rcore,rcut=rcut)*r_mask+mass_outside(c,v,radii,rcore,rcut,delta_vir,z)
    else:
        return m_vir(v,z=z,delta_vir=delta_vir)*m_nfw(c,radii,rcore=rcore,rcut=rcut)

def mass_outside(c,v,radii,rcore,rcut,delta_vir=200,z=0):
    '''Currently inputs in kpc and output in Msun'''

    if rcut:
        radii_loc=radii.copy()
        rs=r_vir_fromv(v=v) # in kpc, for conversion 
        radii_loc[radii<=rcut]=rcut
        rcut=rcut*rs
        radii_loc=rs*radii_loc
        rho_cut=rho(c,np.array([rcut]),rcore,rcut,f_gas)*rho_crit(z=z)
        return 4 * np.pi/(alpha-3)*rho_cut*rcut**alpha*(rcut**(3-alpha)-radii_loc**(3-alpha))/Msun*kpc**3
    else:
        return 0

def r_vir_fromv(v,z=0,delta_vir=200): #in kpc, this is rDelta_vir, i.e if delta_vir=200 then r_vir = r200
    '''Virial radius for a given virial velocity, following DICE
    '''
    m=m_vir(v=v,z=z,delta_vir=delta_vir)
    r=r_vir_fromm(m=m,delta_vir=delta_vir,z=z)
    return r

def r_vir_fromm(m,delta_vir=200,z=0):
    '''Virial radius for a given virial mass, VdB slide 4, in [kpc]
    http://www.astro.yale.edu/vdbosch/jerusalem_lecture3.pdf
    measured at over times the overdensity. Default 200.
    '''
    
    r1=(3*m*Msun/(rho_crit(z=z)*delta_vir*4*np.pi))**(1.0/3)/kpc
    return r1

def rho_ave(c,radii,delta_vir=200):
    '''Calculate the average overdensity within a given radius,
    In units of rho_crit'''
    radii=np.array(radii)
    return f(radii)/f(c)*delta_vir*(c/radii)**3

def r_new(c,radii,delta_vir,delta_new):
    '''Find new radius when changing delta_vir
    Needed by the halo mass converter'''
    radii=np.array(radii)
    rho=rho_ave(c,radii,delta_vir)
    #ind=np.where(np.diff(np.sign(rho)))
    #return radii[ind[0][1]]
    return radii[rho>delta_new][-1]


def halo_mass_converter(Mvir_in,dvir_in,dvir_out,c_in,plot=True):
    '''Converts M200 into M500 or similar'''
    radii=np.linspace(0,1.5*c_in,100000)

    rs=r_vir_fromm(Mvir_in,delta_vir=dvir_in)/c_in
    #print('rvir_in is ',rs*c_in,"at an overdensity of",dvir_in)
    #print("Mvir_in is",sci(Mvir_in),'Msun')

    rvir_out=r_new(c_in,radii,delta_vir=dvir_in,delta_new=dvir_out) #this is c_out
    Mvir_out=m_nfw(c_in,np.array([rvir_out]))*Mvir_in

    if plot:
        plt.plot(radii*rs,m_nfw(c_in,radii)*Mvir_in)
        plt.xlabel('radius [kpc]')
        plt.ylabel('mass contained')
        plt.axhline(Mvir_in,color='k',label=r'$\Delta_{{in}} =  {0}$'.format(dvir_in))
        plt.axvline(rs*c_in,color='k')

        rvir_out=r_new(c_in,radii,delta_vir=dvir_in,delta_new=dvir_out) #this is c_out
        Mvir_out=m_nfw(c_in,np.array([rvir_out]))*Mvir_in
        plt.axhline(Mvir_out,label=r'$\Delta_{{out}} =  {0}$'.format(dvir_out),color='r')
        plt.axvline(rvir_out*rs,color='r')
        plt.legend()

    #print("Mass ratio of in to out",Mvir_out/Mvir_in)
    return float(Mvir_out),rvir_out


def r_s(c,v,z=0,delta_vir=200):  #in kpc
    '''Scale radius, calculated from the virial mass and c, in [kpc]'''
    r=r_vir_fromv(v=v,z=z,delta_vir=delta_vir)/c
    return r

def rho(c,radii,rcore=0,rcut=None,dens_infty=1E-3,delta_vir=200,gentle=True): #In units of rho_crit
    '''Density profile in units of r_crit, in [r_{crit}]'''
    delta_char=(delta_vir/3)*(c**3/f(c))
    dens=delta_char/((radii+rcore)*(1+radii)**2)
    radii_int=radii.copy()

    if rcut:
        if gentle:
            radii_int[radii<=rcut]=rcut
            dens=dens*(rcut/radii_int)**alpha
        else:
            dens=rho*np.exp(-(radii/rcut)**50)+dens_infty
    return dens

def rho_dm(c,radii,rcore=0,rcut=None,dens_infty=1E-3,z=0,f_gas=0.15,gentle=True):  #in gm/cm**3 (same as rho_crit)
    ''' DM density profile in units of g/cm**3'''
    return rho(c=c,radii=radii,rcore=rcore,rcut=rcut,dens_infty=dens_infty,gentle=gentle)*rho_crit(z=z)*(1-f_gas)

def rho_gas(c,radii,rcore=0,rcut=None,dens_infty=1E-3,z=0,f_gas=0.15,gentle=True): #in gm/cm**3 (same as rho_crit)
    ''' Gas density profile in units of g/cm**3'''
    return rho(c=c,radii=radii,rcore=rcore,rcut=rcut,dens_infty=dens_infty,gentle=gentle)*f_gas*rho_crit(z=z)

#Temperature profiles from the NFW density profiles
def dP_dr(radii,c=1,rcore=0,rcut=0,dens_infty=1E-3,z=0,Mbh=0,gentle=True): #Dimensionless, units set in function P below
    return (m_nfw(c,radii,rcut=rcut,rcore=rcore)+Mbh)*rho(c=c,radii=np.array([radii]),rcore=rcore,rcut=rcut,dens_infty=dens_infty,gentle=gentle)/(radii)**2

def P(c,v,radii,rcore=0,rcut=0,dens_infty=1E-3,z=0,Mbh=0,f_gas=0.15,gentle=True):  # in [cgs]
    ''' Pressure at a given radius, in [erg/cm**3]
    This functions assumes spin = 0 !!
    Assumes delta_vir=200!!'''
    try:
        radii[0]
    except:
        radii=[radii]
    mvir=m_vir(v=v)
    press=np.array([integrate.romberg(dP_dr,r,c*2,args=(c,rcore,rcut,dens_infty,z,Mbh/mvir,gentle)) for r in radii])*G*Msun*rho_crit(z=z)*f_gas/(r_s(c,v,z=z,delta_vir=200)*kpc)*mvir #Only one unit of r as the other one is cancelled by dreload
    return press.ravel()

def T(press,c,radii,rcore=0,rcut=0,dens_infty=1E-3,z=0,f_gas=0.15,gentle=True): #in [K]
    '''Temperature at a given radius, based on the pressure and density profiles'''
    temp=press*mp/(rho_gas(c,radii=radii,rcore=rcore,rcut=rcut,dens_infty=dens_infty,z=z,f_gas=f_gas,gentle=gentle)*kb)*mu
    return temp

#Timescales
def t_ff(c,v,radii,rcore=0,Mbh=0,z=0,delta_vir=200,rcut=None,gentle=True):   # in [Myr]
    '''Free fall time, according to McCourt2011'''
    radii_phys=radii*r_s(c,v,z=z,delta_vir=delta_vir)*kpc     #now in cm
    time=np.sqrt(2*radii_phys**3/(G*(m_halo(c,v,radii,rcore=rcore,z=z,delta_vir=delta_vir,gentle=gentle,rcut=rcut)+Mbh)*Msun))    #in s
    time=time/Myr
    return time

def t_cool(press=0,c=0,radii=0,rcore=0,temp=None,dens=None,dens_infty=1E-3,rcut=None,ff=False,z=0,f_gas=f_gas,gentle=True): # in [Myr]
    '''Cooling time, according to McCourt2011, assuming free-free emission
    P: [cgs units]'''
    #Load data for the NFW case if not explicitly given

    #print('press',press)
    if temp==None:
        #print("Calling exception T")
        temp=T(press=press,c=c,radii=radii,rcore=rcore,rcut=rcut,dens_infty=dens_infty,z=z,f_gas=f_gas,gentle=gentle)  #in K
    try:
        temp[0]
    except:
        print("Temperature must be numpy array")
        temp=[temp]
    temp=np.array(temp)
    #print('T',temp)
    try:
        dens[0]
    except:
        #print("Calling exception dens")
        dens=rho_gas(c=c,radii=radii,rcore=rcore,dens_infty=dens_infty,rcut=rcut,z=z,f_gas=f_gas,gentle=gentle)
    #print('rho',dens)
    Lambda=Lambda_cool(temp)
    time=3/2*kb*temp*mu/((dens/mu/mp)*Lambda)/2  # in s  => why the factor of 2? I have no idea but it matches if I add it ...
    time=time/Myr
    return time

def Lambda_cool(temp,ff=False): # in Kelvin
    temp=np.array(temp)
    keV=8.617332401096504e-08 
    if ff:
        Lambda=2.5E-23*(temp/1E8)**0.5 #*(dens/mp/mu)**2  #in erg/s cm**-3,depends only on T
    else:
        temp=temp*keV
        Lambda=8E-24*(0.086*temp**(-1.7)+0.58*temp**0.5+0.63)
    return Lambda

def t_ratio(press,c,v,radii,rcore=0,dens=None,temp=None,rcut=None,dens_infty=1E-3,z=0,Mbh=0,f_gas=0.15,gentle=True):
    '''Time ratio relevant for the thermal instability'''
    tc=t_cool(press=press,dens=dens,temp=temp,c=c,radii=radii,rcore=rcore,rcut=rcut,dens_infty=dens_infty,z=z,f_gas=f_gas,gentle=gentle)
    tff=t_ff(c=c,v=v,radii=radii,rcore=rcore,z=z,Mbh=Mbh,gentle=gentle,rcut=rcut)
    return tc/tff

#Load data files
#Load in data from the RAMSES output
def load_ramses(filename,mu=0.6,folder='data'):
    with open('{1}/{0}.csv'.format(filename,folder)) as ram_files:
        ram_reader=csv.reader(ram_files)
        next(ram_reader)
        time=next(ram_reader)[0]
        time=float(time.split()[3])
        ramses_profiles={}
        for row in ram_reader:
            name=row[0].split(' ')[1]
            ramses_profiles[name]=np.array([float(item) for item in row[1:]])

    if 'temperature' in ramses_profiles.keys():
        ramses_profiles['temperature']*=1/mu
    return ramses_profiles,time


#Concentration parameter correlation
def concentration(m100=None):
    '''Calculates the concentration parameter for a given virial mass using Maccio2007, Eq9 for relaxed halos'''
    '''https://arxiv.org/abs/astro-ph/0608157'''
    ''' CAREFUL: BASED ON m100!!!! '''
    #if v:
    #    c=-0.098*(np.log10(m_vir(v,z=z))-12)+1.071
    c=-0.098*(np.log10(m100/1E12))+1.071
    return 10**c

def T_nfw(radii,c=0,v=0,rcore=0,z=0,Mbh=0,gentle=True):
    press_core=P(c=c,v=v,radii=radii,rcore=rcore,rcut=None,z=z,Mbh=Mbh,gentle=gentle)
    temp=T(press=press_core,c=c,radii=radii,rcore=rcore,rcut=None,z=z,gentle=gentle)
    return np.array(temp)


def dP_dr_fromT(radii,c=1,v=1,rcore=0,rcut=None,z=0,Mbh=0,gentle=True): #Dimensionless, units set in function P below

    temp=np.array(T_nfw(c=c,radii=radii,rcore=rcore,v=v,z=z,gentle=gentle))
    dp=(m_halo(c=c,v=v,radii=radii,rcore=rcore,rcut=None,z=z,gentle=gentle)+Mbh)/temp/(radii)**2
    dp=-1*dp*G*Msun*mp*mu/kb/(r_s(c=c,v=v,z=z)*kpc)   #Put units in here to keep numbers manageable
    try:
        return float(dp)
    except:
        print("dp",dp)
        return dp

def Pandrho_fromT(c,v,radii,rcore=0,rcut=None,rmax=100,z=0,gentle=True):  # in [cgs]
    ''' Pressure at a given radius, in [erg/cm**3]
    This functions assumes spin = 0 !!'''

    press_infty=P(c=c,v=v,radii=rmax,rcore=0,rcut=None,z=z,Mbh=0,gentle=gentle)
    try:
        radii[0]
    except:
        radii=[radii]

        
    temp=T_nfw(c=c,radii=radii,rcore=rcore,v=v,z=z,gentle=gentle)
    press=-1*np.array([integrate.romberg(dP_dr_fromT,r,rmax,args=(c,v,rcore,rcut,z,gentle)) for r in radii])
    press=np.exp(press)*press_infty   #Use normalisation from NFW pressure profiles at large radii
    dens=np.divide(press,temp)*mp*mu/kb
    return press,dens

    return entropy

def broken_power_law(radii,slope1=2.0/3,slope2=1.0,r_transition=13,v=1,normalisation=None):
    try:
        normalisation[0]
    except:
        print("normalisation is one")
        normalisation=np.ones(len(radii))
    lower=(radii/r_transition)**slope1
    upper=(radii/r_transition)**slope2
    transition_index=np.argmin(np.abs(radii-r_transition))
    entropy=upper
    entropy[:transition_index]=lower[:transition_index]

    #Normalise entropy to K200
    entropy=entropy/entropy[transition_index]*normalisation[transition_index]
    return entropy
