#!/bin/bash

dir='zoom_sorted'
mkdir -p  $dir

zooms=$(ls *_density_*.png | grep -o '[[:digit:]]*pc' | sort -u)

for zoom in $zooms;do
    mkdir -p $dir/$zoom
    mv *$zoom*.png $dir/$zoom
done
