# This file collects routines that can be used with ADAPTAHOP and the resulting catalogues
from astropy.io import ascii
import os,csv
import ytutils
import numpy as np
import matplotlib.pyplot as plt
from astropy import table

#################################################
# Deal with time, output and redshift efficiently
#################################################

def write_output_to_time(df):
    outputs=[o for o in os.listdir(df) if o[:6]=='output']
    outputs.sort()

    filename=df+'/ADAPTAclump_maps/output_step_redshift.txt'
    try:
        times=output_step_redshift(df)
        existing_outputs=times['output']
    except:
        with open(filename,'w') as f:
            writer=csv.writer(f)
            writer.writerow(['output','nstep_coarse','aexp','z','time'])
            existing_outputs=[]
    for o in outputs:
        out=int(o.split('_')[-1])
        if not out in existing_outputs:
            infofile=df+f"/{o}/info_{o.split('_')[-1]}.txt"
            with open(infofile) as info:
                i=info.read().split('\n')
                nstep=int(i[5].split('=')[-1])
                aexp=float(i[9].split('=')[-1])
                ds=ytutils.load(out,df)
            with open(filename,'a') as f:
                writer=csv.writer(f)
                writer.writerow([out,nstep,aexp,float(ds.current_redshift),float(ds.current_time.in_units('Gyr').value)])
    return

# Convert between different time units
class time(object):
    def __init__(self,df,verbose=False):
        filename=df+'/ADAPTAclump_maps/output_step_redshift.txt'
        if verbose:
            print("Opening",filename)
        try:
            data=ascii.read(filename)
        except:
            a.write_output_to_time(df)
            data=ascii.read(filename)
        self.data=data
    def convert(self,from_field,to_field,points):
        try:
            return np.array([self.data[to_field][self.data[from_field]==p][0] for p in points])
        except:
            return self.data[to_field][self.data[from_field]==points][0]

#################################################
# Deal with output files from reff.f90
#################################################
# Load the galaxy list in files produces by reff.f90
def load_gal_list(output,df):
    filename=df+f'/ADAPTAclump_maps/2_catalogue_ADAPTA_reff/list_reffgal_{str(output).zfill(5)}.dat.txt'
    print(filename)
    gals=ascii.read(filename,names=['id','level','mass','x','y','z','r50'])
    gals.sort('mass')
    gals.reverse()
    return gals


# Plot halo radii on yt plots
def annotate_substructure(myplot,gals):
    ds=myplot.ds
    for gal in gals:
        if gal['level']==1:
            color='w'
        else:
            color='k'
        c=ds.arr(list(gal['x','y','z']),'code_length')
        r=ds.quan(gal['r50'],'code_length')
        myplot.annotate_sphere(c,r,circle_args={'color':color})
    return myplot

#################################################
# Load datasets
#################################################

def get_gal_center(output,df):
    try:
        gals=load_gal_list(output,df)
        return np.array(list(gals[0]['x','y','z'])),float(np.array(gals[0]['r50']))
    except:
        print("output",output,"has no preloaded center yet")


# Def load dataset
def get_galaxy(output,df,gal_id=1,scale=5):
    ds=ytutils.load(output,df)
    gals=load_gal_list(output,df)

    
    print("Loading",gal_id,"at output",output)
    center,radius=np.array(list(gals[gals['id']==gal_id]['x','y','z'][0])),float(np.array(gals[gals['id']==gal_id]['r50'][0]))
    print(output,"r50",ds.quan(radius,'code_length').in_units('kpc'))
    gal_box=ds.quan(radius,'code_length')*scale*4
    center=ds.arr(center,'code_length')
    bbox=[list((center-gal_box).v),list((center+gal_box).v)]
    ds=ytutils.load(output,df,bbox=bbox)
    center=ds.arr(center,'code_length')
    radius=ds.quan(radius,'code_length')*scale
    print(radius)
    sp=ds.sphere(center,radius)
    return ds,sp



#################################################
# Deal with merger trees
#################################################

class merger_tree(object):
    def __init__(self,base_id,df,verbose=True):

        df_tree=df+'/ADAPTAclump_maps/4_merger_trees/'
        tree_file_name=df_tree+f'/data_tree_{str(base_id).zfill(6)}_MSM.dat'
        self.data=ascii.read(tree_file_name,names=['level','snapshot','gal_id','empty1','empty2','child_id','parent_id','no_idea1','no_idea2','Mgal'])
        self.data.add_column(snaps_to_out(self.data['snapshot'],df),name='output')
        self.time=time(df)
        self.data.add_column(self.time.convert('output','time',self.data['output']),name='time')
        self.data.remove_columns(['empty1','empty2','no_idea1','no_idea2'])
        self.data.add_column(list(range(len(self.data))),name='row',index=0)
        self.ids=list(set(self.data['level']))
        self.ids.sort()
        print("Merger tree",base_id,"has",len(self.ids),"branches:",self.ids)

        self.construct_merger_list()
    def get_branch(self,id,field):
        branch_mask=self.data['level']==id
        return self.data[branch_mask][field]
    def find_target_galaxy(self,gal_id):
        try:
            branch=self.data['level']==gal_id
            target_id=self.data[branch][0]['child_id']
            target_output=self.data[branch][0]['snapshot']+1
            mask=np.logical_and(self.data['gal_id']==target_id,self.data['snapshot']==target_output)
            return self.data[mask]
        except:
            mask=np.logical_and(self.data['gal_id']==target_id,self.data['snapshot']==(target_output-1))
            return merger_tree.data[mask]
    def construct_merger_list(self):
        # This function collects a list of all mergers within the merger tree
        # merger_tree: Table loaded via load_merger_tree
        merger_list=table.Table(names=('primary_level','secondary_level','primary_mass','secondary_mass','merger_snapshot','merger_time','secondary_mass_max'),
                      dtype=('i4','i4','f4','f4','i4','f4','f4'))
        branches=list(set(self.data['level']))
        branches.sort()
        for branch in branches:
            mask=self.data['level']==branch
            main=self.find_target_galaxy(branch)
            try:
                merger_list.add_row([main['level'],branch,main['Mgal'],self.data[mask]['Mgal'][0],main['snapshot'],main['time'],self.data[mask]['Mgal'].max()])
            except:
                print(branch,"survives until the end")
        merger_list.add_column(merger_list['secondary_mass']/merger_list['primary_mass'],name='merger_ratio')
        self.merger_list=merger_list

def snaps_to_out(snaps,df):
    input_file=df+'/ADAPTAclump_maps/3_trees_from_treemaker/input_TreeMaker.dat'
    with open(input_file) as f:
        r=csv.reader(f)
        next(r)
        base_snap=int(next(r)[0].split('/')[-1][-4:-1])
    return [snap+base_snap for snap in snaps]


def time_to_outputput(plot_t):
    times=output_step_redshift()
    return [times['output'][times['time']==t][0] for t in plot_t]
