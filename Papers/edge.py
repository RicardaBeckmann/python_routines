from astropy.io import ascii
import ytutils
import numpy as np
import adaptahop as ahop
import utils
import pictures

df_fiducial='/lustre/dirac/scratch/dp191/dc-beck6/383compromise_fiducial'

# Quick plot with halos
def plot(sp,output,df,field,limits=[None,None],saveit=False,addendum=''):
    myplot=pictures.plot(output,df,field=field,region=sp,center=sp.center,
                     zoom=sp.radius.in_units('kpc')*2,unit='kpc',redshift=True,saveit=False,limits=limits)
    gals=ahop.load_gal_list(output,df)
    ahop.annotate_substructure(myplot,gals)

    if saveit:
        utils.saveas(myplot,f'{field}_{str(output).zfill(5)}'+addendum,pdf=False,output_folder=of)
    return myplot


# make ic_sink file
def make_ic_sink(ds,pos,Mbh,ndx):
    
# Mbh in Msun
    # pos is the position in code units
    sp_BH=ds.sphere(pos,ds.index.get_smallest_dx()*ndx)
    print("This sphere contains",len(sp_BH[('star','particle_mass')]),'star particles')
    print("The most massive star particle in the sphere has a mass of",utils.sci(sp_BH[('star','particle_mass')].max().in_units('Msun')),'Msun')

    xBH=ds.arr(pos,'code_length')
    vBH=sp_BH.quantities.bulk_velocity(use_gas=False,use_particles=True,particle_type='star')
    dat=[float(ds.quan(Mbh,'Msun').in_units('code_mass').v)]+list(xBH.in_units('code_length').v)+list(vBH.in_units('code_velocity').v)+[0,0,0]
    ic_sink=''

    print(xBH)
    print(vBH.in_units('km/s'))

    for quan in dat:
        ic_sink+=str(quan)+','
    print(ic_sink[:-1])
    return

