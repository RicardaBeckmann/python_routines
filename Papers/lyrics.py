import ytutils
import sinks
import csv
import pictures
from utils import *
from astropy.units import G,kpc
from astropy.table import Table

from yt.utilities.physical_constants import \
    kboltz

try:
    ytutils.yt.define_unit('Zsun_ramses',(0.02/0.01295,'Zsun'))
except:
    print("Skip ramses solar mass definition")

#######################
# TOPIC: Plots
######################
def add_cluster_phases(ax,cluster):
    i=0
    flip_times=cluster.phase_change_times
    while(i<len(flip_times)-1):
        ax.axvspan(flip_times[i],flip_times[i+1],color='grey',alpha=0.3,zorder=-1)
        i+=2
    return ax

def add_accretion_rate(ax,sinkfile,cold_gas=False,nsmooth=1000,BHcolour='grey',linestyle='--',alpha=1.0,ylim=[2E44,9E49]):
    ax_right=ax.twinx()
    nan_mask=sinkfile.getattr('L_AGN')>0
    ax_right.plot(shorten_data(sinkfile.getattr('time')[nan_mask]*1E-6,nsmooth=nsmooth),
    shorten_data(sinkfile.getattr('L_AGN')[nan_mask],nsmooth=nsmooth),
    color=BHcolour,label=r'$L_{\rm AGN}$ [erg/s]',linestyle=linestyle,alpha=alpha)
    ax_right.set_yscale('log')
    ax_right.set_ylabel(r'$L_{\rm AGN}$ [erg/s]')
    ax_right.set_ylim(ylim)
    return ax

def get_clump(ad,id):
    return ad.cut_region('obj["hydro_relevant_clump_id"]=={0}'.format(id))

def plot_cluster_properties(folders=['.'],fields=['Mcold'],rb=[0,0],create_file=False,pdf=False):
    fig,axes=plt.subplots(len(fields),1,sharex=True,figsize=(6,3*len(fields)))
    if len(fields)==1:
        axes=[axes]

    data_min={}
    data_max={}
    for i,folder in enumerate(folders):
        colour=get_colours(rb[0],rb[1])[i]
        cluster=Cluster(folder=folder,create_file=create_file,print_info=(i==0))
        time=cluster.times
        for j,(ax,field) in enumerate(zip(axes,fields)):
            data=getattr(cluster,field)
            ax.plot(time,data,label=folder,color=colour)
            data_min[field]=min(data[data>0])  #Used to set the y scale
            data_max[field]=max(data[data>0])

    for j,(ax,field) in enumerate(zip(axes,fields)):
        #ax.set_yscale('log')
        ax.set_yscale('symlog',linthresh=max(data_min[field],data_max[field]/10))
        ax.set_ylabel(field)

    axes[-1].set_xlabel('time [Myr]')
    axes[0].legend()
    figname='cluster_properties_n{0}'.format(len(folders))

    saveas(fig,figname,addendum=None,pdf=pdf)

def annotate_clump_axis(pic,id,pp,axis='major',color='k',folder='.',l_min=0):
    '''This function will overplot a 3D axis vector on a yt plot
    pic: yt picture
    id: clump id
    pp: YTClumps instance corresponding to the output
    axis: axis name
    color: line color'''

    j=int(np.where(np.array(['minor','median','major'])==axis)[0])
    I=int(np.where(pp.ids==id)[0])
    cp=Clump_profile(id,pp.output,folder=folder)
    ax=pic.ds.quan(getattr(pp.axes,axis)[I],'kpc')
    center=pp.pos[I]
    p1=(center-pp.axis_vectors[I][j]*pic.ds.quan(cp.ends[0]*1,'kpc'))
    p2=(center-pp.axis_vectors[I][j]*pic.ds.quan(cp.ends[1]*1,'kpc'))
    if ax>l_min:
        pic.annotate_arrow(pos=p2,starting_pos=p1,plot_args={'color':color})
        return True
    else:
        print("Filament",id,"is only",ax,"long and therefore shorter than the cutoff of",l_min)
        return False


########################
# TOPIC: Fields
#######################
def _clump_category(field,data):
    radii=data.get_field_parameter('radii')
    clump_ids=data['hydro_relevant_clump_id']
    new_data=data['zeros']
    if radii==0.0:
        foo=0
    else:
        # Small clumps =1
        new_data[np.isin(clump_ids,radii.ids[radii.small_clumps])]=1
        # Big clumps =2
        new_data[np.isin(clump_ids,radii.ids[radii.big_clumps])]=2
        # filaments=3
        new_data[np.isin(clump_ids,radii.ids[radii.filaments])]=3
    return new_data


def add_fields(ds):
    ds.add_field(name=('gas','t_cool'),sampling_type="cell",function=_t_cool,
                   units='s')
    ds.add_field(name=('gas','t_AGN'),sampling_type="cell",function=_t_AGN,
                   units='Myr')

    ds.add_field(name=('gravity','t_ff'),sampling_type="cell",function=_t_ff,units='Myr')

    ds.add_field(name=('gas','cell_mass_hot'),sampling_type='cell',function=_hot_gas,
                 units='code_mass')

    ds.add_field(name=('gas','cell_mass_cold'),sampling_type='cell',function=_cold_gas,
                  units='code_mass')


    #Add MHD fields if MHD simulation
    if ('gas','magnetic_field_x_left') in ds.derived_field_list:
        ds.add_field(name=('gas','b_r'), sampling_type='cell',function=_costhetaB,
                     units='dimensionless')

        ds.add_field(name=('gas','b_r_squared'), sampling_type='cell',function=_costhetaB_squared,
                     units='dimensionless')


    if ('gas','pressure_cosmic_ray') in ds.derived_field_list:
        ds.add_field(name=('gas','eta'), sampling_type='cell',function=_fcr, units=('dimensionless'),display_name=r'$\eta$')
    return

def _fcr(field,data):
    return data['pressure_cosmic_ray']/data['pressure']

def _costhetaB(field,data):
    c=data.ds.quan(0.5,'code_length')
    return ((data['x']-c)*data['magnetic_field_x']+(data['y']-c)*data['magnetic_field_y']+(data['z']-c)*data['magnetic_field_z'])/np.sqrt((data['x']-c)**2+(data['y']-c)**2+(data['z']-c)**2)/data['magnetic_field_magnitude']

def _costhetaB_squared(field,data):
    return data['b_r']**2

def _t_ff(field,data):
    '''Free fall time based on gravitational acceleration'''
    return np.sqrt(data['radius']/np.sqrt(data['x-acceleration']**2+data['y-acceleration']**2+data['z-acceleration']**2)).in_units('Myr')

def _cold_gas(field,data):
    '''Cold gas mass'''
    mgas=data['cell_mass']
    mgas[data['temperature']>=1E6]=0
    return mgas.in_units('code_mass')

def _hot_gas(field,data):
    '''Hot gas mass'''
    mgas=data['cell_mass']
    mgas[data['temperature']<1E6]=0
    return mgas.in_units('code_mass')


def _t_cool(field,data):
    '''Gas cooling time'''
    return (data.ds.gamma - 1)*kboltz*data['temperature']/(data['number_density']*data['cooling_total'])

def _t_AGN(field,data):
    '''Time since AGN feedback'''
    t_decay=data.ds.quan(10,'Myr')
    return np.log(data['hydro_jetscalar'])*t_decay*-1

#######################
# TOPIC: Geometric objects
######################

def get_cold_region(ds,ad=None,Tcold='1E5'):
    '''A yt cutregion containing only cold gas cells'''
    if not(ad):
        ad=ds.all_data()
    cold_region=ad.cut_region(["obj['temperature']<{0}".format(Tcold)])
    return cold_region


#################################
# TOPIC: preprocessed data files
#################################
def make_clump_profile(clump_id,cold,pp,folder='.',force_override=False):
    '''Make profiles along the major axis of the clump.
    clump_id = id of the clump
    cold = pre-processed yt cut region of all clumps
    pp =YTClumps file of the output'''
    filename=folder+'/Clump_profiles/clump_profiles_{0}_{1}.csv'.format(str(pp.output).zfill(5),str(clump_id).zfill(5))

    if os.path.exists(filename):
        print("File exists for clump",clump_id,"in output",pp.output)
        if not(force_override):
            return 0,0,0
        else:
            print("Forcing file recreation")
    print(filename)
    I=int(np.where(pp.ids==clump_id)[0]   )
    clump_yt=cold.cut_region('obj["hydro_relevant_clump_id"]=={0}'.format(clump_id))

    major=cold.ds.quan(pp.axes.major[I],'kpc').in_units('code_length')
    minor=cold.ds.quan(pp.axes.minor[I],'kpc').in_units('code_length')

    nbins=int(np.floor(major/cold.ds.index.get_smallest_dx()))-2 #Minus 2 in the hopes of avoiding gaps

    furthest=(cold.ds.arr(clump_yt.quantities.max_location('radius')[1:],'cm')-cold.center).in_units('code_length')
    center=pp.pos[I].in_units('code_length')
    normal=(pp.axis_vectors[I][-1]*major).in_units('code_length')
    normal*=np.sign(np.dot(furthest.v,normal.v)) #Make sure it points away from the center

    cyl=cold.ds.disk(center,normal,minor,major,data_source=clump_yt)
    central=clump_yt['radius'].min().in_units('kpc')
    prof_cyl=ytutils.yt.create_profile(cyl,['height'],fields=['metallicity',
                                                      'density',
                                                      'hydro_dust',
                                                      'temperature',
                                                      'radius'],
                               weight_field='density',logs={'height':False},n_bins=nbins)
    prof_cyl_sum=ytutils.yt.create_profile(cyl,['height'],fields=['cell_mass'],
                               weight_field=None,logs={'height':False},n_bins=nbins)

    along=prof_cyl.x.in_units('kpc')

    with open(filename,'w') as f:
        writer=csv.writer(f)
        writer.writerow(['length [kpc]','density [Hcc]','mass [Msun]','metallicity','dust','temperture'])
        writer.writerow(['minimum distance to sink',central.v])
        writer.writerow(['l_left and l_right along axis',along.min().v,along.max().v])
        writer.writerow((along-along.min()).v)
        writer.writerow(prof_cyl['density'].in_units('amu/cm**3').v)
        writer.writerow(prof_cyl_sum['cell_mass'].in_units('Msun').v)
        writer.writerow(prof_cyl['metallicity'].v)
        writer.writerow(prof_cyl['hydro_dust'].v)
        writer.writerow(prof_cyl['temperature'].v)

    return along,prof_cyl,prof_cyl_sum


def find_time(outputs,df='.',write=False):
    ''' Find the time of a given lyrics output'''

    filename=df+'/Data/'+'time.txt'

    if write:
        with open(filename,'w') as f:
            writer=csv.writer(f)
            writer.writerow(['output', 'time [Myr]'])

        for output in range(1,200):
            ds=ytutils.load(output,df)
            time=ds.current_time.in_units('Myr')
            writer.writerow([output,time.v])

    else:
        with open(filename) as f:
            reader=csv.reader(f)
            next(reader)
            data=np.array([row for row in reader])
        try:
            len(outputs)
        except:
            outputs=[outputs]
        times=data[:,1].astype('float')
        times=np.array([times[output-1] for output in outputs])
        if len(outputs)==1:
            return float(times)
        else:
            return times

def load_hot_tracer_ids(output,df,zoom=55):
    filename=df+'/Data/'+'hot_tracers_{0}_{1}kpc.npy'.format(str(output).zfill(5),zoom)
    return set(np.load(filename))

def read_tracer_file(output,folder,silent=False,cold=True):
    if cold:
        filename=folder+'/Data/'+'clump_tracers_{0}_cold.cvs'.format(str(output).zfill(5))
    else:
        filename=folder+'/Data/'+'clump_tracers_{0}.cvs'.format(str(output).zfill(5))
    if not silent:
        print("Opening",filename)
    with open(filename) as f:
        reader=csv.reader(f)
        next(reader)
        data={}
        for row in reader:
            data[int(row[0])]=np.array(row[1:]).astype('int')
    return data

def load_all_clump_tracers(output,folder,silent=False):
    tracer_data=read_tracer_file(output,folder=folder,silent=silent)
    return set(flatten(tracer_data.values()))

def read_fragmentation_tree(output,clump_id,folder='.'):
#Read fragmentation_tree files written in http://localhost:8888/notebooks/2018_11_26_POSTPROCESS_calculate_clump_ages.ipynb
    filename=folder+'/Data/'+'fragmentation_tree_o{0}_c{1}.csv'.format(str(output).zfill(5),str(clump_id).zfill(4))
    print("Opening",filename)
    with open(filename) as f:
        reader=csv.reader(f)
        data=[row for row in reader]
        data=data[4:]
    outputs=[int(row[0]) for row in data]
    clump_ids={}
    for output,row in zip(outputs,data):
        clump_ids[output]=np.array(row[1:]).astype('int')

    return outputs,clump_ids

def read_cold_gas_times(folder='.',filename='Data/cold_gas_times.csv'):
    with open(folder+'/'+filename) as f:
        reader=csv.reader(f)
        next(reader)
        next(reader)
        next(reader)
        next(reader)
        min_times=np.array(next(reader)).astype('float')
        max_times=np.array(next(reader)).astype('float')
    return min_times,max_times


def post_process_clumpfinder_files(output=1,folder='.',output_folder=None,cold=True):
    ''' Post process the clump_*txt* files produced by the RAMSES
    clumpfinder, for easier reading and plotting later on. Produces one
    output file per output, in Data, called clumpfinder_*csv.
    Reread with the Clumpfinder object below'''

    sink=sinks.read(output,folder)[0]
    ds=sink.ds

    #Load existing data files
    filename=folder+'/output_{1}/clump_{1}.txt{0}'


    if not(os.path.exists(filename.format(str(1).zfill(5),str(output).zfill(5)))):
        print("No RAMSES clump files found")
        return
    else:
        all_data=[]
        for i in range(1,ds.parameters['ncpu']+1):
            with open(filename.format(str(i).zfill(5),str(output).zfill(5))) as f:
                reader=csv.reader(f,delimiter=' ')
                next(reader)
                rows=[row for row in reader]
        #Filter out the empty ones
                data=[[item for item in row if not item==''] for row in rows]
                if len(data)>0:
                    all_data+=data
    all_data=np.array(all_data)

    big_to_small=True
    masses=all_data[:,10].astype('float')
    all_data=sort(all_data,masses,reverse=big_to_small)

    #Save data, sorted from most to least massive
    ids=all_data[:,0].astype('int')
    ncells=all_data[:,3].astype('int')
    masses=ds.arr(sorted(masses,reverse=big_to_small),'code_mass')
    pos=ds.arr(all_data[:,4:7].astype('float')/ds.parameters['boxlen'],'code_length')
    rho_min=ds.arr(all_data[:,7].astype('float'),'code_density')
    rho_max=ds.arr(all_data[:,8].astype('float'),'code_density')
    relevance=all_data[:,11].astype('float')

    r_center=np.linalg.norm((pos-ds.arr([0.5,0.5,0.5],'code_length')).in_units('kpc').v,axis=1)
    r_sink=np.linalg.norm((pos-sink.pos).in_units('kpc').v,axis=1)

    #Open the new data file and write the information to file
    if not output_folder:
        output_folder=folder
    if cold:
        output_file=output_folder+'/Data/clumpfinder_{0}_cold.csv'.format(str(output).zfill(5))
    else:
        output_file=output_folder+'/Data/clumpfinder_{0}.csv'.format(str(output).zfill(5))

    print("opening",output_file)
    check_data_folder(folder)
    with open(output_file,'w') as f:
        writer=csv.writer(f)
        writer.writerow(["Post-processed RAMSES clumpfinder files, sorted by clump mass"])
        writer.writerow(["output, time [Myr], sink mass [Msun], sink position"])
        writer.writerow([output,
                         ds.current_time.in_units('Myr').v,
                         sink.mass.v]+
                        list(sink.pos.v))

        writer.writerow(["id",
                        "ncells",
                        "mass",
                        "r_center",
                        "r_sink",
                        'position',
                         'rho_min',
                        'rho_max',
                         'relevance'])

        for i,clump_id in enumerate(ids):
            writer.writerow([clump_id,
                             ncells[i],
                             masses[i].in_units('Msun').v,
                             r_center[i],
                             r_sink[i]]+list(pos[i].v)+
                            [rho_min[i].v,
                             rho_max[i].v,
                             relevance[i]])
    return

class Clumpfinder(object):
    ''' Read the output text files produced by post processing the RAMSES
    clumpfinder file using post_process_clumpfinder_files. Reads files
    Data/clumpfinder_*'''

    def __init__(self,output=1,folder='.',cold=True):

        if cold:
            filename=folder+'/Data/clumpfinder_{0}_cold.csv'.format(str(output).zfill(5))
        else:
            filename=folder+'/Data/clumpfinder_{0}.csv'.format(str(output).zfill(5))
        ramses_filename=folder+'/output_{1}/clump_{1}.txt{0}'

        if not(os.path.exists(filename)):
            if os.path.exists(ramses_filename.format(str(1).zfill(5),str(output).zfill(5))):
                print("I need to create the clumpfinder file for output",output)
                post_process_clumpfinder_files(output=output,folder=folder)
            else:
                print("No clump data exists for output",output)
                return

        all_data=[]
        with open(filename) as f:
            reader=csv.reader(f)
            next(reader)  #Skip intro
            next(reader)  #Skip global data names
            glob_info=next(reader)  #Read global data
            next(reader)  #Skip field names

            #Load data from file
            all_data=[row for row in reader]

        all_data=np.array(all_data)

        #Save data, sorted from most to least massive
        self.output=int(glob_info[0])
        self.time=float(glob_info[1])  #in Myr
        self.sink_mass=float(glob_info[2])
        self.sink_pos=np.array(glob_info[3:6]).astype('float')   #in code length

        self.ids=all_data[:,0].astype('int')
        self.ncells=all_data[:,1].astype('int')
        self.masses=all_data[:,2].astype('float')
        self.r_center=all_data[:,3].astype('float')
        self.r_sink=all_data[:,4].astype('float')
        self.pos=all_data[:,5:8].astype('float')
        try:
            self.rho_min=all_data[:,8].astype('float')
            self.rho_max=all_data[:,9].astype('float')
            self.relevance=all_data[:,10].astype('float')
        except:
            return

    def get_index_property(self,index,prop):
        return getattr(self,prop)[self.ids==index][0]

def write_cluster_file(output=0,folder='.',append=False,Tcold='1E4',Thot='1E7',sink=None):
    ''' Post-process quantities for the whole cluster using two different temperature cuts
    Read using Cluster object'''
    if not sink:
        sink=sinks.read(output,folder)[0]
    ds=sink.ds
    output=int(str(ds).split('_')[-1])


    check_data_folder(folder)

    radii=[1,5,10,20,30,200]  # in kpc
    ad=ds.sphere(sink.pos,ds.quan(max(radii),'kpc'))
    ad['density']
    fields={
            'Mgas':(lambda d:d['cell_mass'].in_units('Msun').sum()),
            'Mstar':(lambda d:d[('star','particle_mass')].in_units('Msun').sum()),
            'Mmetal':(lambda d:(d['metallicity']*d['cell_mass']).in_units('Msun').sum()),
            'Mdust':(lambda d:(d['hydro_dust']*d['cell_mass']).in_units('Msun').sum()),
            'Tmin':(lambda d:(d['temperature'].min())),
            'Tmax':(lambda d:(d['temperature'].max())),
            'Tavg':(lambda sp:sp.quantities.weighted_average_quantity(['temperature'],'ones')),
            'Tavg_mass':(lambda sp:sp.quantities.weighted_average_quantity(['temperature'],'density'))
            }


    filename='{0}/Data/cluster_properties_{1}.csv'.format(folder,str(output).zfill(5))
    print('Opening',filename)
    with open(filename,'w') as f:
        writer=csv.writer(f)
        writer.writerow(['## File containing cluster properties at different times, at a set of different radii'])
        writer.writerow(['## Each field is given in the order cold, warm, hot'])
        writer.writerow(['## Cold temperature: {0} K'.format(Tcold)])
        writer.writerow(['## Hot temperature: {0} K'.format(Thot)])
        writer.writerow(['## masses in Msun, temperatures in K'])
        writer.writerow(['## output','time [Myr]','Mbh'])

        #write overall data
        writer.writerow([output,
                         ds.current_time.in_units('Myr').v,                       #time
                         sink.mass.in_units('Msun').v
                         ])

        writer.writerow(['## RADIUS [kpc]']+list(fields.keys()))
    for radius in radii:
        sp=ds.sphere(sink.pos,ds.quan(radius,'kpc'),data_source=ad)
        cold=sp.cut_region(["obj['temperature']<{0}".format(Tcold)])
        warm=sp.cut_region(["(obj['temperature']>={0})*(obj['temperature']<={1})".format(Tcold,Thot)])
        hot=sp.cut_region(["obj['temperature']>{0}".format(Thot)])
        data=[radius]
        for field in fields.keys():
            for region in [cold,warm,hot]:
                try:
                    data.append(float(fields[field](region).v))
                except:
                    data.append(0)
                    print("There is an issue with r",radius,'and field',field)
        with open(filename,'a') as f:
            writer=csv.writer(f)
            writer.writerow(data)
        del(sp,cold,warm,hot)
    return

class Cluster(object):
    ''' Read the files written by write_cluster_files, callled cluster_properties'''
    def __init__(self,folder='.',create_file=False,print_info=False):
        folder=folder+'/Data/'
        data_files=np.array([name for name in os.listdir(folder) if name[:18]=='cluster_properties'])
        if len(data_files)==0:
            print("No cluster_properties files found in",folder)
            return
        all_data=[]
        outputs=[]
        times=[]
        Mbh=[]
        for filename in data_files:
            with open(folder+filename) as f:
                reader=csv.reader(f)
                found_data=False
                while not(found_data):
                    line=next(reader)
                    if line[0].split(' ')[1]=='output':
                        radii_fields=line[1:]
                        found_data=True
                        glob=next(reader)
                        fields=next(reader)[1:]
                data=np.array([row for row in reader]).astype('float')
                if len(data)==0:
                    print("Skipping",glob[0],"as it is incomplete")
                elif data[-1,0]<200:
                    print("Skipping",glob[0],"as it is incomplete")
                else:
                    all_data.append(data)
                    outputs.append(int(glob[0]))
                    times.append(float(glob[1]))
                    Mbh.append(float(glob[2]))

        if len(all_data)==0:
            print("No valid data found")
            return
        #Convert to single numpy array
        all_data=np.array(all_data).astype('float')
        #Sort by output number
        all_data=sort(all_data,outputs)
        self.radii=all_data[0,:,0].astype('int')

        self.outputs=np.array(sorted(outputs))
        self.times=sort(times,outputs)
        self.Mbh=sort(Mbh,outputs)
        self.fields=fields
        for j,radius in enumerate(self.radii):
            for i,field in enumerate(fields):
                for k,temp in enumerate(['cold','warm','hot']):
                    setattr(self,field+'_{0}'.format(temp)+str(radius),all_data[:,j,3*i+1+k])
            for field in ['gas','star']:
                mass=getattr(self,'M{0}_cold{1}'.format(field,str(radius)))+getattr(self,'M{0}_warm{1}'.format(field,str(radius)))
                setattr(self,'M{0}_clump{1}'.format(field,str(radius)),mass)
        self.Mgas_clump=self.Mgas_cold200+self.Mgas_warm200
        print("Cluster output range:",min(self.outputs),'-',max(self.outputs))

        try:
            self.calculate_cluster_phases()
        except:
            print("Skip calculating cluster phases")

    def get_output_attr(self,output,attr):
        val=getattr(self,attr)[np.where(self.outputs==output)][0]
        return val


    def calculate_cluster_phases(self):
        '''Get a mask where -1 = heating domianted regime and 1 = cooling dominated regime'''

        #Calculate gradient of the cold gas evolution
        dt=(self.times[1:]-self.times[:-1])*1E6
        dotMgas_cold=(self.Mgas_cold200[1:]-self.Mgas_cold200[:-1])/dt
        dotMgas_warm=(self.Mgas_warm200[1:]-self.Mgas_warm200[:-1])/dt
        dotMgas_hot=(self.Mgas_hot200[1:]-self.Mgas_hot200[:-1])/dt
        dotMgas_clump=(self.Mgas_clump200[1:]-self.Mgas_clump200[:-1])/dt


        self.dotM_cold200=np.array([0]+list(dotMgas_cold))
        self.dotM_warm200=np.array([0]+list(dotMgas_warm))
        self.dotM_hot200=np.array([0]+list(dotMgas_hot))
        self.dotMgas_clump200=np.array([0]+list(dotMgas_clump))

        #Make status array
        phases=np.zeros(len(dotMgas_cold))
        phases[dotMgas_clump>10]=1
        phases[dotMgas_clump<-10]=-1

        #Smooth array to get rid of small fluctuations in the total cold gas mass


        for j in range(2):
            for i,p in enumerate(phases[:-1]):
                if i==0:
                    phases[i]=1
                elif p==0:
                    phases[i]=phases[i-1]
                elif (p*phases[i-1])<0 and (p*phases[i+1])<0:
                    phases[i]=phases[i+1]
                elif (p*phases[i-2])<0 and (p*phases[i+2])<0:
                    phases[i]=phases[i+1]
        phases=np.append(phases,1)

        #Find the times when the cluster changes phase
        flip_times=self.times[1:][(np.diff(np.sign(phases)) != 0)]
        flip_times=np.append(flip_times[::-1],0)[::-1]
        self.cooling_phase=phases>0
        self.heating_phase=phases<0
        self.phase_change_times=flip_times
        return

class SFR(object):
    def __init__(self,df):
        filename=df+'/Data/SFR.csv'
        ds=ytutils.find_last_ds(df,verbose=False)
        t_current=ds.current_time.in_units('Myr').v
        print("Final time",ds.current_time.in_units('Myr'))

        if os.path.isfile(filename):
            with open(filename) as f:
                reader=csv.reader(f)
                next(reader)
                data=[row for row in reader]
                data=np.array(data).astype('float')
            try:
                if(data[-1,0]<t_current*0.99):
                    rewrite_file=True

                else:
                    rewrite_file=False
                    times=data[:,0]
                    sfr=data[:,1]
                    dm_star=data[:,2]
                    mass_evolution=data[:,3]
            except:
                rewrite_file=True
        else:
            rewrite_file=True

        if rewrite_file:
            check_data_folder(df)
            times,sfr,dm_star,mass_evolution=calculate_sfr(folder=df)
            with open(filename,'w') as f:
                writer=csv.writer(f)
                writer.writerow(['time [Myr]','SFR','dm_star','mass_evolution'])
                for row in zip(times,sfr,dm_star,mass_evolution):
                    writer.writerow(row)
        self.times=times[:-1]
        self.sfr=sfr[:-1]
        self.dm_star=dm_star[:-1]
        self.mass_evolution=mass_evolution[:-1]


def calculate_sfr(folder='.',dt=10,preloaded=None,times=[]):
    '''times & dt: in [Myr]'''
    if preloaded:
        ds=preloaded
    else:
        ds=ytutils.find_last_ds(folder=folder)

    ad=ds.all_data()
    masses=ad['star','particle_mass']
    ages=ad['star','star_age'].in_units('Myr')
    now=ds.current_time.in_units('Myr').v
    SFR=[]
    if len(times)==0:
        times=np.array(range(0,int(np.ceil(now))+dt,dt))
    for i,t in enumerate(times[1:]):
        dm=masses[(ages>times[i-1])*(ages<(times[i]))].sum().in_units('Msun').v
        sfr=dm/(dt*1E6)
        SFR.append(sfr)
    SFR.append(0)
    SFR=np.array(SFR)[::-1]
    times=np.abs(times-now)[::-1]
    dm_star=SFR*dt*1E6
    mass_evolution=np.cumsum(dm_star)
    return times,SFR,dm_star,mass_evolution

def plot_sfr(folders=['.'],dt=10,rb=[0,0],pdf=False,preloaded=None,output_folder='.'):
    ''' dt is the time-slice in Myr'''
    fig,axes=plt.subplots(2,1,sharex=True)
    colours=get_colours(rb[0],rb[1])
    for i,folder in enumerate(folders):
        colour=colours[i]
        times,SFR,mass_evolution,masses=calculate_sfr(folder=folder,preloaded=preloaded,dt=dt)

        axes[1].scatter(times[-1],masses,color=colour)
        axes[0].plot(times,SFR,color=colour)
        axes[1].plot(times,mass_evolution,color=colour,label=folder)

    #Set up SFR plot
    axes[0].set_ylabel('SFR [Msun/yr]')
    axes[0].set_yscale('symlog',linthresh=1E0)
    axes[0].axhspan(20,40,color='k',alpha=0.3,label='Obs: Canning2010')
    axes[0].axhspan(1E-1,500,color='grey',alpha=0.1,label='Sim: Li2014')
    axes[0].legend()

    #Set up stellar mass plot
    axes[1].set_ylabel(r'$M_*$ [Msun]')
    axes[1].legend(frameon=False)

    #Other setup
    axes[-1].set_xlabel('time [Myr]')


    saveas(fig,'SFR_n{0}'.format(len(folders)),addendum=None,pdf=pdf,output_folder=output_folder)
    return


def post_process_clumps(output,folder='.',cold=True,obj=None):
    ''' Loop over all clumps and post-process a range of quantities using yt.
    Information to be read using YTclumps'''

    check_data_folder(folder)

    #Open the file
    if cold:
        filename='{0}/Data/clump_properties_{1}_cold.csv'.format(folder,str(output).zfill(5))
        tracername='{0}/Data/clump_tracers_{1}_cold.cvs'.format(folder,str(output).zfill(5))
    else:
        print("Code in filename for non-cold case")
        return

    cf=Clumpfinder(output,folder,cold=cold)

    if not hasattr(cf,'ids'):
        print("Aborting, no clump information found")
        return
    print("Number of clumps in the file",len(cf.ids))


    try:
        existing=YTclumps(output,folder,cold=cold)
        prewritten=existing.ids
        print(len(prewritten),"clumps already in file,",np.round(100*len(prewritten)/len(cf.ids),1),'% done')
    except:
        with open(filename,'w') as f:
            writer=csv.writer(f)
            writer.writerow(['## File containing clump properties at given time'])
            writer.writerow(['## masses in Msun - temperatures in K - velocities in km/s'])
            if cold:
                writer.writerow(['## Cold temperature: {0}  K'.format(1E6)])
            else:
                writer.writerow(['## Cold temperature: {0}  K'.format(2E7)])
            writer.writerow(['id',
                             'ncells',
                             'Mgas',
                             'Mdust',
                             'Mmetal',
                             'Mstar',
                             'x_com',
                             'v_bulk',
                             'T_min',
                             'T_max',
                             'V',
                             'Inertia'])

        with open(tracername,'w') as f:
            writer=csv.writer(f)
            writer.writerow(['## Clump id, followed by ID of all tracer particles contained in the clump'])
        prewritten=[]

    if len(prewritten)==len(cf.ids):
        print("File comlete,exit")
        return

    if not obj:
        ds=ytutils.load(output,folder=folder)
        obj=ds.sphere([0.5,0.5,0.5],ds.quan(50,'kpc'))

    masses=obj['cell_mass']
    x=obj['x']
    y=obj['y']
    z=obj['z']
    vx=obj['velocity_x'].in_units('km/s')
    vy=obj['velocity_y'].in_units('km/s')
    vz=obj['velocity_z'].in_units('km/s')
    dust=obj['hydro_dust']
    metals=obj['metallicity']
    temperature=obj['temperature']
    volumes=obj['cell_volume']

    for clump_id in cf.ids:  #Zero has already been removed
        if not clump_id in prewritten:
            print("Working on output",output,"and clump id",clump_id)
            clump=obj.cut_region('obj["hydro_relevant_clump_id"]=={0}'.format(clump_id))
            clump_mask=obj["hydro_clump_id"]==clump_id

            stars=clump['star','particle_mass'].sum().in_units('Msun').v

            pos=np.array([x[clump_mask].v,y[clump_mask].v,z[clump_mask].v])

        #Center of mass position
            CoM=np.array([x[clump_mask]*masses[clump_mask],
                      y[clump_mask]*masses[clump_mask],
                      z[clump_mask]*masses[clump_mask]])/masses[clump_mask].v.sum()
            CoM=np.sum(CoM,axis=1)

        #Bulk velocity
            velocity=np.array([vx[clump_mask]*masses[clump_mask],
                               vy[clump_mask]*masses[clump_mask],
                               vz[clump_mask]*masses[clump_mask]])/masses[clump_mask].v.sum()
            velocity=np.sum(velocity,axis=1)

            if len(masses[clump_mask].v)>1:
                I=inertia_tensor(mass=masses[clump_mask].in_units('code_mass'),pos=pos.transpose(),center=CoM)
            else:
                I=np.zeros((3,3))

            data=[int(clump_id),
                  len(masses[clump_mask].v),
                  float(masses[clump_mask].sum().in_units('Msun').v),
                  float((masses[clump_mask]*dust[clump_mask]).sum().in_units('Msun').v),
                  float((masses[clump_mask]*metals[clump_mask]).sum().in_units('Msun').v),
                  float(stars)]+list(
                CoM)+list(
                      velocity)+[
                    float(temperature[clump_mask].max().v),
                    float(temperature[clump_mask].min().v),
                    float(volumes[clump_mask].sum().v)
                    ]+list(np.array(I).flatten())
            with open(filename,'a') as f:
                writer=csv.writer(f)
                writer.writerow(data)

            with open(tracername,'a') as f:
                writer=csv.writer(f)
                writer.writerow([int(clump_id)]+list(np.array(clump[('gas_tracer','particle_identity')].v).astype('int')))

    return

class Axes(object):
    def __init__(self,output,folder='.'):
        filename=folder+'/Data/'+'axis_lengths_{0}.txt'.format(str(output).zfill(5))

        self.output=output
        self.folder=folder

        with open(filename) as f:
            reader=csv.reader(f)
            next(reader)
            self.dx_min=float(next(reader)[1])
            self.time=float(next(reader)[1])
            data=np.array([row for row in reader])

        self.ids=data[:,0].astype('int')
        self.major=data[:,1].astype('float')
        self.median=data[:,2].astype('float')
        self.minor=data[:,3].astype('float')
        self.ncells=data[:,4].astype('int')
        self.ratio_median=(self.median/self.major)
        self.ratio_minor=(self.minor/self.major)
        self.ellipse_volume=4/3*self.major*self.median*self.minor*np.pi/8  #Use the half-axis, not the full diameters
        self.ncells_ellipse=self.ellipse_volume/(self.dx_min**3)
        self.volume_ratio=self.ncells/self.ncells_ellipse
        self.volume_ratio[self.ncells<6]=0.9

    def categorise_clumps(self,rsmall=1.5390,rbig=1.5390+1.421):#,ncell_min=100,volume_ratio_max=0.33333,min_major=0):
        from palettable.colorbrewer.sequential import YlGnBu_4

        self.colours=YlGnBu_4.mpl_colors

        #Safe the parameters
        #self.ncell_min=ncell_min
        #self.volume_ratio_max=volume_ratio_max

        self.all_structures=self.ncells>0

        self.rsmall=rsmall
        self.rbig=rbig

        self.small_clumps=self.major<rsmall
        self.filaments=self.major>rbig

        '''if ncell_min>0:
        #Pre-categorise the objects
            self.small_clumps=self.ncells<ncell_min
            self.big_clumps=(~self.small_clumps)&(self.volume_ratio>volume_ratio_max)
        elif min_major>0:
            self.small_clumps=self.major<min_major
            self.big_clumps=(~self.small_clumps)&(self.volume_ratio>volume_ratio_max)
        else:
            print("Please set at least min_major or ncell_min")'''

        self.big_clumps=(~self.small_clumps)&(~self.filaments)
        self.not_small=~self.small_clumps


    def categorise_yt_object(self,object):
        try:
            print("Small radius",self.rsmall)
            #print("Minimum cell number",self.ncell_min)
        except:
            print("Categorising clumps with default values")
            print("If you would like different parameters, please call categorise_clumps directly")
            self.categorise_clumps()
            print("Small radius",self.rsmall)
            #print("Minimum cell number",self.ncell_min)
        #print("Maximum volume ratio",self.volume_ratio_max)

        object.set_field_parameter('radii',self)
        object.ds.add_field(('clump','clump_category'), function=_clump_category, units='dimensionless',force_override=True,particle_type=False,not_in_all=True,take_log=False)
        self.clump_region=object.cut_region('obj["hydro_relevant_clump_id"]>0')
        return self.clump_region

    def plot_clump_categorisation(self,zoom=40,output_folder=None,force_reload=False):
        try:
            self.clumps
        except:
            ds=ytutils.load(self.output,self.folder,silent=True)
            sp=ds.sphere([0.5,0.5,0.5],ds.quan(50,'kpc'))
            self.clumps=self.categorise_yt_object(sp)
        if force_reload:
            ds=ytutils.load(self.output,self.folder,silent=True)
            sp=ds.sphere([0.5,0.5,0.5],ds.quan(zoom,'kpc'))
            self.clumps=self.categorise_yt_object(sp)
        pic=pictures.plot(self.output,self.folder,region=self.clumps,field='clump_category',zoom=zoom,unit='kpc',weight_field='ones',colour_map=('YlGnBu', 'sequential', 4),log=False,time=True,limits=[-0.5,3.5],kwargs={'method':'mip'},ds=self.clumps.ds)
        pic['clump_category'].cax.yaxis.set_ticks([0.125,0.375,0.625,0.875])
        pic['clump_category'].cax.set_yticklabels(['Diffuse','Small \n clumps','Big \n clumps','filaments'])
        pic['clump_category'].cax.set_ylabel('')
        if output_folder:
            pic.save(output_folder+'/'+'categorised_structures_{0}.pdf'.format(str(self.output).zfill(5)))
        data=pic.data_source.to_frb((zoom,'kpc'),512)
        return data['clump_category']

class Grid_clumps(object):
    def __init__(self,output,folder='.',hot=False):
        self.output=output
        self.folder=folder

        self.hot=hot

        #Load cell data
        if hot:
            names=['clump_hot_gas_cells']
        else:
            names=['clump_gas_cells','condensated_tracer']
        for filename in names:
            type=filename.split('_')[-1]
            data=np.load(folder+'/Data/'+filename+'_{0}.npy'.format(str(output).zfill(5)))

            with open(folder+'/Data/'+'{0}_descriptor.txt'.format(filename)) as f:
                content=f.readlines()

            fields=[]
            for row in content:
                row=row.split()
                if row[0]=='#':
                    fields.append(row[1])
            #print(type,':',fields)

            if type=='cells':
                self.cells=data
                self.cell_fields=fields
            else:
                self.tracers=data
                self.tracer_fields=fields

            for i,field in enumerate(fields):
                setattr(self,'{0}_{1}'.format(type,field),data[i])

    def calculate_turbulence(self,t_cut=200):
        vbulk=np.ma.average(self.cells_v_radial,weights=self.cells_cell_volume)
        self.Ek=np.ma.average(np.abs(self.cells_v_radial-vbulk)**2,weights=self.cells_cell_volume)/2
        self.sigma_turb=np.std(self.cells_v_radial)

        self.t_AGN=np.log(self.cells_jet_scalar)*10*-1
        self.f_vol=sum(self.cells_cell_volume[self.t_AGN<200])/sum(self.cells_cell_volume)
        return

    def calculate_condensation(self,mtracer=402392,step=0):
        if self.hot:
            print("You loaded hot cell data.Return")
            return

        filename=self.folder+'/Data/'+'evolution_tree_{0}.csv'.format(str(self.output).zfill(5))
        with open(filename) as f:
            reader=csv.reader(f)
            next(reader)
            next(reader)
            time_steps=np.array(next(reader)[1:]).astype('float')

        self.dt=np.abs(time_steps[1:]-time_steps[0])[step]*1E6
        self.tracer_m=np.ones(len(self.tracer_id))*mtracer
        self.tracer_dotM_cond=self.tracer_m/self.dt
        self.tracer_r_cond=self.tracer_r_sink-0.5*(self.tracer_v_radial*self.dt*365*24*60*60/3.08567E16)

    def make_radial_profile(self,field,nbins=40,bins=[],log_r=False,average=False):

        #Load radial data
        if field.split('_')[0]=='tracer':
            radii=self.tracer_r_sink
        elif field.split('_')[0]=='cells':
            radii=self.cells_r_sink
        else:
            print("Error in field",field)

        #Make bins
        if len(bins)==0:
            if log_r:
                bins=np.logspace(np.log10(radii.min()),np.log10(radii.max()),nbins)
            else:
                bins=np.linspace(radii.min(),radii.max(),nbins)

        #Make histogram
        hist,bin_edges=np.histogram(radii,bins=bins,weights=getattr(self,field))
        if average:
            counts,bin_edges=np.histogram(radii,bins=bins,weights=getattr(self,field))
            hist=hist/counts

        return hist,bins[:-1]

    def compute_clump_averages(self,clump_ids,field,mass_weight=False):
        if mass_weight:
            return np.array([np.average(getattr(self,field)[self.cells_clump_id==id],weights=self.cells_m_gas[self.cells_clump_id==id]) for id in clump_ids])
        else:
            return np.array([np.average(getattr(self,field)[self.cells_clump_id==id]) for id in clump_ids])

    def compute_sigma(self,clump_ids,field):
        vels=self.get_clump_values(clump_ids,field)
        try:
            sigma=np.array([np.std(c) for c in vels])
        except:
            sigma=np.std(vels)
        return sigma

    def get_clump_values(self,clump_ids,field,min=False,max=False):
        data=[getattr(self,field)[self.cells_clump_id==id] for id in clump_ids]
        if min:
            return np.array([c.min() for c in data])
        elif max:
            return np.array([c.max() for c in data])
        else:
            return data

def clump_field(field,data):
    if data.has_field_parameter('ppclumps'):
        ppclumps=data.get_field_parameter('ppclumps')
        clump_ids=ppclumps.ids
        clump_data=getattr(ppclumps,data.get_field_parameter('clump_field'))
        try:
            field_unit=str(clump_data[0].units)
        except:
            field_unit='dimensionless'
    else:
        clump_ids=np.array([0])
        clump_data=np.array([0])
        field_unit='dimensionless'
    grid_ids=data['hydro_relevant_clump_id']
    output=data['zeros']
    for i,(d,id) in enumerate(zip(clump_data,clump_ids)):
        output[grid_ids==id]=d
    output_data=data.ds.arr(output.v,field_unit)
    return output_data

class YTclumps(object):
    '''Read the post-processed clump information, written using post_process_clumps'''
    def __init__(self,output,folder,cold=True,silent=False,load_full=True):

        self.output=output
        self.folder=folder

        self.ds=ytutils.load(output,folder,silent=True)
        self.time=self.ds.current_time.in_units('Myr').v

        if cold:
            filename='{0}/Data/clump_properties_{1}_cold.csv'.format(folder,str(output).zfill(5),cold)
        else:
            filename='{0}/Data/clump_properties_{1}.csv'.format(folder,str(output).zfill(5),cold)
        with open(filename) as f:
            reader=csv.reader(f)
            data=[line for line in reader]
            fields=data[3]
            data=np.array(data[4:])

        try:
            self.ids=np.array(data[:,0]).astype('int')
        except:
            print("Problem in",filename)
            print(data)
        self.ncells=np.array(data[:,1]).astype('int')
        self.Mgas=self.ds.arr(np.array(data[:,2]).astype('float'),'Msun')
        self.Mdust=self.ds.arr(np.array(data[:,3]).astype('float'),'Msun')
        self.Mmetal=self.ds.arr(np.array(data[:,4]).astype('float'),'Msun')
        self.Mstar=self.ds.arr(np.array(data[:,5]).astype('float'),'Msun')
        self.pos=self.ds.arr(data[:,6:9].astype('float'),'cm')
        self.r_center=self.ds.arr(np.linalg.norm(self.pos-self.ds.arr([0.5,0.5,0.5],'code_length'),axis=1),'code_length')
        self.V=self.ds.arr(data[:,9:12].astype('float'),'km/s')
        self.v=self.ds.arr(np.linalg.norm(self.V.v,axis=1),'km/s')
        self.Tmin=self.ds.arr(np.array(data[:,13]).astype('float'),'K')
        self.Tmax=self.ds.arr(np.array(data[:,12]).astype('float'),'K')
        self.volume=self.ds.arr(np.array(data[:,14]).astype('float'),'code_length**3')
        self.I=np.reshape(np.array(data[:,15:24]).astype('float'),(len(self.ids),3,3))
        #self.I[self.I<1E-6]=0  #Set very small values to zero to avoid nan in the axis ratios

        self.mtracer=self.ds.quan(9.210865171374272E-009,'code_mass').in_units('Msun')

        if load_full:
            self.compute_radial_quantities()
            self.add_evolution_information()
            self.categorise_clumps()
            self.compute_axis_ratios()


    def compute_radial_quantities(self):

        #relative to the sink
        self.sink=sinks.read(self.output,self.folder,silent=True)[0]

        self.R_sink=self.pos - self.sink.pos.in_units('cm')  #Radial vector
        self.r_sink=self.ds.arr(np.linalg.norm(self.R_sink,axis=1),'cm').in_units('kpc') #Magnitude of radial vector
        #Calculate the radial velocity vector
        vec=[]
        mag=[]
        for r,v in zip(self.R_sink/self.r_sink[:,np.newaxis],self.V):
            vec.append(np.cross(r,v))
            mag.append(np.dot(r,v))
        self.V_r_sink=self.ds.arr(vec,'km/s')
        self.v_r_sink=self.ds.arr(mag,'km/s')


        #Relative to the center
        self.R_center=self.pos.in_units('code_length') - self.ds.arr([0.5,0.5,0.5],'code_length')
        self.r_center=self.ds.arr(np.linalg.norm(self.R_center.in_units('cm'),axis=1),'cm') #Magnitude of radial vector
        #Calculate the radial velocity vector
        vec=[]
        mag=[]
        for r,v in zip(self.R_center/self.r_center[:,np.newaxis],self.V):
            vec.append(np.cross(r,v))
            mag.append(np.dot(r,v))
        self.V_r_center=self.ds.arr(vec,'km/s')
        self.v_r_center=self.ds.arr(mag,'km/s')

    def compute_axis_ratios(self):
        s=[] #Minor axis
        q=[] #Median axis
        axes=[]
        values=[]
        for i,id in enumerate(self.ids):
            vals,vecs=self.get_clump_axis_ratios(id)
            s.append(vals[0])
            q.append(vals[1])
            axes.append(vecs)
            values.append(vals)
        self.minor_axis=np.array(s)
        self.median_axis=np.array(q)
        self.axis_vectors=np.array(axes)  # in order minor, median, major
        self.axis_values=np.array(values)

    def get_clump_axis_ratios(self,index):
        I=self.get_index_property(index,'I')
        vals, vecs = np.linalg.eigh(I)
        #Put the major axis first and resort vectors
        vals=np.sqrt(vals)
        vecs=vecs.transpose()
        if vals[-1]==0:
            vals=np.ones(len(vals))*np.nan
        vals=vals/vals[-1]
        return vals,vecs

    def categorise_clumps(self):
        self.axes=Axes(self.output,self.folder)
        self.axes.categorise_clumps()

    def add_grids(self):
        self.grids=Grid_clumps(self.output,self.folder)

    def get_category_attribute(self,attribute,category):
        try:
            self.axes
        except:
            self.categorise_clumps()
        if hasattr(self,attribute):
            return getattr(self,attribute)[getattr(self.axes,category)]
        elif hasattr(self.axes,attribute):
            return getattr(self.axes,attribute)[getattr(self.axes,category)]
        else:
            print("Cannot find attribute",attribute)
            return

    def get_profile(self,x,y,bins=[],nbins=10,x_unit=None):
        x_data=getattr(self,x)
        if x_unit:
            x_data=x_data.in_units(x_unit)
        if len(bins)==0:
            bins=np.linspace(x_data.min(),x_data.max(),nbins)
        else:
            bins=np.array(bins)

        vals,hist_bins=np.histogram(x_data,weights=getattr(self,y),bins=bins)
        return vals,hist_bins

    def get_index_property(self,index,prop):
        return getattr(self,prop)[self.ids==index][0]

    def add_evolution_information(self,parent=True,cond_step=0):
        if parent:
            filename=self.folder+'/Data/'+'evolution_tree_{0}.csv'.format(str(self.output).zfill(5))
        else:
            filename=self.folder+'/Data/'+'future_evolution_tree_{0}.csv'.format(str(self.output).zfill(5))

        #Load data and preprocess
        with open(filename) as f:
            reader=csv.reader(f)
            next(reader)
            next(reader)

            time_steps=np.array(next(reader)[1:]).astype('float')
            self.time=self.ds.quan(time_steps[0],'Myr')
            self.time_steps=self.ds.arr(np.abs(time_steps[1:]-self.time.v)*1E6,'yr')
            data={}
            condensation=[]
            recycled=[]
            ntracer=[]
            parent_ids=[]
            parent_counts=[]
            main_parent=[]
            for row in reader:
                nclumps=int(len(row[1:])/2)
                clump_id=int(row[0])

                #Extract quantites => file evolution is in the same order as ids here
                data[clump_id]=np.reshape(np.array(row[1:]).astype('int'),(2,nclumps))
                ids=data[clump_id][0,:]
                counts=data[clump_id][1,:]
                ntracer.append(counts[0]+sum(counts[3:]))
                recycled.append(counts[0])
                condensation.append(counts[ids==-1*cond_step][0])
                parent_ids.append(ids)
                parent_counts.append(counts)
                if len(ids)>4:
                        #Which clump do most of the tracers particles come from?
                        #Only zero when there is NO parent at all
                    main_parent.append(ids[ids>0][np.argmax(counts[ids>0])])
                    #main_parent.append(ids[counts>0][np.argmax(counts[counts>0])])
                else:
                    main_parent.append(0)
                    if not(np.array_equal(ids,[-10,-2,-1,0])):
                        print("Odd parent ids?",ids)

        #Safe data for future use
        self.dt=self.time_steps[cond_step]
        self.mtracer=self.ds.quan(9.210865171374272E-009,'code_mass').in_units('Msun')
        if parent:
            self.evolution=data
            self.ntracer_recy=np.array(recycled)
            self.ntracer_fromhot=np.array(condensation)

            filename=self.folder+'/Data/'+'evaporating_clump_tracers_{0}.txt'.format(str(self.output).zfill(5))
            with open(filename) as f:
                reader=csv.reader(f)
                next(reader)
                next(reader)
                next(reader)
                data=[np.array(row).astype('float')[1:] for row in reader]
            self.parent_evap_counts=data
            self.parent_ntracer_evap=np.array([sum(row) for row in data])

            self.ntracer_exchange=self.ntracer_fromhot-self.parent_ntracer_evap
            self.ntracer_cond=self.ntracer_exchange
            self.ntracer_cond[self.ntracer_cond<0]=0

            #condensation contains the net number of new tracers, i.e. from hot - evaporated from parent
            self.Mgas_exchange=self.ntracer_exchange*self.mtracer
            self.Mgas_cond=self.ntracer_cond*self.mtracer
            self.f_cond=self.Mgas_cond/self.Mgas
            self.ntracer=np.array(ntracer)
            self.Mtracer=self.ntracer*self.mtracer
            self.parent_ids=np.array(parent_ids)
            self.parent_counts=np.array(parent_counts)
            self.main_parent=np.array(main_parent)
            self.Mgasdt_cond=self.Mgas_cond/self.time_steps[cond_step]
            try:
                self.r_cond=np.abs(self.r_sink-(self.v_r_sink*self.dt.in_units('s')).in_units('kpc')).in_units('kpc')
            except:
                foo=0

        else:
            self.ntracer_evap=np.array(condensation)
            self.ntracer_bound=np.array(recycled) #Tracers that end up in stars or the BH
            self.ntracer=np.array(ntracer)
            self.Mgas_evap=self.ntracer_evap*self.mtracer
            self.f_evap=self.Mgas_evap/self.Mgas
            self.child_ids=np.array(parent_ids)
            self.child_counts=np.array(parent_counts)
            self.child_evap=np.array([self.ntracer_evap[i]*
                     self.child_counts[i][self.child_ids[i]>0]/
                     sum(self.child_counts[i][self.child_ids[i]>0]) for i
                     in range(len(self.ntracer))])
            self.main_child=np.array(main_parent)
            self.Mgasdt_evap=self.Mgas_evap/self.time_steps[cond_step]

    def calculate_deltas(self):
        '''Calculate property change for parent'''

        try:
            self.r_sink
        except:
            self.compute_radial_quantities()

        try:
            self.main_parent
        except:
            self.add_evolution_information(parent=True)

        old=YTclumps(self.output-1,self.folder)
        old.compute_radial_quantities()
        r_old=[]
        m_old=[]
        mstar_old=[]
        for parent in self.main_parent:
            try:
                r_old.append(old.get_index_property(index=parent,prop='r_sink'))
                m_old.append(old.get_index_property(index=parent,prop='Mgas'))
                mstar_old.append(old.get_index_property(index=parent,prop='Mstar'))
            except:
                r_old.append(np.nan)
                m_old.append(np.nan)
                mstar_old.append(np.nan)
        self.dr=self.r_sink-self.ds.arr(r_old,'code_length')
        self.dr_dt=self.dr.in_units('km')/self.dt.in_units('s')
        self.Mgas_parent=self.ds.arr(m_old,'Msun')
        self.dMgas=self.Mgas-self.ds.arr(m_old,'Msun')
        self.dMgas_dt=self.dMgas/self.dt.in_units('yr')
        self.Mstar_parent=self.ds.arr(mstar_old,'Msun')
        self.dMstar=self.Mstar-self.ds.arr(mstar_old,'Msun')
        self.dMstar_dt=self.dMstar/self.dt.in_units('yr')

        self.Mtot_parent=self.Mgas_parent+self.Mstar_parent
        self.dMtot=self.dMgas+self.dMstar
        self.dMtot_dt=self.dMtot/self.dt.in_units('yr')

    def get_filament_ids(self,ncells=300,cut=0.5):
        try:
            self.axes.filaments
        except:
            self.categorise_clumps()
        return(self.ids[self.axes.filaments])

    def get_orphan_ids(self):
        try:
            self.orphans
        except:
            self.add_ages()
        return(self.ids[self.orphans])

    def add_ages(self):
        filename=self.folder+'/Data/'+'ages_clumps_{0}.csv'.format(str(self.output).zfill(5))
        with open(filename) as f:
            reader=csv.reader(f)
            time=float(next(reader)[1].split()[0])
            next(reader)
            next(reader)
            data=[row for row in reader]
        self.time=time
        self.age=np.array([float(row[1]) for row in data])
        self.formation_output=np.array([int(row[2]) for row in data])
        self.formation_time=np.array(self.time-self.age)
        self.orphans=np.array([int(row[3]) for row in data]).astype('bool')
        self.childless=np.array([int(row[4]) for row in data]).astype('bool')
        self.ancestors=np.array([np.array(row[5:]).astype('int') for row in data])
        self.one_hit_wonder=np.logical_and(self.orphans,self.childless)  #Clumps that only exist in this output
        self.enduring=np.logical_and(self.orphans,~self.childless)  #Clumps that continue to exist afterwards
        self.ending=np.logical_and(self.childless,~self.orphans)    #Enduring clumps that are found here for the last time

    def get_timeseries(self,attribute,all_data):
        try:
            self.ancestors
        except:
            self.add_ages()

        try:
            times=np.array([ts.times for ts in self.ts])
        except:
            self.ts=np.array([Timeseries(self.output,ancestors,all_data) for ancestors in self.ancestors])
            times=np.array([ts.times for ts in self.ts])

        data=np.array([ts.get_data(attribute,all_data) for ts in self.ts])
        return times,data

    def plot_clump_attribute(self,obj,attribute):
        obj.set_field_parameter('ppclumps',self)
        obj.set_field_parameter('clump_field',attribute)
        try:
            field_unit=str(getattr(self,attribute)[0].units)
        except:
            field_unit='dimensionless'
        self.ds.add_field(('clump',attribute),units=field_unit,function=clump_field,force_override=True,particle_type=False)

        #When using a cut region, make sure the parent also has the relevant clump parameters
        #Somehow they get reloaded during generation process and then can be missing
        clumps=obj.cut_region('obj["hydro_relevant_clump_id"]>0')
        clumps.set_field_parameter('ppclumps',self)
        clumps.set_field_parameter('clump_field',attribute)
        return clumps

class Timeseries(object):
    def __init__(self,output,ancestors,all_data):
        self.output=output
        self.ancestors=ancestors
        self.outputs=np.array(range(output-len(ancestors),output))+1
        self.alldata_index=np.where(np.array([ad.output for ad in all_data])==output)[0][0]
        self.times=np.array([all_data[self.alldata_index-i].time for i in range(len(ancestors))[::-1]])

    def get_data(self,attribute,all_data):
        try:
            return getattr(self,attribute)
        except:
            data=np.array([all_data[self.alldata_index-i].get_index_property(a,attribute) for i,a in enumerate(self.ancestors[::-1])])
            setattr(self,attribute,data[::-1])
        return getattr(self,attribute)

class Clump_profile(object):
    '''
    Properties of filaments along the filaments major axis
    Written using make_clump_profile'''

    def __init__(self,clump_id,output,folder='.'):
        filename=folder+'/Clump_profiles/clump_profiles_{0}_{1}.csv'.format(str(output).zfill(5),str(clump_id).zfill(5))
        self.output=output
        self.id=clump_id

        if not(os.path.exists(filename)):
            print("File does not exist")

        with open(filename) as f:
            reader=csv.reader(f)
            next(reader)
            self.r_min=float(next(reader)[1])
            self.central=self.r_min<0.5
            line=next(reader)
            self.ends=np.array(line[1:]).astype('float')
            data=np.array([row for row in reader]).astype('float')

        self.length=data[0,:]  #in kpc
        self.density=data[1,:] #in Hcc
        self.mass=data[2,:]   #in Msun
        self.metallicity=data[3,:]
        self.dust=data[4,:]
        self.temperature=data[5,:]

def inertia_tensor(pos,mass,center):
    ''' Calculate the reduced inertia tensor according to Gerhard (1983) / Bailin
    and Steinmetz (2005)'''
    #Center the positions on the given center and calculate radii
    pos=pos-center
    r2=np.linalg.norm(pos,axis=1)**2  #Get the radii of cells

    #Exclude r=0 to avoid errors
    r0=~(r2==0)
    pos=pos[r0]
    mass=mass[r0]
    r2=r2[r0]

    ndim=len(pos[0])

    I_xx = np.sum(mass * pos[:,0]**2 / r2)
    I_yy = np.sum(mass * pos[:,1]**2 / r2)
    I_xy = np.sum(mass * pos[:,0]*pos[:,1] / r2)

    if ndim==3:
        I_xz = np.sum(mass * pos[:,0]*pos[:,2] / r2)
        I_zz = np.sum(mass * pos[:,2]**2 / r2)
        I_yz = np.sum(mass * pos[:,1]*pos[:,2] / r2)
        I = np.matrix([[I_xx, I_xy, I_xz], \
                    [I_xy, I_yy, I_yz], \
                    [I_xz, I_yz, I_zz]], dtype=np.float64)
    else:
        I = np.matrix([[I_xx,I_xy],\
                      [I_xy,I_yy]],dtype=np.float64)
    return I

class Profile(object):
    '''Profile quantities, produced using 2021_03_23_parallel_postprocess.py
        Each profile is for a single output, a single object shape, and a single phase (hot, warm, cold or all)
        Twarm=2E5
        Tcold=2E4
    '''

    def __init__(self,output,df,radius=150,phase='',shape='sphere'):

        self.df=df
        self.time=ytutils.load(output,df,silent=True).current_time.in_units('Myr')
        self.radius=radius

        if phase=='':
            self.all=True
        else:
            self.phase=phase

        filename=df+'/Data/'+f'profile_{str(output).zfill(5)}_{shape}'

        if shape=='sphere':
            filename+=f'_{radius}kpc'
        if phase in ['warm','hot','cold']:
            filename+='_'+phase

        self.mean=Table.read(filename+'_mean.ecsv', format='ascii.ecsv')
        self.sum=Table.read(filename+'_sum.ecsv', format='ascii.ecsv')

        empty=self.mean['density']==0

        self.mean=self.mean[~empty]
        self.sum=self.sum[~empty]

        if len(set(self.mean.keys())&set(self.sum.keys()))>1:
            print("There are fields in both profiles! Please code up a fix for this",set(prof.mean.keys())&set(prof.sum.keys()))

        for field in self.mean.keys():
            setattr(self,field,self.mean[field])
        for field in self.sum.keys():
            setattr(self,field,self.sum[field])

        self.total_baryon_mass=self.star_mass+self.cell_mass
        self.contained_stellar_mass=np.cumsum(self.star_mass)
        self.contained_gas_mass=np.cumsum(self.cell_mass)

        self.fields=list(self.mean.keys())+list(self.sum.keys())


class Timeseries(object):
    def __init__(self,df,shape='sphere',radius=150):
        self.df=df
        self.outputs=np.array(ytutils.find_outputs(df))[:5]
        for phase in ['hot','warm','cold','']:
            data=np.array([Profile(o,df,phase=phase,shape=shape) for o in self.outputs])
            exists=np.array([len(p.radius)>0 for p in data])
            if phase=='':
                name='all'
            else:
                name=phase
            setattr(self,name,data[exists])


        #Remove empty data
        self.outputs=self.outputs[exists]

    def get(self,phase,field,radius):
        self.time=np.array([p.time.v for p in self.hot])


        return np.array([getattr(p,field)[np.argmin(np.abs(p.radius.to('kpc')-radius*kpc))] for p in getattr(self,phase)])
