import horizon
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.lines as mlines

all_redshifts=[3,2,1,0.01]
out_z={125:'3',197:'2',343:'1',761:'0.01'}
linewidth=2
mpl.rcParams['lines.linewidth']=linewidth
Zmetal=0.0134  #Following 
linestyles={'AGN':'-','noAGN':'--'}
delta_x=1     #in kpc
species=['H','O','Fe','C','N','Mg','S']
alphas={'AGN':1.0,'noAGN':0.5}

def add_legend(ax):
    lines=[mlines.Line2D([],[],linestyle=linestyles[simulation],label=simulation,color='k') for simulation in ['AGN','noAGN']]
    ax.legend(handles=lines,loc=1,frameon=False)
    
def massfunctions(output,data=None):
    fig,(ax1,ax2)=plt.subplots(2,1,sharex=True)

    if not(data):
        data=Output(output)
    
    for color,equivalent in zip(['r','k'],[True,False]):
        for simulation in ['AGN','noAGN']:
            masses=data.get_data('Mstar',simulation,equivalent=equivalent)
            bins=horizon.make_bins(masses,nbins=20,min_bin=1E9,maxbinNumber=1)
            plot_bins=(bins[1:]+bins[:-1])/2
            counts, bins=np.histogram(masses,bins)

            plot_bins=np.log10(plot_bins)

            ax1.plot(plot_bins,counts,linestyle=linestyles[simulation],color=color)
            ax2=horizon.draw_GMF(ax2,data.get_data('Mstar',simulation,equivalent),linestyle=linestyles[simulation],DM=False,nbins=20,errors=True,color=color)

    #Set scale
    ax1.set_yscale('log')

    #Set legend and axis labels
    add_legend(ax1)
    ax2.set_xlabel('log$_{10}(M)$[M$_\odot$]')
    ax2.set_ylabel('log$_{10}$($\Phi$)[Mpc$^{-3} $dex$^{-1}$]')
    ax1.set_ylabel('number of objects')
    
    #Save
    filename='equivalent_mass_function_{0}'.format(data.AGN.output_forfile)
    horizon.saveas(fig,filename,png=False,addendum=None)

def find_twins(output,ntwins=5,selection='biggest'):
    #Finds the biggest,smallest or the equivalent twins

    pair=horizon.Pair(output)
    twins=pair.get_twins('AGN.mark.Mgas')

    masses=pair.get_data('AGN.halo.mvir')

    
    masssorted_twins=np.array([twin for (mass,twin) in sorted(zip(masses,twins))] )
    masssorted_masses=np.array([mass for (mass,twin) in sorted(zip(masses,twins))])
    
    if selection=='biggest':
        return masssorted_twins[-1*ntwins:]
    elif selection=='smallest':
        return masssorted_twins[:ntwins]
    else:
        print "I do not understand", selection
        return
    return


def plot_profiles(data,mass,equivalent=True):

    fig,ax=plt.subplots()

    colour_l=horizon.pick_colour(2,[1,2])
    colour_r=horizon.pick_colour(1,[1,2])

    error=0.2
    for simulation in ['AGN','noAGN']:
        Mstar=data.get_data('Mstar',simulation,equivalent=equivalent)
        if equivalent:
            galaxies=getattr(data,simulation).equivalent
        else:
            galaxies=getattr(data,simulation).all_galaxies
        bin_galaxies=galaxies[(Mstar>mass*(1-error))*(Mstar<mass*(1+error))]  #Pick galaxies within 10 % of mass cut
        print("There are",len(bin_galaxies),"suitable galaxies in",simulation)
        
        i=0
        j=0

        #bin_galaxies=bin_galaxies[::-1]
        while(i<5):
                galaxy=bin_galaxies[j]
                try:
                    galaxy.add_profiles(simulation=simulation,output=getattr(data,simulation).output)
                    if galaxy.Profile_points>8:
                        print("FOUND ONE")
                        ax.plot(galaxy.profiles['rphysA'],galaxy.profiles['dzmetA'],color=colour_l,linestyle=linestyles[simulation],marker='x',alpha=1.0)
                        i+=1
                    j+=1
                except:
                    j+=1
            #ax2=ax.twinx()
            #ax2.plot(getattr(twin,simulation).mark.profiles['rphysA'],getattr(twin,simulation).mark.profiles['rhogA'],color=colour_r,linestyle=linestyles[simulation],marker='x',alpha=1.0,linewidth=1)
            #ax2.set_yscale('log')
            #ax2.set_ylabel('rhogA',color=colour_r)
    

    #Axis labels and such
    ax.set_ylabel('dZ/dr',color=colour_l)
    ax.set_xlabel('radius [kpc]')
    ax.set_xlim([5,100])
    add_legend(ax)

    #Legend
    #legendlines=[]; labels=[]
    #for simulation in ['AGN','noAGN']:
    #    legendlines.append(mlines.Line2D([],[],color='k',label=simulation,linestyle=linestyles[simulation]))
    #    labels.append(simulation)

    #new_legend=axes[0].legend(legendlines,labels,frameon=False,loc=1)
    #axes[0].add_artist(new_legend)

    filename='Profiles_mass{0}'.format(horizon.sci(mass))
    horizon.saveas(fig,filename)
    

def plot_profile(twins,addendum=None,x='rphysA',y='dzmetA',y2='drhodmA'):    
    fig,axes=plt.subplots(len(twins),1,sharex=True,figsize=(8,3*len(twins)))

    axes2=[ax.twinx() for ax in axes]

    for twin,ax,ax2 in zip(twins,axes,axes2):
        print twin, ax
        colour_l=horizon.pick_colour(2,[1,2])
        colour_r=horizon.pick_colour(1,[1,2])
        for simulation in ['AGN','noAGN']:
            halo_mark=getattr(twin,simulation).mark
            halo_mark.add_profiles()
            data_x=halo_mark.profiles[x]
            data_y=halo_mark.profiles[y]
            ax.plot(data_x,data_y,color=colour_l,linestyle=linestyles[simulation],marker='x',alpha=1.0)
            ax.set_ylabel(y,color=colour_l)
            ax.set_yscale('log')
            if y2:
                data_y2=halo_mark.profiles[y2]
                print data_y2
                ax2.plot(data_x,data_y2,color=colour_r,linestyle=linestyles[simulation],marker='x',alpha=1.0,linewidth=1)
                ax2.set_yscale('log')
                ax2.set_ylabel(y2,color=colour_r)
            

    legendlines=[]; labels=[]
    for simulation in ['AGN','noAGN']:
        legendlines.append(mlines.Line2D([],[],color='k',label=simulation,linestyle=linestyles[simulation]))
        labels.append(simulation)

    new_legend=axes[0].legend(legendlines,labels,frameon=False,loc=1)
    axes[0].add_artist(new_legend)

    axes[-1].set_xlabel(x)
    plt.tight_layout()
    
    
    filename='metalProfiles_{0}_{1}'.format(x,y)
    if y2:
        filename+='_{0}'.format(y2)
    if addendum:
        filename+='_{0}'.format(addendum)
    filename+='.pdf'
    print "Saving as",filename
    plt.savefig(filename)



def virial_values(redshifts=all_redshifts,x='halo.mvir',xlabel=None,y='mark.Zgas.Zave',quartile=False,ylabel=None,average=False):
    fig,ax=plt.subplots()

    legendlines=[]; labels=[]
    for redshift in redshifts:
        colour=horizon.pick_colour(redshift,redshifts)
        legendlines.append(mlines.Line2D([],[],color=colour,linestyle='-'))
        labels.append('Z{0}'.format(round(redshift)))  
        output=horizon.find_output_for_redshift(redshift)
        pair=horizon.Pair(output)

        for simulation in ['AGN','noAGN']:
            ydata=pair.get_data(simulation+'.'+y)
            if (y.split('.')[-1]=='Ztot' or y.split('.')[-1] in species) and average:
                ydata=ydata/getattr(pair,simulation).Zbox
            masses=pair.get_data(simulation+'.'+x)
            horizon.plot_vs_mass(ax,masses=masses,data=ydata,quartile=quartile,colour=colour,linestyle=linestyles[simulation])

    for simulation in ['AGN','noAGN']:
        legendlines.append(mlines.Line2D([],[],color='k',label=simulation,linestyle=linestyles[simulation]))
        labels.append(simulation)  

    ax.set_xscale('log')
    ax.set_yscale('log')
    if not(xlabel):
        xlabel=x

    if not(ylabel):
        ylabel=y
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    #Create legend that sits outside the plot
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.2,
                     box.width, box.height * 0.8])
    new_legend=plt.legend(legendlines,labels,frameon=False,loc='upper center', bbox_to_anchor=(0.5, -0.15),ncol=2)
    ax.add_artist(new_legend)

    plotname="virial_{0}_{1}_{2}".format(x,y,len(redshifts))
    if quartile:
        plotname+='_quartile'
    if average:
        plotname+='_average'
    horizon.saveas(fig,plotname)

def radii(output,metallicities=['Z005','Z010','Z020','Z050'],x='halo.mvir',addendum=None,xlabel=None,quartile=False,NO=False,rvir=False):
    fig,ax=plt.subplots()
    pair=horizon.Pair(output)

    legendlines=[]; labels=[]
  #  for simulation in ['AGN','noAGN']:
   #     rvirs=pair.get_data('{0}.mark.Zgas.rvir_kpc'.format(simulation))
   #     masses=pair.get_data(simulation+'.'+x)
   #     horizon.plot_vs_mass(ax,masses=masses,data=rvirs,quartile=quartile,colour=horizon.colour_1(),linestyle=linestyles[simulation],error=not(quartile))
   #     legendlines.append(mlines.Line2D([],[],color='k',label=simulation,linestyle=linestyles[simulation]))
   #     labels.append(simulation)

    for i,metallicity in enumerate(metallicities):
        colour=horizon.pick_colour(metallicity,metallicities)
        for simulation in ['noAGN','AGN']:
            if NO and simulation=='AGN':
                metallicity+='NO'
            xdata=pair.get_data('{1}.mark.{0}.r_kpc'.format(metallicity,simulation))
            rvirs=pair.get_data('{0}.mark.Zgas.rvir_kpc'.format(simulation))
            masses=pair.get_data(simulation+'.'+x)
            if rvir:
                xdata=xdata/rvirs
            masses=pair.get_data(simulation+'.'+x)

            if i==0:
                if rvir:
                    ax.plot(1.0,linestyle=':',color=horizon.colour_1())
                else:
                    horizon.plot_vs_mass(ax,masses=masses,data=rvirs,quartile=quartile,colour=horizon.colour_1(),linestyle=linestyles[simulation],error=not(quartile),nbins=10,alpha=alphas[simulation])

                legendlines.append(mlines.Line2D([],[],color='k',label=simulation,linestyle=linestyles[simulation],alpha=alphas[simulation]))
                labels.append(simulation)

            horizon.plot_vs_mass(ax,masses=masses,data=xdata,quartile=quartile,colour=colour,linestyle=linestyles[simulation],error=not(quartile),alpha=alphas[simulation],nbins=10)
        legendlines.append(mlines.Line2D([],[],color=colour,label=metallicity[:5]))
        labels.append(metallicity[:5])

    if rvir:
        linestyle=':'
    else:
        linestyle='-'
    legendlines.append(mlines.Line2D([],[],color=horizon.colour_1(),label='Virial radius',linestyle=linestyle))
    labels.append('Rvir')
    ax.axhline(delta_x,linestyle=":",color=horizon.colour_1())

    if not xlabel:
        xlabel=x
    ax.set_xlabel(xlabel)
    ax.set_xscale('log')
    ax.set_yscale('log')
    if rvir:
        ax.set_ylabel('radius [rvir]')
    else:
        ax.set_ylabel('radius [kpc]')
    if NO:
        title="redshift {0}: noAGN average".format(out_z[output])
    else:
        title="output {0}: own average".format(out_z[output])
    plt.title(title)
    
    #Create legend that sits outside the plot
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.2,
                     box.width, box.height * 0.8])
    new_legend=plt.legend(legendlines,labels,frameon=False,loc='upper center', bbox_to_anchor=(0.5, -0.15),ncol=4)
    ax.add_artist(new_legend)

    plotname='Zradii_{2}_{0}_{1}'.format(len(metallicities),x,output)

    #mpl.rcParams['lines.linewidth']=1
    if quartile:
        plotname+='_quartile'
    if rvir:
        plotname+='_rvir'
    if NO:
        plotname+='NO'
    horizon.saveas(fig,plotname,addendum=addendum)



def sample_distribution(redshifts=all_redshifts,x='mark.Zgas.Ztot',xlabel=None,addendum=None,average=False,cummulative=False):
    #Probability density
    fig,ax=plt.subplots()
    split_x=x.split('.')

    for redshift in redshifts:
        output=horizon.find_output_for_redshift(redshift)
        pair=horizon.Pair(output)

        colour=horizon.pick_colour(redshift,redshifts)
        for simulation in ['AGN','noAGN']:
            xdata=pair.get_data(simulation+'.'+x)
            if average:
                if not('Zave' in split_x or 'ZcellMAX' in split_x):
                    xdata=xdata/getattr(pair,simulation).Zbox
            else:
                if not('Ztot' in split_x):
                    xdata=xdata*getattr(pair,simulation).Zbox
                ax.axvline(Zmetal,color=horizon.colour_1(),linewidth=1)
                ax.axvline(getattr(pair,simulation).Zbox,linewidth=1,linestyle=linestyles[simulation],color=colour)
            xdata=xdata[np.logical_not(np.isnan(xdata))]
            bins=np.logspace(np.log10(xdata[xdata>0].min()),np.log10(xdata.max()),40)
            counts,tmp=np.histogram(xdata,bins=bins)
            if cummulative:
                counts=np.cumsum(counts[::-1])[::-1]
            plot_bins=(bins[1:]+bins[:-1])/2

            ax.plot(plot_bins,counts/float(len(xdata)),color=colour,linestyle=linestyles[simulation],label="{0} z={1}".format(simulation,redshift))       
    if not xlabel:
        xlabel=x
        if average:
            xlabel+=" [average metallicity]"
        else:
            xlabel+=" [absolute metallicity]"
            
    ax.set_xlabel(xlabel)
    ax.set_ylabel('% sample')
    ax.set_xscale('log')
    #ax.set_yscale('log')
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.2,
                     box.width, box.height * 0.8])

    # Put a legend below current axis
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),
              frameon=False,  ncol=2)
    ax.set_ylim([1E-5,1E-1])
    plotname="metalsample_{0}_{1}".format(x,len(redshifts))
    if average:
        plotname+='_average'
    horizon.saveas(fig,plotname,addendum=addendum)        


###########################
#Objects

class Output(object):
    def __init__(self,output):
        self.AGN=Simulation(output,simulation='AGN')
        self.noAGN=Simulation(horizon.pairup_output(output,DM=False),simulation='noAGN')

    def get_data(self,name,simulation,equivalent=True):
        return getattr(self,simulation).get_data(name,equivalent=equivalent)


class Simulation(object):
    def __init__(self,output,simulation):
        self.output=output
        self.output_forfile=str(output).zfill(3)

        #Load all galaxies                                                                                                         
        foldername="./Datasets/MARK/{0}".format(simulation)
        filename=foldername+"/HaloMasses_{0}.csv".format(self.output_forfile)
        with open(filename) as f:
            reader=horizon.csv.reader(f)
            next(reader)
            halo_lines=np.array([row for row in reader])                #Read the Halos from file                               
            galaxies=[horizon.Halo_Mark(line,order=i+2) for i,line in enumerate(halo_lines)]
        self.all_galaxies=np.array(galaxies)

        self.all_galaxies=np.array(galaxies)
        self.select_equivalent_galaxies()

    def add_more_data(self):
        #Add SMBH data
        if simulation=='AGN':         #Read the BH information as well                                                             
            filename=foldername+"/BigBH_{0}.csv".format(self.output_forfile)
            print("Opening file",filename)
            with open(filename) as f:
                reader=horizon.csv.reader(f)
                next(reader)
                sink_lines=np.array([row for row in reader])
            [gal.add_sink(sink_line) for gal,sink_line in zip(galaxies,sink_lines)]

        #Add galaxy data
        filename=foldername+"/GalaxyMasses_{0}.csv".format(self.output_forfile)
        print("Opening",filename)
        with open(filename) as f:
            reader=horizon.csv.reader(f)
            next(reader)
            galaxy_lines=np.array([row for row in reader])
        [gal.add_galaxy(galaxy_line) for gal,galaxy_line in zip(galaxies,galaxy_lines)]

        self.all_galaxies=np.array(galaxies)
        self.select_equivalent_galaxies()

    def select_equivalent_galaxies(self):
        Mdm0=3.5E11
        Mstar0=2.5E10
        p1=1.9
        p2=0.8

        Mdm=self.get_data('Mdm',equivalent=False)
        Mstar=self.get_data('Mstar',equivalent=False)

        error_range=0.2

        Mdm=Mdm/Mdm0
        Mout=[2*Mscale/(1/Mdm**p1+1/Mdm**p2) for Mscale in Mstar0*np.array([1-error_range,1+error_range])]

        indices=(Mstar>Mout[0])*(Mstar<Mout[1])*(Mstar<1E12)
        self.equivalent=self.all_galaxies[indices]


    def get_data(self,name,equivalent=True):
        split_name=name.split('.')
        if equivalent:
            data=self.equivalent
        else:
            data=self.all_galaxies
        for component in split_name:
            data=np.array([getattr(galaxy,component) for galaxy in data])
        return data
