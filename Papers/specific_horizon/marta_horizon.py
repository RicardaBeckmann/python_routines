import numpy as np
import matplotlib.pyplot as plot
import horizon as h
import timeseries_horizon as t
import os
import sys
import csv

foldername='./for_Marta'

def ratios_eddingtonsplit(redshifts=h.all_redshifts,BHmass=False):
    ax=h.ratios_mass(redshifts=[],BHmass=BHmass,saveit=False,stars=True)
    ax.show()

def sample_overlap(output):
    ''' Calculate the overlap between my sample and Marta's sample'''
    pair=h.Pair(output)
    twins=pair.get_sink_twins()
    R_gal_ids=np.array([twin.AGN.halo.id for twin in twins])
    R_bh_ids=np.array([twin.AGN.mark.sink.id for twin in twins])
    print len(R_gal_ids),len(R_bh_ids)
    
    BHs=read_file(pair.AGN.ds.z)
    print len(BHs)
    matched=np.array([BH  for BH in BHs if R_bh_ids[R_gal_ids==BH.IDgal]==BH.IDbh])
    summary="#Objects Marta: {0}    #Objects Ricarda: {1}    #Objects Matched: {2}    {3}%".format(len(BHs),len(R_gal_ids),len(matched),round(1.0*len(matched)/len(R_gal_ids),2))
    print summary
    return matched

def sample_scatter(redshift,matched=True,addendum=None):
    '''Plot Martas sample, my sample and the matched sample on the same scatter plot'''
    fig,ax=h.plt.subplots()

    output=h.find_output_for_redshift(redshift)

    #Plot my sample
    ax=h.scatter_mass_mass(output,x='Mstar',y='Mbh',saveit=False,ax=ax)

    #Plot Marta's sample
    BHs=read_file(redshift)
    Mstars=[BH.Mstar for BH in BHs]
    Mbhs=[BH.Mbh for BH in BHs]
    ax.scatter(Mstars,Mbhs,marker='x',color="DarkBlue")


    #Plot matched
    if matched:
        matched_BHs=sample_overlap(output)
        Mstars=[BH.Mstar for BH in matched_BHs]
        Mbhs=[BH.Mbh for BH in matched_BHs]
        ax.scatter(Mstars,Mbhs,marker='x',color=h.colour_1())

    figname="sample_comparison_{0}".format(output)
    if matched:
        figname+="_matched"
    h.saveas(ax,figname,addendum)    

def read_file(redshift=None,foldername=foldername):
    output=find_output_for_redshift(redshift)
    foldername+='/halogal/'
    filename=foldername+"outt{0}_halo_gal_centralBHs_2reff".format(str(output).zfill(5))
    print "Opening",filename
    with open(filename) as f:
        reader=csv.reader(f)
        BHs=np.array([BH(row[0].split()) for row in reader])
    print "There are",len(BHs),"objects."
    return BHs

def find_output_for_redshift(z,foldername=foldername):
    foldername+='/halogal/'
    files=os.listdir(foldername)
    filename='outt'
    outputs=np.array([int(file[len(filename):len(filename)+5]) for file in files if file[:len(filename)]==filename])
    redshifts=np.array([h.load(output,AGN=True,DM=False).z for output in outputs])
    redshifts=abs(redshifts-z)
    print 'Redshift z=',z,"corresponds to Marta's output",outputs[redshifts==redshifts.min()][0]
    return outputs[redshifts==redshifts.min()][0]

             
class BH(object):
    def __init__(self,line):
        self.Mdm=float(line[0])*1E11
        self.Mstar=float(line[1])*1E11
        self.Mbh=float(line[2])*1E8
        self.Edd=float(line[3])
        self.logLbol=float(line[4])
        self.halo_level=int(line[5])
        self.galaxy_level=int(line[6])
        self.IDdm=int(line[7])
        self.IDgal=int(line[8])
        self.IDbh=int(line[9])
