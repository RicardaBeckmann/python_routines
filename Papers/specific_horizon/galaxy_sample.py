#Find the sample of galaxies with more Athan 50 star particles
#Hosten in a halo with more than 500 particles


import horizon
import matplotlib.pyplot as plt
import numpy as np
import sys
import matplotlib.lines as mlines


gal_mass_cut=3636500   #2E8
nstar=100
ndm=500
Elisa=False
radius='1'
redshift=float(sys.argv[1])

def make_histogram(masses,nbins=20,min_bin=gal_mass_cut):
    sorted_masses=sorted(masses)

    min_bin=np.log10(min_bin)
    bins=np.logspace(min_bin,np.log10(sorted_masses[-1]+1),nbins)
    print bins[0],bins[-1]
    print bins
    counts,bins=np.histogram(masses,bins)
    print len(counts),len(bins)
    print counts
    plot_bins=(bins[1:]+bins[:-1])/2
    return counts,plot_bins

#######################

colours=['#d7191c','#fdae61','#2c7bb6','#abd9e9']

fig,ax=plt.subplots()
legendlines=[]
labels=[]

print '%%%%%%%%%%%%%%%%%%'

if Elisa:
    print "USING ELISAS MATCHING"


for AGN in [True,False]:
    if AGN:
        simulation='AGN'
        linestyle='-'
    else:
        simulation='noAGN'
        linestyle='--'

    print '##############'
    output=horizon.find_output_for_redshift(redshift,AGN=AGN)
    
    #Plot all galaxies
    galaxies=horizon.get_output_halos(output,AGN=AGN, DM=False)
    print "XXXXXXXXXXXX","There are",len(galaxies),"galaxies to start with"
    masses=np.array([galaxy.mvir for galaxy in galaxies])
    parts=np.array([galaxy.npart for galaxy in galaxies])
    counts,bins=make_histogram(masses[parts>=nstar])
    colour=colours[0]
    ax.plot(bins,counts,label='All galaxies',color=colour,linestyle=linestyle)
    print "XXXXXXXXXXXX",sum(parts>=nstar),"are above the particle cut of 100"
    print "XXXXXXXXXXXX",counts.sum(),"are above the mass_cut"

    #Use if you want to restrict the number of star particles in galaxies
    #galaxies=[galaxy for galaxy in galaxies if galaxy.mvir>gal_mass_cut]
    
    #In massive enough halos
    halos=horizon.get_output_halos(output,AGN=AGN,DM=True)
    print "XXXXXXXXXXXX","There are",len(halos),"DM halos."
    halos_500_ids=np.array([halo.id for halo in halos if halo.npart>=ndm])
    print "XXXXXXXXXXXX","There are",len(halos_500_ids),"with more than 500 particles"
        
    if Elisa:
        foldername='./Datasets/HaloMatching_Elisa/'
        filename=foldername+'match0p{0}_gal2halo_125.out'.format(radius)
    else:
        foldername="./AGN_VS_NOAGN/"
        filename=foldername+"halo2galaxy"
        if AGN:
            filename+='_AGN'
        else:
            filename+='_noAGN'
        filename+='_{0}.asc'.format(str(output).zfill(3))

    print "Opening",filename
    with open(filename) as f:
        reader=horizon.csv.reader(f,delimiter='\t')
        if not(Elisa):
            next(reader)
            next(reader)
            rows=horizon.np.array([row for row in reader])
            halo_ids=horizon.np.array([int(row[0]) for row in rows])
            galaxy_ids=horizon.np.array([int(row[1]) for row in rows])
        else:
            rows=horizon.np.array([row[0].split() for row in reader])
            halo_ids=horizon.np.array([int(row[0]) for row in rows])
            galaxy_ids=horizon.np.array([int(float(row[1])) for row in rows])
            print "Halos",halo_ids
            print "Galaxies",galaxy_ids
            
    print "XXXXXXXXXXXX","There are",len(halo_ids),"pairings in total" 
    galaxies_with_halos_ids=np.array([galaxy_ids[halo_ids==id][0] for id in halo_ids])
    galaxies_with_halos=np.array([galaxy for galaxy in galaxies if galaxy.id in galaxies_with_halos_ids])
    masses=[galaxy.mvir for galaxy in galaxies_with_halos]
    parts=np.array([galaxy.npart for galaxy in galaxies_with_halos])
    counts,bins=make_histogram(masses)
    colour=colours[1]
    ax.plot(bins,counts,label='In halos',color=colour,linestyle=linestyle)
    print "XXXXXXXXXXXX","There are",counts.sum(),"in halos at all"
    print "XXXXXXXXXXXX",sum(parts>=nstar),"are in halos and above the stellar mass cut"
    
    included_galaxy_ids=horizon.np.array([galaxy_ids[halo_ids==id][0] for id in halo_ids if id in halos_500_ids])
    included_galaxies=horizon.np.array([galaxy for galaxy in galaxies if galaxy.id in included_galaxy_ids])
    masses=[galaxy.mvir for galaxy in included_galaxies]
    counts,bins=make_histogram(masses)
    colour=colours[2]
    ax.plot(bins,counts,label='In massive halos',color=colour,linestyle=linestyle)
    print "XXXXXXXXXXXX","There are",counts.sum(),"in sufficiently massive halos"

    output=horizon.find_output_for_redshift(redshift,AGN=True)
    pair=horizon.Pair(output)
    masses=[getattr(twin,simulation).halo.mvir for twin in pair.twins if getattr(twin,simulation).halo.host.npart >= ndm]
    counts,bins=make_histogram(masses)
    colour=colours[3]
    ax.plot(bins,counts,label='Twinned',color=colour,linestyle=linestyle)

    print "XXXXXXXXXXXX","Twinned objects",counts.sum()

    if AGN:
        legendlines.append(mlines.Line2D([],[],color=colours[0],label='All Galaxies'))
        labels.append('All galaxies')
        legendlines.append(mlines.Line2D([],[],color=colours[1],label='Galaxies with halos'))
        labels.append('In halos')
        legendlines.append(mlines.Line2D([],[],color=colours[2],label='Galaxies with massive halos'))
        labels.append('In massive halos')
        legendlines.append(mlines.Line2D([],[],color=colours[3],label='Twinned galaxies'))
        labels.append('Twinned galaxies')
        legendlines.append(mlines.Line2D([],[],color='k',linestyle='-',label='AGN'))
        labels.append('AGN')
        legendlines.append(mlines.Line2D([],[],color='k',linestyle='--',label='AGN'))
        labels.append('noAGN')

ax.set_xscale('log')
ax.set_yscale('log')
#ax.set_xlim(1E8,1E14)
ax.set_xlabel('Mstar [Msun]')
ax.set_ylabel('Number of objects')
ax.axvline(1E9,color='grey',linewidth=1)
ax.axvline(1E11,color='grey',linewidth=1)
ax.axvline(gal_mass_cut,color='k',linestyle=':',linewidth=1)
new_legend=plt.legend(legendlines,labels,frameon=False,loc=1,fontsize=12)
plt.gca().add_artist(new_legend)
plotname='sample_counts_{0}.pdf'.format(output)
print "Saveas {0}".format(plotname)
plt.savefig(plotname)

            

            
