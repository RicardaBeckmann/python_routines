#Find the number of objects in each galaxy mass category

import horizon

redshifts=horizon.all_redshifts

for DM in [True,False]:
    if DM:
        print "DM"
    else:
        print "STARS"
    for AGN in [True,False]:
        if AGN:
            print "AGN"
        else:
            print "noAGN"
        for redshift in redshifts:
            output=horizon.find_output_for_redshift(redshift,AGN=AGN)
            halos=horizon.get_output_halos(output=output,AGN=AGN,DM=DM)
            if DM:
                nmin=499
            else:
                nmin=49
            massive=[halo.npart for halo in halos if halo.npart>nmin]
            #Counts number of objects with 50 (STARS) or 500 (DM) or more particles
            print redshift,len(massive),len(halos)

for redshift in redshifts:
    print redshift
    pair=horizon.Pair(horizon.find_output_for_redshift(redshift))
    for twins in [pair.twins,pair.get_marks_twins(noAGN=True)]:
        small=len([twin for twin in twins if twin.AGN.halo.mvir < 1E9])
        large=len([twin for twin in twins if twin.AGN.halo.mvir > 1E11])
        #Prints number of galaxies in each mass bin
        print "Galaxy numbers S M L ALL:",small,len(twins)-small-large,large,len(twins)
    

    pair=horizon.Pair(horizon.find_output_for_redshift(redshift),DM=True)
    twins=pair.twins
    both=len([twin for twin in twins if twin.AGN.halo.npart>499 and twin.noAGN.halo.npart>499])
    #Counts number of twins  where both halos have more than 500 particles
    print "Halo numbers:",redshift,both,len(twins)
    
