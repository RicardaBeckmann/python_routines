import horizon
import csv
import numpy as np
import matplotlib.pyplot as plt


output=761
output_forfile=str(output).zfill(3)

#Mstar-Mdm
fig,(ax1,ax2)=plt.subplots(2,1)

pair=horizon.Pair(output)

foldername="./Datasets/MARK/{0}".format('AGN')
filename=foldername+"/HaloMasses_{0}.csv".format(output_forfile)
print "Opening file",filename
with open(filename) as f:
    reader=csv.reader(f)
    next(reader)
    halo_lines=np.array([row for row in reader])                #Read the Halos from file                                                                                               
    halo_ids=np.array([int(line[0]) for line in halo_lines])    #Extract ids only for sorting        

halos=[horizon.Halo_Mark(line) for line in halo_lines]
    
mstars=np.array([halo.Mstar for halo in halos])
mdms=np.array([halo.Mdm for halo in halos])

ax1=horizon.plot_vs_mass(ax1,mdms,mstars,colour='b',quartile=True)
ax1=horizon.plot_vs_mass(ax1,pair.get_data('AGN.mark.Mdm'),pair.get_data('AGN.mark.Mstar'),colour='g',quartile=True)


ax1.set_yscale('log')
ax1.set_xscale('log')
ax1.set_ylabel('Mdm')

#Add sink data
filename=foldername+"/BigBH_{0}.csv".format(output_forfile)
print "Opening file",filename
with open(filename) as f:
    reader=csv.reader(f)
    next(reader)
    sink_lines=np.array([row for row in reader])
        
[halo.add_sink(sink_line,halo.Mstar) for halo,sink_line in zip(halos,sink_lines)]

mstars=np.array([halo.Mstar for halo in halos])
msinks=np.array([halo.sink.mass for halo in halos])

ax2=horizon.plot_vs_mass(ax2,msinks,mstars,colour='b',quartile=True)
ax2=horizon.plot_vs_mass(ax2,pair.get_data('AGN.mark.sink.mass'),pair.get_data('AGN.mark.Mstar'),colour='g',quartile=True)

for ax in (ax1,ax2):
    ax.set_yscale('log')
    ax.set_xscale('log')
ax2.set_xlabel('Mstar')
#ax2.set_ylim(1E6,1E9)
ax2.set_ylabel('Mbh')

plt.tight_layout()
figname='sample_{0}.pdf'.format(output)
print "Saving as",figname
fig.savefig(figname)

