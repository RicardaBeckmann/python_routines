import horizon
import csv

for output in [43,70,125,197,343,519,761]:
    pair=horizon.Pair(output)
    twins=pair.get_sink_twins(accretion=True)

    data=[]
    for twin in twins:
        data.append([twin.AGN.mark.sink.mass,twin.AGN.mark.sink.accretion,twin.AGN.mark.sink.eddington_ratio,twin.AGN.halo.mvir,twin.AGN.halo.host.mvir])

    filename='HORIZON_BlackHoles_{0}.csv'.format(pair.AGN.output_forfile)
    with open(filename,'wb') as f:
        writer=csv.writer(f)
        writer.writerow(['# aexp={0}    Mbox={1} Mpc/h  output={2}'.format(pair.AGN.ds.aexp,pair.AGN.ds.Mpc_h,output)])
        writer.writerow(['Mbh [Msun]', 'dotM [Msun/yr]', 'Eddington ratio', 'galaxy Mstar [Msun]', 'halo Mdm [Msun]'])
        for row in data:
            writer.writerow(row)
    print "Done with output",output
    print "################################"

