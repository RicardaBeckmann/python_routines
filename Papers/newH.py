from utils import *
from astropy.cosmology import WMAP7


plt.rcParams['figure.figsize']=[16,8]

msun=2e+33
mp=1.667e-24
G=6.67e-8
yr=365*24*60*60

#################
# FUNCTIONS
#################

def t_df(Mbh,Mgal,sigma,r0):
    '''Dynamical friction time, from point of numerical merger, assuming fixed BH mass,In units of Gyr'''
    t=0.67*(r0/4)*(sigma/100)*(1E8/Mbh)*1/np.log10(1+Mgal/Mbh)
    return t

def r_DF(t,Mbh,Mgal,sigma,r0):
    '''Dynamical friction orbit evolution, for a total time t, starting at r0
    at fixed BH mass (Mbh), galaxy mass (Mgal) and velocity dispersion (sigma)'''
    rad2 = ((r0/4)**2 - 1/0.67 / (sigma/100) * Mbh/1E8 * np.log10(1+Mgal/Mbh)*t)
    return 4*np.sqrt(rad2)

def r_integrate(times,Mbh,Mgal,sigma,r0):
    '''Integrate orbit for a set of times, while updating Mbh, Mgal and sigma at these points in time.
    Starting from r0
    '''
    r_int=[r0]
    try:
        Mbh[1]
    except:
        Mbh=np.ones(len(times))*Mbh
    try:
        Mgal[1]
    except:
        Mgal=np.ones(len(times))*Mgal
    try:
        sigma[1]
    except:
        sigma=np.ones(len(times))*sigma
    for i,dt in enumerate(times[1:]-times[:-1]):
        r_int.append(r_DF(dt,Mbh[i],Mgal[i],sigma[i],r_int[-1]))
    r_int=np.array(r_int)
    r_int[np.isnan(r_int)]=0
    return r_int
#################
# UTILITIES
################

from yt.utilities.lib.cosmology_time import \
    friedman

def age_to_t(age):
    H0          =  0.704000015258789E+02/100
    omega_m     =  0.272000014781952E+00
    omega_l     =  0.727999985218048E+00
    unit_t      =  0.179648987118545E+17

    tau_frw, t_frw, dtau, n_frw, time_tot = friedman( omega_m, omega_l, 1. - omega_m - omega_l )

    iage = 1 + int(10.*age/dtau)
    iage = np.min([iage,n_frw//2 + (iage - n_frw//2)//10])
    time_simu = t_frw[iage]*(age-tau_frw[iage-1])/(tau_frw[iage]-tau_frw[iage-1])+ t_frw[iage-1]*(age-tau_frw[iage  ])/(tau_frw[iage-1]-tau_frw[iage])
    current_time = (time_tot + time_simu)/(H0*1e7/3.08e24)/(365*24*3600*1E9) #in Gyr
    return current_time


#################
# DATA ACCESS
################

class Output(object):
    def __init__(self,folder='../'):
        folder+='/Data_local/'
        filename=folder+'output_step_redshift.txt'  #snap2red.txt'

        with open(filename) as f:
            reader=csv.reader(f)
            next(reader)
            data=[row for row in reader]
        self.outputs=np.array([row[0] for row in data]).astype('int')
        self.nstep_coarse=np.array([row[1] for row in data]).astype('int')
        self.aexp=np.array([row[2] for row in data]).astype('float')
        self.z=np.array([row[3] for row in data]).astype('float')
        self.redshifts=self.z
        self.t=np.array([row[4] for row in data]).astype('float')

    def get_output_from_z(self,z,verbose=True):
        index=np.argmin(np.abs(self.redshifts-z))

        if verbose:
            print("The closest redshift in outputs to ",z,"is",self.redshifts[index],"at output",self.outputs[index])
        return self.outputs[index]

    def get_from_output(self,attr,output,verbose=False):
        index=np.argmin(np.abs(self.outputs-output))
        if verbose:
            print("Output",output,"was written at ",attr," ",self.redshifts[index])
        return getattr(self,attr)[index]


    def get_z_from_output(self,output,verbose=True):
        print("Depreceated, please use self.get_from_output(output,attr)")
        return self.get_from_output('z',output)

    def get_nstep_for_output(self,output,verbose=True):
        print("Depreceated, please use self.get_from_output(output,attr)")
        return self.get_from_output('nstep_coarse',output)


#################
# OBJECTS
################
class BlackHoles(object):
    def __init__(self,folder='..',reff=2,addendum='HOP'):#,pairs=False):
        #Construct filename
#        folder+='/Catalogues/'
        folder+='/Data/'
        self.basefolder=folder

        subfolder=folder+'NewHorizon_100perc'
        if addendum:
            subfolder+='_{0}'.format(addendum)

        filename='/halo_gal_sink_0-10rvir_'

        filename+=("%.2f"%reff)[:4].replace('.','-')
        filename+='reff.txt'

        print(subfolder+filename)
        if not(os.path.isfile(subfolder+filename)):
            print(subfolder+filename,"does not exist")

        self.reff_cut=reff
        self.filename=filename
        self.folder=subfolder

        try:
            data=np.load(self.folder+filename+'.npy')
        except:
            print("Loading the text file for BH data")

        #Load data
            with open(self.folder+filename) as f:
                reader=csv.reader(f,delimiter=' ')
                data=[]
                for i,row in enumerate(reader):
                    row=np.array(row)
                    row=row[row!='']
                    data.append(row.astype('float'))
            data=np.array(data)

            np.save(self.folder+filename,data)

        self.z=data[:,0]
        self.time=np.array([WMAP7.age(z).value for z in self.z])
        #self.time=(WMAP7.hubble_time-WMAP7.lookback_time(self.z)).value
        self.gal_id=data[:,1].astype(int)
        self.bh_id=data[:,2].astype(int)
        self.Mbh=10**data[:,3]  #Msun
        self.Mgal=10**data[:,4] #Msun
        self.reff=data[:,5] #kpc
        self.d_gal=data[:,6]  #distance to galaxy center, kpc
        self.fedd=data[:,7]
        self.fedd[self.fedd<0]=0  #Take out spurious negative values
        self.spin_mag=data[:,8]
        self.abs_spin_mag=abs(self.spin_mag)
        self.eps_rad=data[:,9]
        self.radio=self.fedd<0.01
        self.halo_id=data[:,10].astype(int)
        self.Mhalo=10**data[:,11]  #Msun
        self.rvir_halo=data[:,12]  #kpc
        self.d_halo=data[:,13]   #kpc
        self.gal_level=data[:,14].astype(int)
        self.halo_level=data[:,15].astype(int)
        self.bh_vel=data[:,16]  #in the frame of the box, km/s
        self.rho_gas=data[:,17]   #Hcc
        self.c_s=data[:,18]    #km/s
        self.v_rel=data[:,19]  #km/s
        self.jgas=data[:,20:23]  # units of jgas_mag
        self.jgas_mag=data[:,23] # unknown units
        self.spin=data[:,24:27]
        self.most_massive=data[:,27]>0


        self.theta=np.arccos((self.spin[:,0]*self.jgas[:,0]+self.spin[:,1]*self.jgas[:,1]+self.spin[:,2]*self.jgas[:,2]))/np.pi*180



        #Sort out feedback efficiencies
        self.eps_MCAD=(4.10507+0.328712*self.spin_mag+76.0849*self.spin_mag**2+47.9235*self.spin_mag**3+3.86634*self.spin_mag**4)/100

        self.eps=self.eps_rad.copy()
        self.eps[self.radio]=self.eps[self.radio]*(self.fedd[self.radio]/0.01)

        self.eta=self.eps_rad.copy()*0.15
        self.eta[self.radio]=self.eps_MCAD[self.radio]

        #self.sigma_pp=(self.Mbh/1E8/1.9)**(1/5.1)*200
        self.sigma_pp=np.sqrt(6.67408E-8*self.Mgal*1.989E33/(2*self.reff*3.086E21))/1E5

        self.unique_ids=np.array(list(set(self.bh_id)))
        self.z0=self.get_massordered_bhs(0)

        try:
            self.construct_tree(new=addendum!=None)
        except:
            print("Failed to add mergers")

    def get_closest_redshift(self,z=0,verbose=False,for_plot=False):
        ''' Find clostest redshift present in the sample to the input redshift'''
        z_closest=self.z[np.argmin(np.abs(self.z-z))]
        if verbose:
            print("The closest redshift is",z_closest)
        if for_plot:
            return np.round(z_closest,2)
        else:
            return z_closest

    def get(self,attr,z=0,verbose=False,ordered=None):
        '''Get attribute data at a given redshift'''
        z_mask=self.z==self.get_closest_redshift(z=z,verbose=verbose)
        if verbose:
            print("There are",sum(z_mask),"BHs at this point")
        data=getattr(self,attr)[z_mask]
        if ordered:
            return data[getattr(self,ordered)[z_mask].argsort()[::-1]]
        else:
            return data

    def get_bh(self,bh_id,attr,z=0):
        ''' load timeseries of attr for bh_id'''
        mask=self.bh_id==bh_id
        return getattr(self,attr)[mask]

    def get_massordered_bhs(self,z=0):
        '''Returns a list of BH ids at a given redshift,
        in descending order of mass'''
        masses=self.get('Mbh',z=z)
        ids=self.get('bh_id',z=z)
        return ids[np.argsort(masses)[::-1]]


    def construct_tree(self,new=False):
        if new:
            treename='/newH_merger_catalogue.dat'
        else:
            treename='/BHmergers_all_NH_tdf_0.3_scale_l_xyz'

        print(self.folder+treename)
        try:
            data=np.load(self.folder+treename+'.npy')
        except:
            print("Reading text file for merger data")
            with open(self.folder+treename) as f:
                reader=csv.reader(f)
                if new:
                    next(reader)
                data=np.array([row[0].split() for row in reader]).astype('float')
            np.save(self.basefolder+treename,data)
        self.mergers=Mergers(data,self.z0)


class Distance(object):
    def __init__(self,primary,secondary,folder='..'):
        folder+='/Data/'
        self.basefolder=folder


        subfolder=folder+'/Pairs/'
        self.folder=subfolder
        filename='dist_{0}_{1}.dat'.format(primary,secondary)
        print(subfolder+filename)
        if not(os.path.isfile(subfolder+filename)):
            print(subfolder+filename,"does not exist")

        self.filename=filename


        #Load data
        with open(self.folder+filename) as f:
            reader=csv.reader(f,delimiter=' ')
            data=[]
            for i,row in enumerate(reader):
                row=np.array(row)
                row=row[row!='']
                data.append(row.astype('float'))
            data=np.array(data)

        self.z=data[:,0]
        self.time=(WMAP7.hubble_time-WMAP7.lookback_time(self.z)).value
        self.d=data[:,1]   #in kpc
        self.M_p=10**data[:,2]
        self.M_s=10**data[:,3]


class Mergers(object):
    def __init__(self,data,survivors):

        self.survivors=survivors

        self.minor_id=data[:,0].astype('int')
        self.major_id=data[:,1].astype('int')
        self.dist=data[:,2]
        self.minor_Mbh=10**data[:,3]
        self.major_Mbh=10**data[:,4]
        self.minor_fedd=data[:,5]
        self.major_fedd=data[:,6]
        self.t0_file=data[:,7]
        self.z=data[:,8]
        self.t0=(WMAP7.hubble_time-WMAP7.lookback_time(self.z)).value
        self.minor_pos=data[:,10:13]
        self.major_pos=data[:,13:16]
        self.q=self.minor_Mbh/self.major_Mbh

    def get_merger_ids(self,id,verbose=False,mass_ratio=False):
        '''Get ids of minor BHs involved in the mergers'''
        mask=self.major_id==id
        data=self.minor_id[mask]
        if verbose:
            print("BH with id",id,"has ",sum(mask),'mergers with BHs',data)
        if mass_ratio:
            return data,self.q[mask]
        else:
            return data

    def get_final(self,id,verbose=False):
        mask=self.minor_id==id
        merger_id=self.major_id[mask]
        if verbose:
            if len(merger_id)>0:
                print("BH",id,"merges into BH",merger_id[0])
            elif id in self.survivors:
                print("BH",id,"survives until z=0")
            else:
                print("BH",id,"vanishes from catalogue")
        if len(merger_id)>0:
            return int(merger_id)
        else:
            return

    def get_merger_time(self,id,fromfile=False):
        '''Id has to be of the minor'''
        mask=self.minor_id==id
        if fromfile:
            return self.t0_file[mask][0]
        else:
            return self.t0[mask][0]

    def get_merger_mass(self,id,major=False):
        '''Get the mass of the companion
        major=FALSE : id is for the smaller BH'''

        mask=self.minor_id==id
        if major:
            return self.major_Mbh[mask][0]

        #elif total:
        #    return float(self.z[mask]),float(self.major_Mbh[mask]+self.minor_Mbh[mask])
        else:
            return self.minor_Mbh[mask][0]

    def get_all_children(self,id):
        #Get direct child ids
        all_ids=list(self.get_merger_ids(id))

        #Recursive call to get all children of children
        len_old=0
        while len(all_ids)>len_old:
            len_old=len(all_ids)
            for id in all_ids:
                all_ids+=list(self.get_merger_ids(id))
            all_ids=list(set(all_ids))
        return np.array(all_ids)


    from astropy.cosmology import WMAP7

class BH_timeseries(object):
    def __init__(self,id):
        self.id=id
        self.foldername='../Data/timeseries/'
#        self.foldername='../Catalogues/timeseries/'
        self.filename='bh_{0}.txt'.format(str(id).zfill(5))
        with open(self.foldername+self.filename) as f:
            reader=csv.reader(f)
            fields=next(reader)
            #print(fields)
            data=np.array([row[0].split() for row in reader]).astype('float')
        self.nstep=data[:,0].astype('int')
        self.z=data[:,1]
        #self.time=(WMAP7.hubble_time-WMAP7.lookback_time(self.z)).value
        self.Mbh=data[:,2]
        self.dmbondi=data[:,3]
        self.dmedd=data[:,4]
        while any(self.dmedd<=0):
            for loc in np.where(self.dmedd<=0):
                self.dmedd[loc]=self.dmedd[loc-1]

        #self.fedd=self.dmbondi/self.dmedd
        #self.fedd[self.fedd>1]=1
        #self.fedd[self.fedd<0]=0

        self.spinmag=data[:,5]
        self.spinmag_abs=np.abs(self.spinmag)
        self.eps_rad=data[:,6] #Thin disc radiative efficiency

        self.bh_vel=data[:,7]
        self.rho_gas=data[:,8]   #Hcc
        self.c_s=data[:,9]    #km/s
        self.v_rel=data[:,10]  #km/s
        self.jgas=data[:,11:14]  # units of jgas_mag
        self.jgas_mag=data[:,14] # unknown units
        self.spin=data[:,15:18]
        self.Esave=data[:,18]

        self.macc=data[:,19]  #Msun
        #self.time_code=data[:,20]  #Gyr
        self.time=np.array([WMAP7.age(z).value for z in self.z])

        self.dt=(self.time[1:]-self.time[:-1])*1E9 #year
        self.dt=np.array(list(self.dt)+[self.dt[-1]])
        self.dmacc=self.macc/self.dt
        self.fedd=self.dmacc/self.dmedd
        self.fedd[self.fedd>1]=1

        self.radio=self.fedd<0.01

	#Sort out feedback efficiencies
        self.eps_MCAD=(4.10507+0.328712*self.spinmag+76.0849*self.spinmag**2+47.9235*self.spinmag**3+3.86634*self.spinmag**4)/100
        self.eps=self.eps_rad.copy()
        self.eps[self.radio]=self.eps[self.radio]*(self.fedd[self.radio]/0.01)

        self.eta=self.eps_rad.copy()*0.15
        self.eta[self.radio]=self.eps_MCAD[self.radio]

        self.lum=self.eps*self.dmacc*msun*(3E10)**2/yr

        self.theta=np.arccos((self.spin[:,0]*self.jgas[:,0]+self.spin[:,1]*self.jgas[:,1]+self.spin[:,2]*self.jgas[:,2]))/np.pi*180

    def average(self,attr,t_target,dt=50,delog=False):
        dt=dt/1E3 # in Gyr
        t_mask=np.logical_and(self.time>=(t_target-dt),self.time<=(t_target+dt))
        if len(t_mask)>len(getattr(self,attr)):
            t_mask=t_mask[:-1]
        if t_mask.sum()==0:
            return 0
        else:
            if delog:
                return 10**np.average(np.log10(getattr(self,attr)[t_mask]))
            else:
                return np.average(getattr(self,attr)[t_mask])

    def mean(self,attr,t_target,dt=50,delog=False):
        data=(self._statistical(np.mean,attr,t_target,dt,delog))
        if delog:
            return 10**data
        else:
            return data

    def variance(self,attr,t_target,dt=50,delog=False):
        data=(self._statistical(np.var,attr,t_target,dt,delog))
        if delog:
            return 10**np.sqrt(data)
        else:
            return np.sqrt(data)

    def	change(self,attr,t_target,dt,delog=False):
        data=(self._statistical(lambda x:x[-1]-x[0],attr,t_target,dt,delog))
        if delog:
            return 10**(data)
        else:
            return data

    def _statistical(self,func,attr,t_target,dt=50,delog=False):
        dt=dt/1E3 # in Gyr
        t_mask=np.logical_and(self.time>=(t_target-dt),self.time<=(t_target+dt))
        if len(t_mask)>len(getattr(self,attr)):
            t_mask=t_mask[:-1]
        if t_mask.sum()==0:
            return 0
        else:
            data=getattr(self,attr)
            if delog:
                exists=np.logical_and(~np.isnan(data),data>0)
                return func(np.log10(data[np.logical_and(t_mask,exists)]))
            else:
                exists=~np.isnan(data)
                return func(data[np.logical_and(t_mask,exists)])


    def peak(self,attr,t_target,dt=50):
        dt=dt/1E3 # in Gyr
        t_mask=np.logical_and(self.time>=(t_target-dt),self.time<=(t_target+dt))
        if t_mask.sum()==0:
            return 0
        else:
            return getattr(self,attr)[t_mask[:-1]].max()


class Kinematics(object):
    '''
    Based on Minjungs kinetmatics data
    #0  nout: snapshot output number
    #1  galid: galaxy id (only "pure" galaxies, free from low-res DM contamination, above 1e8 solar mass)
    #2  mstellar: the stellar mass of a galaxy (AdaptaHOP)
    #3  R50: effective radius, 3d radius containing 50% of the total stellar mass of the galaxy
    #4  R90: radius containing 90% of the total stellar mass of the galaxy
    #5  DT:  disk-to-total ratio: the mass ratio of the disk component (stellar particles having circularity parameter higher than 0.5)
    #6  msph_R50: the mass of the spheroidal component (stellar particles having circularity parameter lower than 0.5) inside R50
    #7  msph_2R50: the mass of the spheroidal component inside 2 R50
    #8  vrot_R50:  mass-weighted mean rotational (tangential) velocity of the stellar particles inside R50
    #9  sigma_R50: mass-weighted 1d velocity dispersion (3d dispersion divided by sqrt(3)) of the stellar particles inside R50
    #10 vrot_2R50: mass-weighted mean rotational (tangential) velocity of the stellar particles inside 2 R50
    #11 sigma_2R50: mass-weighted 1d velocity dispersion of the stellar particles inside 2 R50
    #12 sigma_sph_R50: mass-weighted 1d velocity dispersion of the "spheroidal" component inside R50
    #13 sigma_sph_2R50: mass-weighted 1d velocity dispersion of the "spheroidal" component inside 2 R50
    '''

    def __init__(self,output=790,folder='..',catalogue='HOP'):

        available=[110, 158, 251, 446, 599, 790, 906]
        if not(output in available):
            print("No data available for output",output)
            print("Please pick one of ",available)
            return

        filename=folder+'/Data_local/Kinematics_minjung/{0}/'.format(catalogue)+'data_{0}.npy'.format(output)
        if catalogue=='HOP':
            filename=filename.replace('data_','data_HOP_')

        self.output=output

        log=Output()
        self.z=log.get_z_from_output(self.output)

        self.filename=filename
        data=np.load(filename)
        self.gal_id=data['galid']
        print(data.dtype.names)
        print(data.dtype.names[3:])
        self.Mgal=data['mstellar']
        self.Mgal_cut=self.Mgal.min()
        for name in data.dtype.names[:]:
            '''R50, R90, DT, msph_R50, msph_2R50, vrot_R50,
               sigma_R50, vrot_2R50, sigma_2R50, sigma_sph_R50, sigma_sph_2R50'''
            setattr(self,name,data[name])
    def get_gal(self,id,attr='msph_R50'):
        mask=self.gal_id==id
        return getattr(self,attr)[mask][0]

    def get_dict(self,attr):
        data_dict={}
        for id,val in zip(self.gal_id,getattr(self,attr)):
            data_dict[id]=val
        return data_dict
    def get_sample(self,BH,attr):
        bh_mask=BH.get('Mgal',z=self.z)>=self.Mgal_cut

        try:
            return BH.get(attr,z=self.z)[bh_mask]
        except:
            attr_dict=self.get_dict(attr)
            gal_ids=BH.get('gal_id',z=self.z)[bh_mask]
            return np.array([attr_dict[id] for id in gal_ids])


class Greene(object):
    '''Read post-processed Latex tables from Greene2020 review on black holes in dwarf galaxies'''
    def __init__(self,number):
        filename='../Data_local/Greene2020_Table{0}.txt'.format(number)

        with open(filename) as f:
            reader=csv.reader(f)
            data=np.array([row for row in reader if not row[0][0]=='#'])

        print(data[0,:])
        data=data[1:,:]  #Take off the header row
        data[data=='']=0

        if number in [2,4]:
            self.name=data[:,0]
            self.distance=data[:,1].astype('float')
            self.method=data[:,2]
            self.Mbh=data[:,3].astype('float')
            self.Mbh_lower=self.Mbh-data[:,4].astype('float')
            self.Mbh_upper=self.Mbh+data[:,5].astype('float')
            self.Mcluster=data[:,6].astype('float')
            self.Mstar=data[:,7].astype('float')
            self.sigma=data[:,8].astype('float')
            self.sigma_limit=data[:,9]=='True'
            self.literature=data[:,10]
        elif number == 5:
            self.literature=data[:,0]
            self.selection=data[:,1]
            self.Ntot=data[:,2].astype('int')
            self.f_AGN=data[:,3].astype('float')
            self.L_bol=data[:,4].astype('float')
        elif number == 10:
            data=data.astype('float')
            self.Mbh=10**data[:,0]
            self.lin=data[:,1]
            self.lin_low=data[:,2]
            self.lin_high=data[:,3]
            self.nc=data[:,4]
            self.nc_low=data[:,5]
            self.nc_high=data[:,6]
        elif number in [6,7,8]:
            self.gal_id=data[:,0]
            self.dist=data[:,1].astype('float')
            self.sigma=data[:,2].astype('float')
            self.sigma_err=data[:,3].astype('float')
            self.Kband=data[:,4].astype('float')
            self.MVcolor=data[:,5].astype('float')
            self.Mstar=data[:,6].astype('float')
            self.Mbh=data[:,7].astype('float')
            self.Mbh_err_low=data[:,8].astype('float')
            self.Mbh_err_high=data[:,9].astype('float')
            self.hubble=data[:,10]
            if number==6:
                self.literate=data[:,11]
