import horizon
import csv
import numpy as np

# Works with branch create_TWINS_files

all_outputs=[43,70,90,125,197,343,519,713,761]

def twins(DM=False,r10=False):
    if r10:
        outputs=[125]
    else:
        outputs=all_outputs
    for output in outputs:
        noAGN=horizon.pairup_output(output,DM=False)
        pair=horizon.Pair(output,DM=DM,add_halos=not(DM),r10=r10)
        if not DM:
            pair.add_host_ids(AGN=True,r10=r10)
            pair.add_host_ids(AGN=False,r10=r10)

            filename='./STAR_TWINS'
            if r10:
                filename+='_R10'
            else:
                filename+='_R05'
            filename+='/twins_{0}_{1}.csv'.format(str(output).zfill(3),str(noAGN).zfill(3))
        else:
            filename='./DM_TWINS/twins_{0}_{1}.csv'.format(str(output).zfill(3),str(noAGN).zfill(3))
        print "Saving as",filename

        with open(filename,'wb') as f:
            writer=csv.writer(f)
            if DM:
                writer.writerow(['AGN DM','noAGN DM'])
                for twin in pair.twins:
                    writer.writerow([twin.AGN.id,twin.noAGN.id])
            else:
                writer.writerow(['AGN STARS','noAGN STARS','AGN DM','noAGN DM'])
                for twin in pair.twins:
                    writer.writerow([twin.AGN.id,twin.noAGN.id,twin.AGN.halo.host_id,twin.noAGN.halo.host_id])


        horizon._separator()

def stellar_halos(r10=False):
    if r10:
        outputs=[125]
    else:
        outputs=all_outputs
    for output in outputs:
        pair=horizon.Pair(output,DM=False,r10=r10)

        for DM in [True,False]:
            if DM:
                npart=100
            else:
                npart=10

            for type in ['AGN','noAGN']:
                if DM:
                    pair.add_host_ids(AGN=True,r10=r10)
                    pair.add_host_ids(AGN=False,r10=r10)
                    twin_ids=np.array([getattr(twin,type).halo.host_id for twin in pair.twins])
                else:
                    twin_ids=np.array([getattr(twin,type).id for twin in pair.twins])

                if DM:
                    folder="./{0}_DM".format(type)
                else:
                    folder="./{0}_STARS".format(type)

                filename=folder+'/yt_halos_{0}_{1}.txt'.format(getattr(pair,type).output_forfile,npart)
                print "Opening",filename
                with open(filename) as f:
                    reader=csv.reader(f)
                    lines=np.array([row[0].split() for row in reader])

                halo_ids=np.array([int(float(line[0])) for line in lines])
            
                if r10:
                    folder='./STAR_TWINS_R10/'
                else:
                    folder='./STAR_TWINS_R05/'
                if DM:
                    filename='yt_DM_halos'
                else:
                    filename='yt_STAR_halos'
                filename=folder+filename+'_{0}_{1}.csv'.format(type,getattr(pair,type).output_forfile)
                print "Saving in",filename

                with open(filename,'wb') as f:
                    writer=csv.writer(f)
                    writer.writerow(['ID','x','y','z','rvir','mvir [1E11 MSun]','mhalo [1E11 MSun]','npart'])
                    for twin_id in twin_ids:
                        writer.writerow(lines[halo_ids==twin_id][0])
        horizon._separator()

def DM_halos():
    outputs=all_outputs
    for output in outputs:
        pair=horizon.Pair(output,DM=True,add_halos=False)
        npart=100

        for simulation in ['AGN','noAGN']:
            twin_ids=np.array([getattr(twin,simulation).id for twin in pair.twins])

            folder="./{0}_DM/".format(simulation)
            filename=folder+'/yt_halos_{0}_{1}.txt'.format(getattr(pair,simulation).output_forfile,npart)

            print "Opening",filename
            with open(filename) as f:
                reader=csv.reader(f)
                lines=np.array([row[0].split() for row in reader])
            halo_ids=np.array([int(float(line[0])) for line in lines])
            
            folder='./DM_TWINS/'
            filename=folder+'yt_halos_{0}_{1}.csv'.format(simulation,getattr(pair,simulation).output_forfile)
            print "Saving in",filename

            with open(filename,'wb') as f:
                writer=csv.writer(f)
                writer.writerow(['ID','x','y','z','rvir','mvir [1E11 MSun]','npart','mhalo [1E11 MSun]'])
                for twin_id in twin_ids:
                    writer.writerow(lines[halo_ids==twin_id][0])
        horizon._separator()
                    
#################
#for r10 in [False]:#,True]:
twins(DM=False,r10=True)
#    horizon._separator_2()
#stellar_halos(r10=True)

#horizon._separator_2()
#twins(DM=True)
horizon._separator_2()
#DM_halos()
