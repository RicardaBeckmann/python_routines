from utils import *
import ytutils
import sinks
import re
import lyrics

from astropy.constants import G
from astropy.units import kpc,Msun,u
from astropy.table import Table


#######################################
# CUSTOM FIELDS
#######################################


def divB(field,data):
    values = data['magnetic_field_x_right'] - data['magnetic_field_x_left'] + data['magnetic_field_y_right'] - data['magnetic_field_y_left'] + data['magnetic_field_z_right'] - data['magnetic_field_z_left']
    return abs(values)


def _w_cool(field,data):
    '''wc, Kempski2020, eq (9) with number density instead of density for unit consistency'''
    return data['number_density']**2*data['cooling_total']/data['pressure']

def _CR_diff(field,data):
    '''From Kepski2020, section 4.6.1'''
    Dc=data.ds.quan(1E29,'cm**2/s')
    return Dc*data['w_cool']/data['eta']/data['alfven_speed']**2



#######################################
# UTILITIES
#######################################
def add_fields(ds):

    lyrics.add_fields(ds)
    ds.add_field(name=('gas','w_cool'),sampling_type="cell",function=_w_cool,
                   units='1/s')

    ds.add_field(name=('gas','CR_diff'),sampling_type='cell',function=_CR_diff,units='dimensionless',display_name=r'$\chi$')
    return ds

def check_divB(ds):
    ds.add_field(("divB"), function=divB, units="gauss",sampling_type='cell',force_override=True)
    ad=ds.all_data()
    return ad['divB'].max()


#######################################
# OBEJCTS
#######################################

class Profile(object):
    '''Profile quantities, produced using 2021_03_23_pllpp_profiles.py
        Each profile is for a single output, a single object shape, and a single phase (hot, warm, cold or all)
        Twarm=2E5
        Tcold=2E4
    '''

    def __init__(self,output,df,radius=150,phase='',shape='sphere',verbose=False):
        self.df=df
        self.output=output
        self.time=ytutils.load(output,df,silent=True).current_time.in_units('Myr')
        self.radius=radius
        self.shape=shape

        if phase=='':
            self.all=True
        else:
            self.phase=phase

        filename=df+'/Data/'+f'profile_{str(output).zfill(5)}_{shape}'

        if shape=='sphere':
            filename+=f'_{radius}kpc'
        if phase in ['warm','hot','cold']:
            filename+='_'+phase
        if verbose:
            print(filename)

        self.mean=Table.read(filename+'_mean.ecsv', format='ascii.ecsv')
        self.sum=Table.read(filename+'_sum.ecsv', format='ascii.ecsv')

        #empty=self.mean['density']==0

        #self.mean=self.mean[~empty]
        #self.sum=self.sum[~empty]

        if len(set(self.mean.keys())&set(self.sum.keys()))>1:
            print("There are fields in both profiles! Please code up a fix for this",set(prof.mean.keys())&set(prof.sum.keys()))

        for field in self.mean.keys():
            setattr(self,field,self.mean[field])
        for field in self.sum.keys():
            setattr(self,field,self.sum[field])

        self.total_baryon_mass=self.star_mass+self.cell_mass
        self.contained_stellar_mass=np.cumsum(self.star_mass)
        self.contained_gas_mass=np.cumsum(self.cell_mass)

        self.fields=list(self.mean.keys())+list(self.sum.keys())
        self.radius=self.radius.to('kpc')
        
    def add_sink(self):
        try:
            self.BH_mass=sinks.read(self.output,self.df,silent=True,code_mass=True)[0].mass.to_astropy()
        except:
            self.BH_mass=0*Msun

    def get_total_mass(self):
        self.add_sink()
        ds=ytutils.load(1,self.df,silent=True)
        fgas=ds.parameters['namelist']['poisson_params']['gravity_params'][3]
        if self.output>1:
            p0=Profile(1,self.df,phase=self.phase,radius=self.radius,shape=self.shape)
        else:
            p0=self
        self.dm_mass=p0.cell_mass/fgas*(1-fgas)
        self.contained_dm_mass=p0.contained_gas_mass/fgas*(1-fgas)
        self.total_mass=self.dm_mass+self.total_baryon_mass+self.BH_mass
        self.contained_total_mass=self.contained_dm_mass+self.contained_stellar_mass+self.contained_gas_mass+self.BH_mass

        self.t_ff=np.sqrt(2*(self.radius)**3/(G*self.contained_total_mass)).to('yr')
        self.t_ratio=self.t_cool/self.t_ff

class Timeseries(object):
    def __init__(self,df,shape='sphere',radius=150,output_min=1):
        self.df=df

        
        
        #self.outputs=list(set(ytutils.find_outputs(df))&set([int(re.sub("\D", "", f)) for f in os.listdir(df+'/Data/') if f[:6]=='fluxes']))
        #self.outputs.sort()

        
        self.outputs=ytutils.find_outputs(df)
        self.outputs=self.outputs[self.outputs>=output_min]
        self.radius=radius
        for phase in ['hot','warm','cold','']:
            data=[]
            for o in self.outputs:
                try:
                    data.append(Profile(o,df,phase=phase,shape=shape,radius=radius))
                except:
                    print("Skipping",df,o,phase)
            data=np.array(data)
            if phase=='':
                name='all'
            else:
                name=phase
            setattr(self,name,data)

        #Remove empty data
        self.time=np.array([p.time.v for p in self.hot])

    def get(self,phase,field,radius=0,contained=False):
        if radius==0:
            radius=self.radius
        print(phase,field,radius)
        profiles=getattr(self,phase)
        print(profiles[-1].radius,getattr(profiles[-1],field).data)
        data=[]
        for p in profiles:
            if len(p.radius)>0:
                if contained:
                    data.append(getattr(p,field).data[p.radius.to('kpc')<=radius*kpc].sum())
                else:
                    data.append(getattr(p,field).data[np.argmin(np.abs(p.radius.to('kpc')-radius*kpc))])
            else:
                data.append(0)
        return np.array(data)


class R80(object):
    '''Written using 2021_05_11_maker80.py '''
    def __init__(self,df):
        filename=df+'/Data/r80.csv'
        data=np.loadtxt(filename,skiprows=2,delimiter=',')
        self.outputs=data[:,0]
        self.time=data[:,1]
        self.r80=data[:,2]
        self.Mwarm=data[:,3]
        return
