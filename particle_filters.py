##########################################################################
# This file collections a number of particle filters to differentiate
# different types of particles in version of RAMSES too old to have 
# particle types.
#
#
#
##########################################################################
import yt
import numpy as np

def add_sink_filters(ds):
    #from yt.data_objects.particle_filters import add_particle_filter

    yt.add_particle_filter("sinks",function=get_sinks,filtered_type='io')
    yt.add_particle_filter("cloud",function=cloud, filtered_type='io')
    ds.add_particle_filter('sinks')
    ds.add_particle_filter('cloud')
    return ds


def add_filters(ds):
    #from yt.data_objects.particle_filters import add_particle_filter
    # Filters
    yt.add_particle_filter("star",function=stars,filtered_type='io')#,requires=[('io', 'particle_extra_field_1')])
    yt.add_particle_filter("DM",function=DM,filtered_type='io')#,requires=[('io','particle_extra_field_1')])
#    yt.add_particle_filter("cloud",function=cloud, filtered_type='io')
#    yt.add_particle_filter("sinks",function=get_sinks,filtered_type='io')
    ds.add_particle_filter('star')
    ds.add_particle_filter('DM')
#    ds.add_particle_filter('cloud')
#    ds.add_particle_filter('sinks')

    return ds
        
## These functions apply particle filters for DM and star particles to a data set. To use, add the lines

def stars(pfilter,data):
    filter=np.logical_and(data[('io','particle_extra_field_1')]<0,data[('io','particle_mass')]>0)
#    filter=np.logical_and(filter,data['io','particle_identity']>0)
    return filter

def DM(pfilter,data):
    filter=np.logical_and(data[("io","particle_extra_field_1")]==0,data[('io','particle_mass')]>0)
#    filter=np.logical_and(filter,data['io','particle_identity']>0)
    return filter

def cloud(pfilter,data):
    filter=np.logical_and(data[('io','particle_mass')]>0,data['io','particle_identity']==-1)
    return filter

def get_sinks(pfilter,data):
    filter=np.logical_and(data[('io','particle_mass')]>0,data['io','particle_identity']<0)
    filter=np.logical_and(filter,data['io','particle_extra_field_1']>0)
    return filter
