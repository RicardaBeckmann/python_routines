import numpy as np
import scipy.io as ff
from astropy.table import Table




def read_sinkfile(df,nstep):
    filename=df+f'/sink_{str(nstep).zfill(5)}.dat'
    print("Opening",filename)

    fields={'idsink':'',
        'mbh':'Msun',
       'xsink':'',
       'ysink':'',
       'zsink':'',
        'vxsink':'',
        'vysink':'',
        'vzsink':'',
       'xjsink':'',
        'yjsink':'',
        'zjsink':'',
        'Mbondi':'Msun/yr',
        'Medd':'Msun/yr',
        'macc':'Msun',
        'rho':'',
        'cs':'',
        'vr':'',
        'Esave':''
       }

    #Open file and get number of BH
    p=ff.FortranFile(filename)
    nsink=int(p.read_ints())
    ndim=int(p.read_ints())
    aexp=float(p.read_reals('d'))
    scale_l=float(p.read_reals('d'))
    scale_d=float(p.read_reals('d'))
    scale_t=float(p.read_reals('d'))
    
    
    #Make astropy table
    BHs = Table([np.zeros(nsink).astype('int')]+[np.zeros(nsink) for f in list(fields.keys())[1:]],
                names=fields.keys(), 
                dtype=('i4',*('f8' for f in list(fields.keys())[1:])),units=[fields[f] for f in fields.keys()])
    
    #Load remaining data
    BHids=p.read_ints()
    BHs['idsink']=BHids
    for field in list(fields.keys())[1:]:
        d=p.read_reals('d')
        BHs[field]=d
        BHs[field].unit=fields[field]

    return BHs
