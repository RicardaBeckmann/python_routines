#########################################################################
#  The routines in this file use the ramses version from October 2013
#
#
#
#
#
##########################################################################
import yt
import ytutils
import sinks
import halos
import matplotlib as mpl
mpl.pyplot.switch_backend('agg')
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import numpy as np
import math
import utils
from mpl_toolkits.axes_grid1 import AxesGrid

from yt.visualization.base_plot_types import get_multi_plot


font_size=32
#mpl.rcParams.update({'font.size': font_size})

###########################
#Plotting functions

# A function that makes a projection or slice of a particular dataset. All fields as
# defined in yt are available, as well as all other ProjectionPlot arguments
# output: integer, number of output
# slice: boolean, determines whether a slice is produced
# field='density': any yt field
# los='x' => or any other desired line of sight
# center=[0.5,0.5,0.5] => given in code length
# whole_box=False : plots whole box.
# zoom=50 : in units of unit
# unit='kpc': options are pc, kpc and Mpc. Also affect file name
# sink=True : boolean whether to center on sink and plot it in. Overrides "center"
# star_center: to plot sink halo before it contains sink. Overriden by sink position
# addendum: for plot name
# grids=False: overplot grids
# particles=False: overplot particle positions
# kwargs={} : passed to yt.projectionPlot()
# saveit=False : If false, no plot is saved to file
# filters=False : apply star and DM filters to plot the corresponding fields
# log=True: If False, plot colorbar on linear scale
# redshift=False: if true, annotate current redshift in lower left corner
#sink_frame: Plot streamlines in frame of reference of the sink


def plot(output,
         folder='.',
         ds=None,
         zoom=100,
         slice=False,
         field="density",
         los='x',
         center=[0.5,0.5,0.5],
         unit='pc',
         field_unit=None,
         sink=True,addendum="",
         grids=False,
         particle_type=None,
         kwargs={},
         saveit=False,
         filters=False,
         log=True,
         redshift=False,
         DM=False,
         whole_box=False,
         cloud=False,
         sink_center=True,
         npixels=None,
         velocities=False,
         time=False,
         cut_region=False,
         colorbar=True,
         scale=False,
         annotation=None,
         limits=[None,None],
         title=None,
         contour=False,
         streamlines=False,
         magnetic_fields=False,
         pdf=False,
         sonic=False,
         sink_frame=False,
         sink_size=3,
         sink_marker='x',
         fontsize=font_size,
         plot_r_refine=True,
         ff_time=False,
         streamlines_factor=50,
         weight_field='density',
         halo=False,
         subhalos=False,
         colour_map=None,
         time_unit='Myr',
         showit=True,
         small_figure=False,
         verbose=True,
         region=None,
         picture_only=False,
         output_folder=None,
         normal=[],
         north_vector=None,
         annotate_jet=False,
         sinkfile=None,jetargs={},
         background_colour=None,
         max_level=None):

    #Basic stylistic setup for ease of reading
    linewidth=max(3,sink_size-2)
    sink_size=max(1,sink_size)

    if small_figure:
        fontsize=14
        if verbose:
            print("Overruling the fontsize to fit the small plot")


    #Set up name
    if saveit and verbose:
        utils._separator()
    name='projection'
    if slice:
        name='slice'
    if  region:
        ds=region.ds
        try:
            mysinks=sinks.read(output,folder,ds=ds,silent=not(verbose))
        except:
            if verbose:
                print("No sinks found")
            sink=False
    elif sink:
        try:
            mysinks=sinks.read(output,folder,silent=not(verbose))
            ds=mysinks[0].ds
        except:
            if verbose:
                print("No sinks found")
            sink=False
            ds=ytutils.load(output,folder=folder,silent=not(verbose))
    elif not ds:
        ds=ytutils.load(output,folder=folder,silent=not(verbose))

    #Set colormap

    if not(colour_map):
        if not(type(field)==tuple):
            if field=='temperature':
                colour_map='inferno'
            elif 'xray' in field.split('_') and not(colour_map):
                colour_map='inferno'
            elif 'jetscalar' in field.split('_'):
                if limits[0]==None:
                    limits=[1E-5,1E-2]
            elif field.split('_')[0]=='star':
                colour_map='dusk'
        else:
            if 'jetscalar' in field[1].split('_'):
                if not limits[0]:
                    limits=[1E-6,1]

    #Handling DM
    if not(('gas','density') in ds.derived_field_list):
        if verbose:
            print("This is a DM run")
        DM=True
        if weight_field=='density':
            if verbose:
                print("Adjust the weight field to ('deposit','all_density')")
            weight_field=('deposit','all_density')

    #Handling particle fields
    fieldname=field
    if type(field)==tuple:
        fieldname=field[1]
        if type(color_map)==type(None):
            colour_map='dusk'

        if field[-1]=='all_density':
            colorbar_label=r'DM density [Hcm$^{-3}$]'
        elif field[-1]=='stars_density':
            colorbar_label=r'stellar density [Hcm$^{-3}$]'
        else:
            colorbar_label=None
    else:
        colorbar_label=None

    #Set up zoom size and adjust plotname accorindly
    if whole_box:
        if not(unit):
            unit=ds.get_smallest_appropriate_unit(ds.length_unit)
        if unit=='cm':
            unit='code_length'
        zoom=ds.length_unit.in_units(unit)
        sink_center=False
    else:
        zoom=ds.quan(zoom,unit)

    if verbose:
        print("Box size:",zoom)
    zoom_label="{:.5f}".format(float(zoom))
    plotname=name+'_{0}_{3}_{1}_{4}{2}'.format(fieldname,los,zoom_label.zfill(10),str(output).zfill(5),unit)

    #If sink is drawn in, load sink data and center plot
    if sink:
        try:
            if len(mysinks)>0:
                sink_pos=mysinks[0].pos
                plotname="{0}_sink".format(plotname)
            if sink_center:
                center=sink_pos
            else:
                if verbose:
                    print("Centering on the box")
                sink=False
        except:
            if verbose:
                print("Error in loading the sink. Sinks not activated?")
            mysinks=[]
            sink_center=False
    else:
        mysinks=[]
    if verbose:
        print("Plot center:",center)

    #Set up the region to be plotted
    if not region:
        if slice:
            region=ds.all_data()
        else:
            center=ds.arr(center,'code_length')
            left=center-zoom/2
            right=center+zoom/2
            #if zoom>ds.quan(1,'Mpc') and not(DM):
            #    depth=ds.quan(0.1,'Mpc')
            #    if verbose:
            #        print("Projection too deep, setting depth to",depth)
            #    left[2]=left[0]+zoom-depth
            #    right[2]=right[0]-zoom+depth
            region=ds.box(left,right)

    #Calculate relative velocities of the gas within the region when required
    if sink_frame:
        region=mysinks[0].set_relative_velocity(region)
    if type(field)==tuple:
        if field[1][:4]=='vrel':
            region=mysinks[0].set_relative_velocity(region)

    #Define the boolean mask for cut regions
    if cut_region:
        print("When setting up a cut region, set the field_unit parameter")
        if limits[0]:
            limit=ds.quan(limits[0],field_unit)
            print("applying lower limit",limit)
            region=region.cut_region("obj['{0}'].in_units('{2}') > {1}".format(field,float(limit.in_units(field_unit)),field_unit))
        if limits[1]:
            limit=ds.quan(limits[1],field_unit)
            print("applying upper limit",limit)
            region=region.cut_region("obj['{0}'].in_units('{2}') < {1}".format(field,float(limit.in_units(field_unit)),field_unit))

    #Actually plot the plot
    if len(normal)>1:
        if slice:
            myplot=yt.OffAxisSlicePlot(region.ds,normal,field,center=center,width=zoom,data_source=region,fontsize=fontsize,north_vector=north_vector,**kwargs)
        else:
            myplot=yt.OffAxisProjectionPlot(region.ds,normal,field,center=center,width=zoom,fontsize=fontsize,weight_field=weight_field,depth=zoom/2,data_source=region,north_vector=north_vector,**kwargs)
    else:
        if slice:
            myplot=yt.SlicePlot(region.ds,los,field,center=center,width=zoom,data_source=region,fontsize=fontsize,**kwargs)
        else:
            myplot=yt.ProjectionPlot(region.ds,los,field,center=center,width=zoom,data_source=region,weight_field=weight_field,fontsize=fontsize,max_level=max_level,**kwargs)

    #Style plot
    if background_colour:
        myplot.set_background_color(field,background_colour)

    #Fix plot style and limits
    Sfont=fontsize+10        #For boxes and annotations
    myplot.set_font_size(fontsize)
    myplot.set_axes_unit(unit)      #Set colorbar unit
    if npixels:
        myplot.set_buff_size(npixels)
    if colour_map:
        myplot.set_cmap(field,cmap=colour_map)
    if limits[0] or limits[1]:
        if verbose:
            print("Setting limits",limits)
        myplot.set_zlim(field,limits[0],limits[1])
        plotname+='_limit'

    #Annotate specified quantities
    if grids:
        myplot.annotate_grids()
        plotname+='_grids'
    if particle_type:
        myplot.annotate_particles(zoom,ptype=particle_type,data_source=region,p_size=15)

    if cloud:
        myplot.annotate_particles(zoom,ptype='cloud',col='k',p_size=15)
        plotname+='_cloud'
    if velocities:
        myplot.annotate_velocity(normalize=True)
    if contour:
        plotname+='_contour'
        #Clim needs to be given in CODE UNITS -> use convert_to_base to get
        myplot.annotate_contour(field=field,ncont=1,clim=ds.arr([contour*0.9,contour*1.1],getattr(ytutils.Units(),fieldname.split('_')[-1])).convert_to_base(),plot_args={'colors':['k','k'],'linewidth':2},factor=2)
    if field=='mach_number' or sonic:
        myplot.annotate_contour(field='mach_number',ncont=1,clim=(1.2,1.2),plot_args={'colors':'k','linewidth':linewidth},factor=2)
    if streamlines:
        if sink_frame:
            x_vel='vrel_x'
            y_vel='vrel_y'
            z_vel='vrel_z'
            colour='w'
            plotname+='_SINK'
        else:
            x_vel='velocity_x'
            y_vel='velocity_y'
            z_vel='velocity_z'
        colour='w'
        plotname+='_'

        if los=='x':
            stream_x=y_vel
            stream_y=z_vel
        elif los=='y':
            stream_x=z_vel
            stream_y=x_vel
        elif los=='z':
            stream_x=x_vel
            stream_y=y_vel

        myplot.annotate_streamlines(stream_x,stream_y,factor=streamlines_factor,plot_args={'color':colour,'linewidth':linewidth})  #Use 2 for more detail
        plotname+='streamlines'

    if magnetic_fields:
        colour='w'
        x_line='magnetic_field_x'
        y_line='magnetic_field_y'
        z_line='magnetic_field_z'
        colour='w'

        if los=='x':
            stream_x=y_line
            stream_y=z_line
        elif los=='y':
            stream_x=z_line
            stream_y=x_line
        elif los=='z':
            stream_x=x_line
            stream_y=y_line

        myplot.annotate_streamlines(stream_x,stream_y,factor=streamlines_factor,plot_args={'color':colour,'linewidth':linewidth})  #Use 2 for more detail                                                                                      
        plotname+='_magnetic'


    if halo:
        ids=False
        readhalos=halos.readhalos(output,npart=halos.npart_global,stars=False,folder=folder)   #Load all halos
        halo_dist=np.array([np.linalg.norm(halo.pos.in_units('kpc')-ds.arr(center,'code_length').in_units('kpc')) for halo in readhalos])
        name=None
        #Plot all halos onto the plot
        for each_halo in readhalos:
            myplot.annotate_sphere(each_halo.pos,each_halo.rvir,{'fill':False,'color':'grey','linewidth':sink_size},name,text_args={'size':'small', 'color':'k','clip_on':True})
        plotname+='_halo'

    #plot the subhalos of the halo given as halo=
    if subhalos:
        if not halo:
            print("NEED TO INPUT HALO TO PLOT SUBHALOS")
        else:
            ids=False
            readhalos=halos.readhalos(output,npart=10,stars=False,folder=folder,subhalos=True)
            halo_dist=np.array([np.linalg.norm(halo.pos.in_units('kpc')-ds.arr(center,'code_length').in_units('kpc')) for halo in readhalos])
            print("Number of subhalos",len(readhalos))
            for halo in readhalos[halo_dist<np.sqrt(3)*zoom]:
                name=None
                myplot.annotate_sphere(halo.pos,halo.rvir,{'fill':False,'color':'grey','linewidth':sink_size/2},name,text_args={'size':'small', 'color':'k','clip_on':True})

    #If possible&required, annotate the sink and its radii/arrows
    for sink in mysinks:
        myplot.annotate_marker(sink.pos,marker=sink_marker,plot_args={'s':sink_size*100,'color':'k','linewidth':sink_size},coord_system='data')
        if grids:
            myplot.annotate_marker(sink.pos,marker=sink_marker,plot_args={'s':sink_size*100,'color':'w','linewidth':sink_size},coord_system='data')
        elif hasattr(sink,'r_refine') and plot_r_refine==True:
            if sink.r_refine/zoom>0.0001:                
                print("Refinement radius",sink.r_refine.in_units('pc'),sink.r_refine)
                myplot.annotate_sphere(sink_pos,sink.r_refine.in_units('code_length'),circle_args={'fill':False,'color':'w','linewidth':linewidth})

    #annotate boxes with information
    if redshift:
        text="z={0}".format("{:.2f}".format(abs(ds.current_redshift)))
    if time:
        time=ds.current_time
        text="t={0} {1}".format("{:.2f}".format(float(time.in_units(time_unit))),time_unit)
        if verbose:
            print(text)
        if ff_time:
            t_unit=ytutils.cooling_halo_fftime(ds)
            text=r"$t={:.2f}$  [$t_{{ff}}/10^3$]".format(float(time.in_units('Myr')/t_unit))
    if redshift or time:
        #if annotation:
        #    myplot.annotate_text((0.1,0.9),text,coord_system='axis',text_args={'size':Sfont,'color':'w'})
        #else:
        myplot.annotate_text((0.1,0.9),text,coord_system='axis',text_args={'size':Sfont,'color':'w'},inset_box_args={'boxstyle':'round,pad=0.3','facecolor':'grey','linewidth':3,'edgecolor':'white', 'alpha':1.0})
    if annotation:
        myplot.annotate_text((0.08,0.08),annotation,coord_system='axis',text_args={'size':Sfont,'color':'w'},inset_box_args={'boxstyle':'round,pad=0.3','facecolor':'grey','linewidth':0,'edgecolor':'w', 'alpha':1.0})
    if scale:
        myplot.annotate_scale()
        myplot.hide_axes([field])
    if title:
        myplot.annotate_title(title)
    if annotate_jet:
        if type(sinkfile)==None:
            print("Please preload and pass a sinkfile using sinkfile= to annotate the jet")
        myplot=draw_jet(myplot,sinkfile=sinkfile,**jetargs)
        if 'length' in jetargs.keys():
            jetlength=jetargs['length']
        else:
            jetlength=10
        plotname+='_jet{0}'.format(jetlength)

    #Fix plot style and limits
    Sfont=fontsize+10        #For boxes and annotations
    myplot.set_font_size(fontsize)

    myplot.set_log(field,log)
    myplot.set_axes_unit(unit)      #Set x and y axis unit

    try:
        if field_unit==None:              #Set colorbar unit
            units=ytutils.Units()
            if hasattr(units,fieldname.split('_')[-1]):
                myplot.set_unit(field,getattr(units,(fieldname.split('_')[0])))
        else:
            myplot.set_unit(field,field_unit)
    except:
        pass
    if not colorbar or picture_only:
        myplot.hide_colorbar([field])
        if picture_only:
            myplot.hide_axes()
    if colorbar_label:
        print('''Code up an automatic solution for this''')
        #Fix the colorbar label from All_density
        myplot.set_colorbar_label(field,colorbar_label)

    if npixels:
        myplot.set_buff_size(npixels)
    if colour_map:
        myplot.set_cmap(field,cmap=colour_map)
    if limits[0] or limits[1]:
        if verbose:
            print("Setting limits",limits)
        myplot.set_zlim(field,limits[0],limits[1])
        plotname+='_limit'

    if small_figure:
        #Set the figure size to something more reasonable
        myplot.set_figure_size(4)

    #Return whatever is needed
    if saveit:
        plotname=ytutils.addendum(plotname,addendum,pdf=pdf)
        if output_folder:
            plotname='./'+output_folder+'/'+plotname
        if verbose:
            print("Saving as",plotname)
        myplot.save(plotname,mpl_kwargs={'bbox_inches':'tight'})
        limits=[None,None]
        if showit:
            return myplot
    else:
        if showit:
            return myplot
        else:
            return myplot,plotname,center,myplot.show_colorbar([field])


def multiplot(outputs,fields,folder='.',field_units={},zoom=100,limits={},
              unit='kpc',preloaded=None,addendum=None,output_folder='.',saveit=False,
              add_time=[],bubbles=[],bubble_levels=[-4],annotation=None,time_bubble=False,fontsize=16,annotate_jet=False,sinkfile=None,jetargs={},t_decay=10,jet_weight=[],annotate_clumps=[]):

    plt.rcParams.update({'font.size': fontsize})

    #Check inputs
    if len(add_time)==0:
        add_time=np.zeros(len(fields))
    elif len(add_time)<len(fields):
        print("add_time array is too short for the number of fields")
        return

    if len(bubbles)==0:
        bubbles=np.zeros(len(fields))
    elif len(bubbles)<len(fields):
        print("bubble array is too short for the number of fields")
        return
    elif len(bubbles)>len(fields):
        print("Bubble array longer than the list of fields. Discarding final entries")

    if len(jet_weight)==0:
        jet_weight=np.zeros(len(fields))
    elif len(jet_weight)<len(fields):
        print("jet_weight array is too short for the number of fields")
        return
    elif len(jet_weight)>len(fields):
        print("jet_weight array longer than the list of fields. Discarding final entries")

    if len(annotate_clumps)==0:
        annotate_clumps=np.zeros(len(fields))
    elif len(annotate_clumps)<len(fields):
        print("annotate_clumps array is too short for the number of fields")
        return
    elif len(annotate_clumps)>len(fields):
        print("Bubble array longer than the list of fields. Discarding final entries")


    for field in fields:
        if not(field) in limits.keys():
            print("Please set a field limit for",field,"using the limits={} keyword")
            if len(outputs)>1:
                return

    labels={'t_AGN':r'$t_{{\rm AGN}}$',
            'radial_velocity':r'$v_{{\rm radial}}$'}

    #Make figure
    extent=[-1*zoom/2,zoom/2,-1*zoom/2,zoom/2]

    fig, axes, colorbars = get_multi_plot(len(fields), len(outputs), colorbar='horizontal', bw = 3.5, cbar_padding=0.5)
    axes=np.array(axes)

    xray=np.any(['xray' in f.split('_') for f in fields])

    for i,output in enumerate(outputs):
        if preloaded:
            print("Using preloaded image")
            ds=preloaded.data_source.ds
            pic_dens=preloaded
            pic_sum=preloaded
            pic_AGN=preloaded
            pic_ones=preloaded
        else:
            sink=sinks.read(output,folder)[0]
            ds=sink.ds
            sp=ds.sphere(sink.pos,ds.quan(zoom,unit))
            clumps=sp.cut_region("obj['hydro_relevant_clump_id']>0")

            if xray:
                yt.add_xray_emissivity_field(ds, 0.1, 100.0, table_type='cloudy', metallicity=0.3)
            pic_dens=plot(output,folder=folder,ds=ds,zoom=zoom,unit=unit,saveit=False,region=sp,sink=False,center=sink.pos,annotate_jet=annotate_jet,sinkfile=sinkfile,jetargs=jetargs)
            if np.any(jet_weight):
                pic_AGN=plot(output,folder=folder,ds=ds,zoom=zoom,unit=unit,saveit=False,region=sp,sink=False,center=sink.pos,annotate_jet=annotate_jet,sinkfile=sinkfile,jetargs=jetargs,weight_field='hydro_jetscalar')

            pic_sum=plot(output,folder=folder,ds=ds,zoom=zoom,unit=unit,saveit=False,weight_field='ones',region=sp,sink=False,center=sink.pos,kwargs={'method':'sum'})
            pic_ones=plot(output,folder=folder,region=clumps,zoom=zoom,unit=unit,field='ones',weight_field='ones',kwargs={'method':'mip'},log=False,ds=ds)

        data_dens=pic_dens.data_source.to_frb((zoom,unit),resolution)
        if np.any(jet_weight):
            data_AGN=pic_AGN.data_source.to_frb((zoom,unit),resolution)
        t_decay=ds.quan(t_decay,'Myr')

        if np.any(annotate_clumps):
            data_clumps=pic_ones.data_source.to_frb((zoom,unit),resolution/2)
            cps=data_clumps['ones']
            cps[cps>0.5]=1
            cps[cps<=0.5]=0

        if np.any(bubbles):
            data_sum=pic_sum.data_source.to_frb((zoom,unit),resolution/8)
            scalar=data_sum['hydro_jetscalar']
            scalar=scalar/scalar.max()  #renormalise to 1
            if time_bubble:
                bubb=np.log(scalar)*t_decay*-1
            else:
                bubb=np.log10(scalar)


        for j,field in enumerate(fields):

            if type(field)==tuple:
                f=field[-1]
            else:
                f=field

            #Set custom colormaps
            cmap='viridis'
            if f=='temperature':
                cmap='plasma'
            elif f=='radial_velocity':
                cmap='RdBu_r'
            elif f=='hydro_jetscalar':
                cmap='Greens'
            elif f=='t_AGN':
                cmap='Greens_r'
            elif f=='pressure':
                cmap='bone'
            elif f.split('_')[0]=='xray':
                cmap='inferno'
            elif f.split('_')[0]=='star':
                cmap='dusk'

            #Get axis
            ax=axes[i,j]
            if field in ['hydro_jetscalar','t_AGN']:
                data_sum=pic_sum.data_source.to_frb((zoom,'kpc'),resolution)
                if field=='t_AGN':
                    scalar=data_sum['hydro_jetscalar']
                    scalar=scalar/scalar.max()  #renormalise to 1
                    proj=np.log(scalar)*t_decay*-1
                    t=ds.current_time.in_units('Myr').v
                    proj[proj>t]=t
                else:
                    proj=data_sum[field]
                    proj=proj/proj.max()  #renormalise to 1
            elif jet_weight[j]:
                proj=data_AGN[field]
            else:
                proj=data_dens[field]

            if field in field_units.keys():
                proj=proj.in_units(field_units[field])
            else:
                field_units[field]=proj.unit_quantity.units

            if proj.min().v<0:
                norm=colors.SymLogNorm(linthresh=5E2)
                #norm=colors.Normalize()
            else:
                norm=colors.LogNorm()
            plotted=ax.imshow(proj.v,origin='lower',norm=norm,extent=extent,cmap=cmap)
            if bubbles[j]:
                ax.contour(bubb,extent=extent,colors='grey',levels=bubble_levels,linestyles=['-','--',':'])

            if annotate_clumps[j]:
                ax.contour(cps,extent=extent,colors='k',levels=[1],linestyles=['-'],linewidth=1)

            if field in limits.keys():
                field_limits=limits[field]
                plotted.set_clim((field_limits[0],field_limits[1]))
            if add_time[j]:
                utils.add_text_box(ax,string='time = {0} Myr'.format(np.round(ds.current_time.in_units('Myr').v,1)),fontsize=fontsize)

            #Add colorbars
            field_unit_label=field_units[field]
            field_unit_label=str(field_unit_label).replace('**','^')
            if field in labels.keys():
                cbar_label=labels[field]
            else:
                cbar_label=field
            cbar=fig.colorbar(plotted, cax=colorbars[j], orientation='horizontal',label=r"{1}  [$\rm {0} $]".format(field_unit_label,cbar_label))

            #Hide every second axis label to make them easier to see
            if field=='radial_velocity':
                for label in cbar.ax.xaxis.get_ticklabels()[1::2]:
                    label.set_visible(False)

            if annotate_jet:
                if type(sinkfile)==None:
                    print("Please preload and pass a sinkfile using sinkfile= to annotate the jet")
                else:
                    ax=draw_jet(ax,sinkfile=sinkfile,ds=ds,**jetargs)

#Format the plot
    for ax in axes.flatten():
        #Turn off axis labels
        ax.xaxis.set_visible(False)
        ax.yaxis.set_visible(False)

    for ax in axes[:,0]:
        ax.yaxis.set_visible(True)
        ax.set_ylabel('y [kpc]')

    for ax in axes[-1,:]:
        ax.xaxis.set_visible(True)
        ax.set_xlabel('x [kpc]')

    if annotation:
        utils.add_text_box(axes[0],annotation)

    plt.tight_layout()
    plotname='multiplot_o{0}_f{1}'.format(len(outputs),len(fields))
    if annotate_jet:
        if 'length' in jetargs.keys():
            jetlength=jetargs['length']
        else:
            jetlength=10
        plotname+='_jet{0}'.format(jetlength)
    for output in outputs:
        plotname+='_o{0}'.format(str(output).zfill(5))
    if saveit:
        utils.saveas(fig,plotname,addendum=addendum,pdf=True,output_folder=output_folder)
    return fig


def draw_jet(fig,sinkfile,length=10,unit='kpc',colour='w',width=2,linestyle=':',los='z',ds=None,flat=False):
    '''length 10: in [unit], i.e. kpc by default
    color='w
    '''
    try:
        ds=fig.data_source.ds
    except:
        ds=ds
    t=np.argmin(np.abs(sinkfile.getattr('time')-ds.current_time.in_units('yr').v))
    sink=ds.arr((sinkfile.getattr('x')[t],sinkfile.getattr('y')[t],sinkfile.getattr('z')[t]),'pc')
    jet=length/2*ds.arr((sinkfile.getattr('bhspin_x')[t],sinkfile.getattr('bhspin_y')[t],sinkfile.getattr('bhspin_z')[t]),unit)
    if type(fig) in [yt.visualization.plot_window.ProjectionPlot,yt.visualization.plot_window.AxisAlignedSlicePlot]:
        fig.annotate_line(sink-jet,sink+jet, coord_system='data',plot_args={'color':colour,'linewidth':width,'linestyle':linestyle})
    else:
        if los=='z':
            print("Assuming a projection along the z-axis and centred on the sink")
            raise EmptyException
        elif los=='x':
            plot_jet=np.array([-1*jet[1:].v,jet[1:].v])
            fig.plot(plot_jet[:,0],plot_jet[:,1],color=colour,linestyle=linestyle,linewidth=width)
        else:
            plot_jet=np.array([-1*jet[:2].v,jet[:2].v])
            fig.plot(plot_jet[:,0],plot_jet[:,1],color=colour,linestyle=linestyle,linewidth=width)

    return fig

#########################################
# COMPOSITE PLOTTING FUNCTIONS
##########################################

#Make a range of zoom plots for a zoomed out gif
def zoom_gif(output,min_zoom=-1,max_zoom=5,number=300):
    ds=ytutils.load(output)
    for zoom in np.logspace(min_zoom,max_zoom,number):
        plot(output,zoom=zoom,unit='pc',colorbar=False,scale=True,addendum=str(zoom)[0:4])

#Plot that creates a timeseries of a single simulation on a grid
def timeseries(folders,outputs,zoom=2.9,limits=[1E5,1E8],addendum=None,cloud=False):
    ''' multiplot, with each row presenting a simulation set and each column an
    output number'''
    font_size=14
    fig=plt.figure(figsize=(4*len(folders),1.8*len(outputs)))
    grid=AxesGrid(fig,(0.1,0.1,0.9,0.9),
                  nrows_ncols=(len(outputs),len(folders)*2),
                  axes_pad=0.1,
                  label_mode="L",
                  share_all=True,
                  cbar_location="right",
                  cbar_mode="single",
                  cbar_size="3%",
                  cbar_pad="0%" )

    i=0
    for output in outputs:
        for folder in folders:
            print(folder,output)
            ds=ytutils.load(output,folder)
            for los,slice in zip(['z','x'],[False,True]):

                p,tmp,center,colorbar=plot(output,zoom=zoom,slice=slice,pdf=True,folder=folder,los=los,saveit=False,limits=limits,cloud=cloud,plot_r_refine=False,sink_size=2)
                #Do plot modifications (http://yt-project.org/doc/reference/api/generated/yt.visualization.plot_window.ProjectionPlot.html)
                p.set_zlim('density',limits[0],limits[1])
                p.set_font_size(font_size-2)

                #Extract plots and redraw
                myplot=p.plots['density']
                myplot.figure=fig
                myplot.axes=grid[i].axes
                myplot.cax=grid.cbar_axes[0]
                p._setup_plots()

                #Reset ticklabel size
                grid[i].axes.tick_params(axis='both', which='major', labelsize=font_size-2)
                grid[i].axes.axis['bottom'].major_ticklabels.set_rotation(90)

                if len(folders)>1:
                    if los=='z' and output==min(outputs):
                        grid[i].axes.set_title(folder.split('/')[-1],fontsize=font_size+4)
                grid[i].axes.set_xlabel("[pc]",fontsize=font_size-2)
                    #grid[i].axes.xaxis.set_title_coords(1.0,-0.1)

                grid[i].axes.set_ylabel('{0} Myr'.format(round(ds.current_time.in_units('Myr'),2)),fontsize=font_size)
                i+=1

    folders=[folder.split('/')[-1] for folder in folders]
    plotname='timeseries'
    for folder in folders:
        plotname+='_{0}'.format(folder)
    utils.saveas(fig,plotname,addendum=addendum,pdf=True)
    return


