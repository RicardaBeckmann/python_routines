#########################################################
# This file contains functions to extract, write to 
# file and then plot data contained in a sphere around
# the sink for use in cosmological simulations
########################################################
import yt
import ytutils
import utils
import sinks
import numpy as np
import os
import sys
import csv
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

##########################################################
# TOPIC: PLOTTING FUNCTIONS
##########################################################
def plot_timeseries(folders=['.'],ncells=8,addendum=None,rb=[0,0],limits=None,png=False):

    fig,axes=plt.subplots(3,1,sharex=True)

    colours=utils.get_colours(rb[0],rb[1])
    masses=['Mgas','Mstar','Msink']
    linestyles=['--',':','-']
    labels=['$M_{gas}$','$M_*$','$M_{BH}$']

    for i,folder in enumerate(folders):
        colour=colours[i]
        radius=Radius(folder=folder,ncells=ncells)
        xdata=radius.getattr('time')
        for mass,linestyle in zip(masses,linestyles):
            ydata=radius.getattr(mass)
            axes[0].plot(xdata,ydata,color=colour,linestyle=linestyle)
        axes[1].plot(xdata,radius.getattr('T_mw'),color=colour)
        axes[2].plot(xdata,radius.getattr('L_mag'),color=colour)

    legend_lines=[mlines.Line2D([],[],linestyle='-',color=colour,label=folder.split('/')[-1]) for colour,folder in zip(colours,folders)]

    #Set up mass plot
    ax=axes[0]
    ax.set_yscale('log')
    ax.set_ylabel('masses [$M_\odot$]')
    mass_lines=[mlines.Line2D([],[],linestyle=linestyle,color='k',label=label) for linestyle,label in zip(linestyles,labels)]
    ax.legend(loc=0,frameon=False,handles=mass_lines)

    #set up temperature plot
    ax=axes[1]
    ax.set_ylabel('T [K]')
    ax.legend(loc=0,frameon=False,handles=legend_lines)


    #set up angular momentum plot
    ax=axes[2]
    ax.set_ylabel('L [$cm^2/s$]')
    ax.set_yscale('log')

    axes[-1].set_xlabel('time [Myr]')
    if limits:
        axes[-1].set_xlim(limits[0],limits[1])
    plt.tight_layout(h_pad=0.0)

    filename='sphere_timeseries'
    utils.saveas(fig,filename,addendum=addendum,pdf=not(png))

#Plot profiles within the spheres
def plot_profile(time=102,folders=['.'],pc=1E4,rb=[0,0],addendum=None,cummulative=True,nbins=100,ncolumns=2,angular_momentum=False,r_core=None,last=False,fields=None,output=None,r_log=True):
    print("########################")

    if not fields:
        fields=[('gas','cell_mass'),('deposit','stars_mass')]
    linestyles=['--',':','-','-.']
    labels=['$M_{gas}$','$M_*$','$M_{tot}$','$M_{BH}$']

    if angular_momentum:
        print("Also plotting the angular momentum")
        fig,axes=plt.subplots(2,1,sharex=True)
    else:
        fig,ax=plt.subplots(1,1)
        axes=[ax]

    colours=utils.get_colours(rb[0],rb[1])
    r_min=1E-2

    if r_core:
        ax.axvline(r_core,linestyle='-',color='grey',alpha=0.5)

    for i,folder in enumerate(folders):
        print(":::::::::::::::::::::::")
        #Load data
        if not(output):
            if last:
                output=ytutils.find_outputs(folder)[-2]
                print("Using last output")
            else:
                output=ytutils.find_output_at_time(time=time,folder=folder)
        sink=sinks.read(output,folder)[0]
        ytutils.add_filters(sink.ds)
        sphere=sink.ds.sphere(sink.pos,sink.ds.quan(pc,'pc'))
        colour=colours[i]

        #Create and plot mass profiles
        profile=yt.create_profile(sphere,['radius'],fields=fields,weight_field=None,accumulation=cummulative,n_bins=nbins,logs={'radius':r_log})
        if cummulative:
            Msink=profile[fields[0]]*0+sink.ds.quan(sink.mass,'Msun')
        else:
            Msink=profile[fields[0]]*0
            Msink[0]=sink.ds.quan(sink.mass,'Msun')

        Mtot=Msink.copy()
        for field,linestyle in zip(fields,linestyles[:3]):
            field_profile=profile[field].in_units('Msun')
            Mtot+=field_profile
            if cummulative:
                field_profile=[field_profile[0]]+list(field_profile)
                radii=[r_min]+list(profile.x.in_units('pc'))
            else:
                radii=list(profile.x.in_units('pc'))
            axes[0].plot(radii,field_profile,linestyle=linestyle,color=colour)

        if cummulative:
            Mtot=[Mtot[0].in_units('Msun')]+list(Mtot.in_units('Msun'))
            axes[0].axhline(float(sink.ds.quan(sink.mass,'Msun')),color=colour,linestyle='-.',alpha=0.5)
        else:
            Mtot=Mtot.in_units('Msun')
            axes[0].scatter(r_min*1.001,sink.ds.quan(sink.mass,'Msun'),color=colour)

        axes[0].plot(radii,Mtot,linestyle='-',color=colour)

        if angular_momentum:
        #Create and plot angular momentum profile
            field=('gas','angular_momentum_magnitude')
            profile=yt.create_profile(sphere,['radius'],fields=[field],weight_field=None,accumulation=cummulative,n_bins=nbins)
            axes[1].plot(radii[1:],profile[field],color=colour)

    field_lines=[mlines.Line2D([],[],linestyle=linestyle,label=label,color='k') for linestyle,label in zip(linestyles+['-'],labels)]
    field_legend=axes[0].legend(loc=4,handles=field_lines,frameon=False,ncol=1)
    axes[0].add_artist(field_legend)
    if len(folders)>1:
        folder_lines=[mlines.Line2D([],[],linestyle='-',color=colour,label=folder.split('/')[-1]) for colour,folder in zip(colours,folders)]
        folder_legend=axes[0].legend(loc=2,handles=folder_lines,frameon=False,ncol=1)
        axes[0].add_artist(folder_legend)
    if r_log:
        axes[0].set_xscale('log')
    for ax in axes:
        ax.set_yscale('log')

    if cummulative:
        axes[0].set_ylabel('enclosed mass [$M_\odot$]')
    else:
        axes[0].set_ylabel('mass [$M_\odot$]')
    axes[-1].set_xlabel('radius from the black hole [pc]')

    #Set limits
    y_lims=axes[0].get_ylim()
    axes[0].set_ylim(1E0,y_lims[1])
    axes[-1].set_xlim(1E-2,pc)

    plt.tight_layout(h_pad=0.1)

    filename='profiles_n{0}_t{1}'.format(len(folders),int(time))
    if angular_momentum:
        filename+='_L'
    if cummulative:
        filename+='sum'
    else:
        filename+='prof'
    utils.saveas(fig,filename,addendum=addendum,pdf=True)

    return profile
############################################################################################################################ 
# TOPIC: Small helper functions
############################################################################################################################ 

def get_filename(ncells=8):
    return  "sink_sphere_{0}cells.csv".format(str(ncells).zfill(5))

############################################################################################################################ 
# TOPIC: Write and read functions
############################################################################################################################ 

#Function to write sink_sphere file
def write_sink_sphere(folder='.',ncells=8,min_output=0,new=False):
    outputs=np.array([int(name[-5:]) for name in os.listdir(folder) if name[:6]=='output'])
    outputs.sort()

    Nmin_data=10
    if len(outputs)>Nmin_data:
        spacing=int(np.round(len(outputs)/Nmin_data))
        outputs=outputs[::spacing]
    if len(outputs)==0:
        print("You are in the WRONG PLACE")
        print("==========================")
        sys.exit(1)

    foldername=folder+"/Data"
    filename=get_filename(ncells=ncells)
    filename=foldername+'/'+filename

    print(filename)
    if not os.path.exists(foldername):
        os.makedirs(foldername)

    if new:
        f=open(filename,'w')
    else:
        f=open(filename,'a')

    writer=csv.writer(f)
    if new:
        #Write units to file as memory aid
        writer.writerow([
                '#',
                '[t] = Myr',
                '[mass] = Msun',
                '[length] = pc',
                'temperature = K',
                'angular momentum L = cm**2/s'
                ])

        #Write attribute names to file
        writer.writerow([
                'output',
                'time',
                'redshift',
                'ncells_v',
                'r_sphere',
                'Msink',
                'Mgas',
                'Mstar',
                'Mdm', #Still add
                'T_mw',  #Mass weighted T
                'L_x',  #Angular momentum
                'L_y',
                'L_z',
                'L_mag'
                ])
        
    for output in outputs:
        if output>min_output:
            #try:
            sink=sinks.read(output,folder=folder)[0]
            #except:
            #    continue

            #Set up data object
            ytutils.add_filters(sink.ds)
            sphere=sink.ds.sphere(sink.pos,sink.ds.index.get_smallest_dx()*ncells)

            #Calculate quantities
            Mgas=sphere['cell_mass'].sum()
            try:
                Mstar=sphere[('stars','particle_mass')].sum()
            except:
                Mstar=sink.ds.quan(0,'Msun')   #If there are no stars, this returns an expection, caught here
            L=sphere.quantities.angular_momentum_vector()
            T_weighted=sphere.quantities.weighted_average_quantity('temperature',weight='cell_mass')

            writer.writerow([
                    output,
                    float(sink.ds.current_time.in_units('Myr')),
                    float(sink.ds.current_redshift),
                    len(sphere['density']),
                    float(sink.ds.index.get_smallest_dx()*ncells),
                    float(sink.mass.in_units('Msun')),
                    float(Mgas.in_units('Msun')),
                    float(Mstar.in_units('Msun')),
                    0,     #Code up DM mass within sphere   
                    float(T_weighted.in_units('K')),
                    float(L[0]),
                    float(L[1]),
                    float(L[2]),
                    float(np.linalg.norm(L))
                    ])
        else:
            print("Skipping",output)
                        
    return

#################################################
# TOPIC: Objects
#################################################
####Timeseries for a particular radius
class Radius(object):
    def __init__(self,ncells=8,folder='.'):

        self.ncells=ncells
        foldername=folder+"/Data"
        filename=get_filename(ncells=ncells)
        filename=foldername+'/'+filename
        print("Opening",filename)

        outputs=np.array([int(name[-5:]) for name in os.listdir(folder) if name[:6]=='output'])
        outputs.sort()
        Nmin_data=10
        if len(outputs)>Nmin_data:
            spacing=int(np.round(len(outputs)/Nmin_data))
            outputs=outputs[::spacing]

        try:
            with open(filename) as f:
                reader=csv.reader(f)
                rows=[row for row in reader]
                keys=rows[1]
        except:
            print("Writing new one")
            write_sink_sphere(folder=folder,ncells=ncells,new=True)
            with open(filename) as f:
                reader=csv.reader(f)
                rows=[row for row in reader]
                keys=rows[1]

        self.data=[Sphere(keys,row) for row in rows[2:]]
        if self.data[-1].output<outputs.max():
            write_sink_sphere(folder=folder,min_output=self.data[-1].output,new=False)
            self.__init__(folder=folder,ncells=ncells)
            
    def getattr(self,name):
        data_loc=np.array([getattr(datum,name) for datum in self.data])
        return data_loc

#Data for sphere at particular point in time => collect them  together under Radius
class Sphere(object):
    def __init__(self,keys,row):
        for i,key in enumerate(keys):
            setattr(self,key,float(row[i]))


        for key in ['output','ncells_v']:
            setattr(self,key,int(getattr(self,key)))
