from IPython.display import Image
from IPython.display import HTML
import matplotlib as mpl
if __name__=="__main__":
        mpl.use('Agg') #makes sure matplotlib doesn't look for a display when plotting    
import matplotlib.pyplot as plt

#plt.rcParams['figure.figsize'] = [16,8]
#font = {'family' : 'normal',
#        'weight' : 'normal',
#        'size'   : 22}
#mpl.rc('font', **font)
#plt.style.use('ggplot')

def h_lyrics(date='.'):
    ''' Link to the data folders on horizon for the lyrics project'''
    foldername='/Users/ricardabeckmann/horizon/lyrics/jupyer/'
    return foldername+date

def show_image(filename,foldername,width=500):
    '''A function to quickly show an image in an ipython notebook'''
    return display(Image(foldername+'/'+filename,width='{0}px'.format(width)))

def image_grid(filenames,foldername,width=400):
    ''' width: opt, def 400, given in px'''
    table="<table width='80%' border='-1'> <tr> "
    for filename in filenames:
        image="<img src='./{0}/{1}' alt='' align='left' style='width: {2}px;'/>".format(foldername,filename,width)
        table+=image
    table+="</table>"
    #print(table)
    return HTML(table)

