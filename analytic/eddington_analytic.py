import numpy as np

#Parameters

variable=0         #final mass=0 or time=1 or alpha=2 or initial mass=3
M_init=5046006.58782            #in solar masses
alpha=0.0073024998             #percentage eddington accretion

t_fin=6.0E8           #in years
M_fin=1.0E9          #in solar masses

#Constants

c=3E8
sigma=6.6525E-29
e_r=0.1
G=6.67E-11
m_p=1.167E-27
seconds=365*24*60*60

C=alpha*4*np.pi*G*m_p/(e_r*sigma*c)

#Calculation

if(variable==0):
    final=M_init*np.exp(C*t_fin*seconds*alpha)
    print "After ",'%0.2e' %t_fin," years a ",'%0.2e' %M_init," BH has grown to ",'%0.2e' %final
elif(variable==1):
    final=np.log(M_fin/M_init)/C/seconds/alpha
    print "It takes ",'%0.2e' %final," years to grow the BH from ",'%0.2e' %M_init," to ",'%0.2e' %M_fin
elif(variable==2):
    final=np.log(M_fin/M_init)/C/t_fin/seconds
    print "To grow a",'%0.2e' %M_init,"BH into a",'%0.2e' %M_fin,"BH in",'%0.2e' %t_fin,"years, one needs alpha=",final
elif(variable==3):
    final=M_fin*np.exp(-C*t_fin*seconds*alpha)
    print "After ",'%0.2e' %t_fin," years a ",'%0.2e' %final," BH has grown to ",'%0.2e' %M_fin

