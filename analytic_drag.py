import numpy as np
from astropy.constants import G
from astropy import units as u
from scipy import integrate
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib as mpl

G=G.cgs.value
Msun=u.Msun.cgs.scale
RMAX=1000

font_size=16
mpl.rcParams.update({'font.size': font_size})

colours_glob=['#fff7fb','#ece7f2','#d0d1e6','#a6bddb','#74a9cf','#3690c0','#0570b0','#034e7b'][::-1]

########################
#Plotting fuction


def plot_Idrag(coulombs,colours=colours_glob):
    if not(type(coulombs)==list):
        coulombs=[coulombs]
    coulombs=list(coulombs)
    coulombs.sort()
    coulombs=coulombs[::-1]
    fig,ax=plt.subplots(1,1)
    machs=np.linspace(0,10,1000)
    ax.axvline(1.0,linestyle=':',color='k')
    for i,coulomb in enumerate(coulombs):
        for interpolation in [True]:
            if interpolation:
                linestyle='-'
                linewidth=2
            else:
                linestyle='--'
                linedwith=1
            Idrag=[Fdrag_analytic(mach,coulomb,interpolation=interpolation) for mach in machs]
            ax.plot(machs,Idrag,color=colours[i],label="$\log(\Lambda)=${0:2.0f}".format(coulomb),linewidth=2,linestyle=linestyle)

    ax.legend(frameon=False)
    ax.set_xlabel(r"$\mathcal{M}_\infty$")
    ax.set_ylabel(r"$F^D$  $[4 \pi \rho_{\infty} (GM_{BH})^2/c_{s,\infty}^2]$")
    plotname='Idrag.pdf'
    fig.savefig(plotname)
        

def plot_alpha_profile(range_R=[0,2],mach=0.9,ss=False):
    plt.clf()
    for s in -1*np.logspace(-3,2,10):
        data_R=np.linspace(range_R[0],range_R[1],1000)
        data_alpha=[alpha(R=R,mach=mach,s=s,ss=ss) for R in data_R]
        plt.plot(data_R,data_alpha,label="s = {0}".format(round(s,2)))
    plt.axhline(1.0,linestyle='--',color='grey')
    plt.legend()
    plt.ylabel(r'$\alpha$')
    plt.xlabel(r"R [$R^B$]")
    plt.title('Mach number = {0}'.format(mach))
    plt.yscale('log')
    plotname='Density_alpha_M{0}'.format(mach)
    print("Saving as", plotname)
    savefig(plotname,ss)

def plot_alpha_colormap(machs,dR=2,dS=2,ss=False,pdf=False):
    plt.clf()

    fig,axes=plt.subplots(1,len(machs),sharey=True,figsize=(14,4))

    vmin=1E-1
    vmax=1E2
    for mach,ax in zip(machs,axes):
        data_S=np.linspace(-1*(mach+dS),dS,500)
        data_R=np.linspace(-1*dR,dR,500)
        S,R=np.meshgrid(data_S,data_R)
        data_alpha=1/(S**2+R**2*(1-mach**2))
        for i,s in enumerate(data_S):
            for j,r in enumerate(data_R):
                data_alpha[j,i]=alpha(s=s,R=r,mach=mach,ss=ss)
        data_alpha=data_alpha+vmin  #data_alpha[data_alpha>0].min()    #add the smallest existing value to fill in the rest
        mesh=ax.pcolormesh(S,R,data_alpha,norm=colors.LogNorm(vmax=vmax,vmin=vmin),cmap='PuBu_r',zorder=-20)#,norm=colors.LogNorm(vmax=data_alpha.max()),cmap='PuBu_r',vmin=data_alpha[data_alpha>0].min())
        #mesh.set_rasterized(True)
        ax.set_rasterization_zorder(-10)
        ax.contour(S,R,data_alpha,[1.0],colors='w',linewidth=3)
        if ss:
            trajectory=[data_S.min(),0.0]
        else:
            trajectory=[-mach,0.0]
        ax.plot(trajectory,[0.0,0.0],color='k',linestyle=':',linewidth=3)
        ax.scatter(trajectory[1],[0.0],color='gold',marker='>',s=200,linewidth=1)
        ax.set_xlabel(r"s [$1/t*c_\infty$]")
        ax.text(0.1,0.1,r"$\mathcal{{M}}_\infty = {0} $".format(mach),horizontalalignment='left',verticalalignment='center', transform=ax.transAxes,fontdict={'color':'w','fontsize':24})

    fig.subplots_adjust(right=0.87,bottom=0.2,left=0.07)
    cbar_ax = fig.add_axes([0.9, 0.15, 0.01, 0.7])
    fig.colorbar(mesh, cax=cbar_ax, label=r"$\alpha$ [$c_\infty*t/R^B$]")  #,ax=axes.ravel().tolist(),label=r"$\alpha$ [$c_\infty*t/(R_{bondi})$]")
    axes[0].set_ylabel(r"R [$1/t*c_\infty$]")
    
    #plt.tight_layout()
    plotname='Overdensity_n{0}'.format(len(machs))
    savefig(plotname,ss,pdf=pdf)

def plot_Fdrag(range_M=[0,2],addendum=None,ss=True):
    plt.clf()
    data_M=np.linspace(range_M[0],range_M[1],30)
    Fdrag=[]
    data_S=np.linspace(-100,100,500)
    ds=data_S[1]-data_S[0]
    print("Step",ds)
    for mach in data_M:
        data_INT=np.array([integrate_alpha(mach=mach,s=s,ss=ss) for s in data_S])*(ds)
        data_y=np.array(data_INT)*ds/data_S**2*(-1*data_S/np.abs(data_S))
        Fdrag.append(data_y.sum())

    plt.plot(data_M,Fdrag,label='numerical')
    for coulomb in [2.0,3.2,4,6,8,10]:
        Fdrag_ana=np.array([Fdrag_analytic(M,r_min=ds,r_max=RMAX,coulomb=coulomb) for M in data_M])

        plt.plot(data_M,Fdrag_ana,label='analytic {0}'.format(coulomb),linestyle='--')
    plt.legend()
    #plt.yscale('symlog')
    plt.xlabel('Mach number M')
    plt.ylabel('Fdrag [4pi*rho0*(G Mbh)**2/cs**2]')
    plotname='Fdrag'
    savefig(plotname,ss,addendum)

def plot_overds(Fdrag=True,machs=[1.13,1.35,0.97,0.75,0.7,0.0],addendum=None,ss=False):
    plt.clf()
    dS=2
    if ss:
        dS=50
    for mach in machs:
        data_S=np.linspace(-1*(mach+dS),dS,1000)
        data_INT=np.array([integrate_alpha(mach,s,ss) for s in data_S])
        data_INT[data_INT<0]=0   #remove negative values
        if Fdrag:
            data_y=np.array(data_INT)/(data_S**2)*(-1*data_S/np.abs(data_S))
            ylabel='dFdrag/ds [(G*Mbh*rho0*R_bondi)/(t*c_s)]'
        else:
            data_y=data_INT
            ylabel='dEta/ds [R_bondi/(t*c_s)**3]'

        plt.plot(data_S,data_y,label="M = {0}".format(mach))
    plt.axvline(0,linestyle='-',color='grey',linewidth=2)

    plt.xlabel('S [1/t*c_s]')
    if not(Fdrag):
        legend_loc=2
    else:
        legend_loc=3
    plt.legend(loc=legend_loc,frameon=False)
    plt.ylabel(ylabel)
    if Fdrag:
        plt.yscale('symlog')
    if ss:
        plt.title('Steady state')
    else:
        plt.title('Time variant')

    if Fdrag:
        plotname="dFdrag_ds"
    else:
        plotname="dM_ds"
    savefig(plotname,ss,addendum)


#########################
#Integrators and similar

def integrate_alpha(mach,s=-1,ss=False):
    if ss:
        upper=2
    else:
        upper=2
    integral=integrate.quad(alpha,0,upper,args=(mach,s,True,ss))[0]
    if integral > 0:    #everything else is unphysical
        return integral
    else:
        return 0

#########################
# Samples

def chapon():
    return [1.13,1.35,0.97,0.75,0.7]

def ostriker():
    return [0.1,0.5,0.9,1.01,1.5,2,10]

def savefig(plotname,ss,addendum=None,pdf=True):
    if ss:
        plotname+='_steady'
    else:
        plotname+='_time'
    if addendum:
        plotname+='_{0}'.format(addendum)
    if pdf:
        plotname+='.pdf'
    else:
        plotname+='.png'
    print("Saving as",plotname)
    plt.savefig(plotname)

#########################
# Analytic functions

def alpha(R,mach=0.9,s=-1,integrate=False,ss=True):
    '''s : [R_bondi]
       R : [R_bondi]'''
    z=s+mach
    alpha=1/(s**2+R**2*(1-mach**2))**0.5
    if not(ss):
        if (R**2+z**2)<1:
            alpha=alpha
        elif mach > 1 and s/np.abs(R) < -1*(mach**2-1)**0.5 and (R**2+z**2) > 1 and z > 1/mach:
            alpha=2*alpha
        else:
            alpha=0
    else:
        if mach > 1: 
            if s/np.abs(R) < -1*(mach**2-1)**0.5:
                alpha=2*alpha
            else:
                alpha=0
    if integrate:
        alpha=alpha*R
    return alpha

    
def Fdrag_analytic(M,coulomb=3.2,interpolation=False):   #
    coulomb=coulomb#np.log(r_max/r_min)

    if interpolation:
        lower=0.97
        upper=1.03
        if M==0:
            f=None
        elif M<lower:
            f=(0.5 * np.log((1+M)/(1-M))-M)/M**2
        elif M>upper:
            f=(0.5*np.log(M**2-1)+coulomb)/M**2
        else:
            f_sub=(0.5*np.log((1+lower)/(1-lower))-lower)/lower**2
            f_sup=(0.5*np.log(upper**2-1)+coulomb)/upper**2
            f=f_sub+(f_sup-f_sub)/(upper-lower)*(M-lower)  #Linear interpolation
    else:
        if M==0:
            f=None
        elif M<1:
            f=(0.5 * np.log((1+M)/(1-M))-M)/M**2
        elif M>1:
            f=(0.5*np.log(M**2-1)+coulomb)/M**2
        else:
            f=None
    return f
