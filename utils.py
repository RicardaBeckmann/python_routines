##############################################
# This file contains a range of utilities not
# based on the yt package
#
#
###############################################
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import csv
import os
import sys
import glob
from scipy import optimize
import matplotlib.collections as mcoll
from matplotlib.lines import Line2D
import astropy.units as u
from astropy.cosmology import WMAP7

#import seaborn as sns

datafolder='Data'

###############
# TOPIC: PLOTTING
###############
#Make lines colorcouded by value
def colorline(x, y, z=None, cmap='copper', norm=plt.Normalize(0.0, 1.0),
        linewidth=3, alpha=1.0, ax=None):
    """
    http://nbviewer.ipython.org/github/dpsanders/matplotlib-examples/blob/master/colorline.ipynb
    http://matplotlib.org/examples/pylab_examples/multicolored_line.html
    Plot a colored line with coordinates x and y
    Optionally specify colors in the array z
    Optionally specify a colormap, a norm function and a line width
    """
    # Default colors equally spaced on [0,1]:
    if z is None:
        z = np.linspace(0.0, 1.0, len(x))

    # Special case if a single number:
    # to check for numerical input -- this is a hack
    if not hasattr(z, "__iter__"):
        z = np.array([z])

    z = np.asarray(z)
    segments = make_segments(x, y)
    lc = mcoll.LineCollection(segments, array=z, cmap=cmap, norm=norm,
                              linewidth=linewidth, alpha=alpha)
    if not ax:
        ax = plt.gca()
    ax.add_collection(lc)

    return lc

def make_segments(x, y):
    """
    Create list of line segments from x and y coordinates, in the correct format
    for LineCollection: an array of the form numlines x (points per line) x 2 (x
    and y) array
    """

    points = np.array([x, y]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)
    return segments


#Fits and returns values for plotting  a powerlaw for x_in, y_in
def fit_power_law_log(x_in,y_in,x_plot,lin=True):
    x_in=np.log10(x_in)
    y_in=np.log10(y_in)
    fitfunc=lambda p, x:p[0] + (x)*p[1]
    errfunc = lambda p, x, y: (y - fitfunc(p, x))
    pinit = [10.0, -0.2]
    power,tmp = optimize.leastsq(func=errfunc, x0=pinit,args=(x_in,y_in))
    return(10**fitfunc(power,np.log10(x_plot)),power)

def fit_straight_line(x_in,y_in,x_plot):
    fitfunc=lambda p,x: p[0]+(x)*p[1]
    errfunc = lambda p, x, y: (y - fitfunc(p, x))
    pinit=[0,1]
    power,tmp = optimize.leastsq(func=errfunc, x0=pinit,args=(x_in,y_in))
    return(fitfunc(power,x_plot),power)

def gaussian(x, mu, sig):
    print("mu",mu)
    print("sigma",sig)
    return 1./(np.sqrt(2.*np.pi)*sig)*np.exp(-np.power((x - mu)/sig, 2.)/2)

def get_plot_bins(bins,log=False,twoD=False):
    if twoD:
        if log:
            return 10**((np.log10(bins[1:,1:])+np.log10(bins[:-1,:-1]))/2)
        else:
            return (bins[1:,1:]+bins[:-1,:-1])/2
    else:
        if log:
            return 10**((np.log10(bins[1:])+np.log10(bins[:-1]))/2)
        else:
            return (bins[1:]+bins[:-1])/2

def plot_contours(ax,x,y,z=None,nbins=20,colours=['w'],histogram=True,
                  xlim=[None,None],ylim=[None,None],zlim=[None,None],contour=False,logx=False,logy=False,levels=None,log_count=False,average=False,normed=False,cmap='viridis',filled_only=True,linestyles=['-'],nbins_x=None,nbins_y=None,uniform=False,label=None,alpha=1,fill_contours=False,scaled=False):
#Normed: Normalised to the total count
#Scaled: Scaled to maximum value so projected onto range 0-1

    if not(contour) and not(histogram):
        print("Please set either histogram or contour to True")
        return

    if not(nbins_x):
        nbins_x=nbins
    if not(nbins_y):
        nbins_y=nbins

    #print("#x bins",nbins_x)
    #print("#y bins",nbins_y)
    if logx:
        if np.any(xlim):
            xbins=np.logspace(np.log10(xlim[0]),np.log10(xlim[1]),nbins_x)
        else:
            xbins=np.logspace(np.log10(x).min()*0.9,np.log10(x).max()*1.1,nbins_x)
    else:
        if np.any(xlim):
            xbins=np.linspace(xlim[0],xlim[1],nbins_x)
        else:
            xbins=np.linspace(x.min()*0.9,x.max()*1.1,nbins_x)
    if logy:
        if np.any(ylim):
            ybins=np.logspace(np.log10(ylim[0]),np.log10(ylim[1]),nbins_y)
        else:
            ybins=np.logspace(np.log10(y).min()*0.9,np.log10(y).max()*1.1,nbins_y)
    else:
        if np.any(ylim):
            ybins=np.linspace(ylim[0],ylim[1],nbins_y)
        else:
            ybins=np.linspace(y.min()*0.9,y.max()*1.1,nbins_y)
    try:
        z[0]
    except:
        z=np.ones(len(x))


    H, xedges, yedges = np.histogram2d(x, y, weights=z,bins=(xbins, ybins))
    N, xedges, yedges = np.histogram2d(x, y, bins=(xbins, ybins))

    if filled_only:
        H[N==0]=np.nan

    if average:
        H=H/N
    if normed:
        H=H/len(x)
    if scaled:
        H=H/np.nanmax(H)

    H = H.T # DO NOT REMOVE

    if not(type(zlim[0])==type(None)):
        H[H<zlim[0]]=zlim[0]
    if zlim[1]:
        H[H>zlim[1]]=zlim[1]

    if uniform:
        H[H>0]=1


    if log_count:
        if H.min()<0:
            norm=mpl.colors.SymLogNorm(linthresh=1, linscale=1,
                       vmin=H[~np.isnan(H)].min(), vmax=H[~np.isnan(H)].max())
        else:

            norm=mpl.colors.LogNorm(vmin=H[H>0].min(), vmax=H[~np.isnan(H)].max())
    else:
        norm=mpl.colors.Normalize(vmin=H[H>0].min(),vmax=H[~np.isnan(H)].max())
    #Normalise the number count
    #H=H/len(y)

    X, Y = np.meshgrid(xedges, yedges)
    if histogram:
        grid=ax.pcolor(X,Y,H,cmap=cmap,norm=norm,alpha=alpha,label=label)#,edgecolors='',label=label)
    else:
        grid=None
    if contour:
        H[np.isnan(H)]=0
        if fill_contours:
            contours=ax.contourf(X[:-1,-1:-1],Y[:-1,:-1],H,levels=[1E-10]+levels,colors=colours,alpha=alpha)  #Level keyword does not work with integers
        else:
            contours=ax.contour(get_plot_bins(X,logx,twoD=True),get_plot_bins(Y,logy,twoD=True),H,levels=levels,colors=colours,linestyles=linestyles,alpha=alpha)  #Level keyword does not work with integers

            #10**((np.log10(Y[:-1,:-1])+np.log10(Y[1:,1:]))/2)
    else:
        contours=None
    if logy:
        ax.set_yscale('log')
    if logx:
        ax.set_xscale('log')
    return ax,grid,contours,xbins,ybins

class AnchoredHScaleBar(mpl.offsetbox.AnchoredOffsetbox):
    """ size: length of bar in data units
        sep: distance between bar and label """


    """ To use:
    ob = AnchoredHScaleBar(size=3, label="3 units", loc=4, frameon=True,
                       pad=0.6,sep=4,color="crimson")
    ax.add_artist(ob)
    """
    def __init__(self, size=1, label="", loc=2, ax=None,
                 pad=0.4, borderpad=0.5, ppad = 0, sep=2, prop=None, colour='w',
                 frameon=True, **kwargs):
        if not ax:
            ax = plt.gca()
        trans = ax.get_xaxis_transform()
        size_bar = mpl.offsetbox.AuxTransformBox(trans)
        line = Line2D([0,size],[0,0],color=colour, **kwargs)
        size_bar.add_artist(line)
        txt = mpl.offsetbox.TextArea(label, minimumdescent=False,textprops={'color':colour})
        self.vpac = mpl.offsetbox.VPacker(children=[size_bar,txt],
                                 align="center", pad=ppad, sep=sep)
        mpl.offsetbox.AnchoredOffsetbox.__init__(self, loc, pad=pad,
                 borderpad=borderpad, child=self.vpac, prop=prop, frameon=frameon)


def shorten_data(data,nsmooth=1,nsample=1):
    ''' This function is used to smooth or downsample data for plotting'''
    if nsmooth>1:
        return smooth(data,nsmooth)
    elif nsample>1:
        return sample(data,nsample)
    else:
        return np.array(data)

def smooth(data,nsmooth):
    '''Smooth data by averaging over nsmooth values'''
    if nsmooth==1:
        return np.array(data)
    data=data[:(len(data)//nsmooth)*nsmooth]   #cut to multiple of nsmooth for smoothing
    smoothed=[data[0]]+list(np.mean(data.reshape(-1,nsmooth),axis=1))     #average over nsmooth values
    return np.array(smoothed)

def sample(data,nsample):
    '''Sample the data at regular intervals'''
    if nsample==1:
        return np.array(data)
    return data[::nsample]

def add_text_box(ax,string,fontsize=14,pos=[0.05,0.95],rotation='horizontal',facecolor='wheat',zorder=99,alpha=0.5):
    props = dict(boxstyle='round', facecolor=facecolor, alpha=alpha)
    ax.text(pos[0],pos[1], string, transform=ax.transAxes, fontsize=fontsize,
            verticalalignment='top', bbox=props,rotation=rotation,zorder=zorder)
    return ax

def add_redshift_axis(ax,plot_redshifts=[10,9,8,7,6,5,4,3,2,1,0.5,0.25,0.1],labels=True):
    ax2=ax.twiny()
    if labels:
        ax2.set_xlabel('redshift')
    xticks=ax2.get_xticks()
    ax2.set_xticks([WMAP7.age(z).value for z in plot_redshifts])
    ax2.set_xlim(ax.get_xlim())
    if labels:
        ax2.set_xticklabels(plot_redshifts)
    else:
        ax2.set_xticklabels([])
    return ax

def time_from_redshift(redshifts):
    return np.array([WMAP7.age(z).value for z in redshifts])


################
# TOPIC: COLOUR
################
def get_colour(current,all_objects,reds=0,blues=0):
    colours=get_colours(reds,blues)
    return colours[current]

def get_greys(grey=0):
    greys=['#cccccc','#969696','#525252']
    if grey>0:
        return greys[:grey]
    else:
        return greys

def get_colours(red=0,blue=0):
    reds=['#fed976','#fd8d3c','#f03b20','#bd0026','#800026'][::-1]
    if red>len(reds):
        print("Not enough REDS, need",red,"have",len(reds))
    if red>0:
        if red==3:
            reds=['#fecc5c','#f03b20','#bd0026'][::-1]   #'#fd8d3c',
        elif red<=2:
            reds=['#fecc5c','#bd0026'][::-1]
    blues=['#c7e9b4','#7fcdbb','#41b6c4','#1d91c0','#225ea8','#0c2c84']
    if blue>len(blues):
        print("Not enough BLUES, need",blue,'have',len(blues))
    if blue>0:
        if blue<=3:
            blues=['#38ddb5','#2b8cbe','#084081']
        elif blue<=5:
            blues=['#38ddb5','#a6bddb','#2b8cbe','#016450','#084081']

    if not(red+blue):
        colours=reds+blues
    else:
        colours=reds[:red]+blues[:blue]
    if len(colours)==1:
        return colours[0]
    else:
        return colours

# 2 colour functions
def ygb_small(i):
    colours=['#d7191c','#fdae61','#2c7bb6','#abd9e9']
    return colours[i]

def ygb_big(i):
    if i>9:
        print("Help, not enough colours!")
    colours=['#a50026','#d73027','#f46d43','#fdae61','#fee090','#e0f3f8','#abd9e9','#74add1','#4575b4','#313695']
    return colours[i]

###############
# TOPIC: Input/Output
################

#Save a figure, whether plt or yt
def saveas(fig,filename,addendum=None,pdf=False,output_folder=None,format='png',dpi=1200):
    if addendum:
        filename+=addendum
    if pdf:
        filename+='.pdf'
    else:
        filename+="."+format
    if output_folder:
        filename='./'+output_folder+'/'+filename

    print("Saving as",filename)

    if hasattr(fig,'savefig'):
        fig.savefig(filename,bbox_inches="tight",dpi=dpi)
    else:
        fig.save(filename)
    print(":::::::::::::::::::::::::::::::::::::::")

#write a set of IDs to file
def ids_tofile(ids,filename):
    with open(filename,'w+') as f:
        f.write("{0}\n".format(int(len(ids))))
        for id in ids:
            f.write("{0}\n".format(int(id)))

def ids_fromfile(filename):
    with open(filename,'r') as file:
        file_ids=[]
        for line in file:
            file_ids.append(line[:-1])

    file_ids=[int(id) for id in file_ids]
    file_ids=file_ids[1:]
    return np.array(file_ids)



#Flatten a list of lists / np arrays
def flatten(l):
    return np.array([item for sublist in l for item in sublist])

#Extract a set of properties from a list of objects
def get_property_objects(objects,attr):
    data=np.array(flatten([getattr(obj,attr) for obj in objects]))
    return data
#
def count_property_objects(objects,attr):
    data=np.array([sum(getattr(obj,attr)) for obj in objects])
    return data


#add outside legend to plot
def legend_outside_plot(ax,location='top',ncol=3,pad=0,fontsize=15):
    if location=='top':
        ax.legend(bbox_to_anchor=(0,1.02+pad,1,0.2), loc="lower left",
                  mode="expand", borderaxespad=0, ncol=ncol,prop={'size': fontsize})
    else:
        print("Location",location,"not implemented. Please code up")
    return ax

###########################
# TOPIC: POST PROCESSING
###########################



#Zip separate coordinate lists into vector array
def make_vector_array(x,y,z=None):
    if not type(z)==None:
        return np.array([[X,Y,Z] for X,Y,Z in zip(x,y,z)])
    else:
        return np.array([[X,Y] for X,Y in zip(x,y)])

#Rotate by a given angle
def rotate_by_angle(theta,vec):
    X=vec[0]*np.cos(theta)+vec[1]*np.sin(theta)
    Y=-1*vec[0]*np.sin(theta)+vec[1]*np.cos(theta)
    return np.array([X,Y])

#Calculate polar angles from a series of vetors
def post_process_polar_angles(vectors):
    '''Post-process continuoud theta and phi angle timeseries
    from a timeseries of vectors. The angle is cummulative'''
    theta=[np.arctan2(vectors[0,1],vectors[0,0])]
    phi=[np.arctan2(vectors[0,0],vectors[0,2])]
    for i, vec in enumerate(vectors[1:]):
        #Find new theta
        xy=rotate_by_angle(theta[-1],[vec[0],vec[1]])
        theta.append(np.arctan2(xy[1],xy[0])+theta[-1])

        #Find new phi
        zx=rotate_by_angle(phi[-1],[vec[2],vec[0]])
        phi.append(np.arctan2(zx[1],zx[0])+phi[-1])

    return np.rad2deg(theta),np.rad2deg(phi)

def project_to_range(values,lower=0,upper=1):
    ''' Reduce values to the given range by subtracting multiples of the difference
    primarily intended to project large continuous angles back into a more restricted
    quadrany'''
    diff=upper-lower
    remainders=values % diff
    return remainders + lower

###########################
# TOPIC: SMALL UTILITIES
###########################
# Create the data folder if necessary
def check_data_folder(folder,datafolder=datafolder):
    if not os.path.exists(folder):
        print("Your are in the WRONG LOCATION")
        print("Folder name",folder)
        print("===================")
        sys.exit(1)
    foldername="{0}/{1}".format(folder,datafolder)
    if not os.path.exists(foldername):
        os.makedirs(foldername)
    return foldername

#Return an empty exception
class EmptyException(Exception):
    pass

#Plot function separators for easier reading of screen output
def _separator():
    print("##########################################")
    return

#Print a number in human legible scientific format
def sci(number,sf=4,printit=False):

    if number < 10 and number > 0.01:
        pretty=np.round(number,sf)
    else:
        if sf==4:
            pretty='{0:2.4e}'.format(float(number))
        elif sf==3:
            pretty='{0:2.3e}'.format(float(number))
        elif sf==2:
            pretty='{0:2.2e}'.format(float(number))
        elif sf==1:
            pretty='{0:2.1e}'.format(float(number))
        elif sf==0:
            pretty='{0:2.0e}'.format(float(number))
        else:
            print("current maximum of sf is 4. Code up more!")
    if printit:
        print(pretty)
    else:
        return(pretty)

#Sort array X by array Y
def sort(X,Y,reverse=False):
    return np.array([x for _,x in sorted(zip(Y,X),reverse=reverse)])

# Find the min and max of a given field in an object (for use as limits in plots)
def min_max_obj(obj,field):
    return (obj[field][obj[field]>0].min(),obj[field].max())
