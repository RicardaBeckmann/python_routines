import numpy as np
import csv
import sinks
import sys
import glob
from utils import  *
import ytutils

from astropy.cosmology import FlatLambdaCDM
from astropy.cosmology import WMAP9


key_lengths={'nstep_fine':1,
             'nstep_coarse':1,
             'aexp':1,
             'time':1,
             'BHmass':1,
             'position':3,
             'vsink':3,
             'dotMbondi':1,
             'dotMedd':1,
             'rg_scale':1,
             'spinmag':1,
             'dpacc':3,
             'lsink':3,
             'f_edd':1,
             'eta_jet':1,
             'eta_disk':1,
             'eps_sink':1,
             'dMsmbh':1,
             'Esave':1,
             'Efeed':1,
             'jgas':3,
             'bhspin':3,
             'levelsink':1,
             'dotMavail':1,
             'velocity':3,
             'nstep':1,
             'ljet':3,
             'dx_min':1,
             'r_refine':1,
             'fdrag':1,
             'rho_mean':1,
             'cs_mean':1,
             'vsink_mag':1,
             'c2sink':1,
             'v2sink':1,
             'sink_fully_zoomed':1,
             'L_AGN':1,
             'dotMacc_in':1,
             'fsink':3,
             'grid_level':1
         }

renaming= {'dotMacc':'dotMbondi',
           'xsink':'position',
           'msink':'BHmass',
           'acc_momentum':'dpacc',
           'jsink':'jgas',
           'velocity':'vsink_mag',
           'nstep':'nstep_fine',
           't':'time',
           'BH':'BHmass',
           'density':'rho_mean',
           'sound':'cs_mean',
           'jet':'ljet',
           'accretion':'dotMbondi',
           'Eddington':'dotMedd',
           'Bondi':'rg_scale',
           'Refinement':'r_refine',
           'Drag':'fdrag',
           'dotMacc_in':'dotMbondi'
}

def write(folder,correct_msun=False,nsink=1,verbose=True):
    check_data_folder(folder)  #Check location and create data folder if necessary
    foldername="{0}/{1}".format(folder,datafolder)
    orig_file=Sinkfile(folder=folder,correct_msun=correct_msun,nsink=nsink)

    filename=foldername+'/sink_info_{0}.csv'.format(nsink)

    step_data=[]
    active_keys=['nstep_fine','nstep_coarse','aexp','time','BHmass','position','vsink','dotMbondi','dotMedd','rg_scale','dpacc','spinmag','f_edd','eta_jet','eta_disk','eps_sink','dMsmbh','Esave','Efeed','jgas','bhspin','levelsink','dotMavail','fsink','c2sink','v2sink','L_AGN','dotMacc_in','grid_level']

    orig_field_location=np.cumsum(orig_file.field_lengths)
    data=[]
    ndata=len(orig_file.data)
    
    for key in active_keys:
        if key in orig_file.fields:
            # Use data from original sinkfile if it exists
            try:
                i=int(np.where(orig_file.fields==key)[0][0])
            except:
                print(key,orig_file.fields,np.where(orig_file.fields==key)[0][0])
            field_length=orig_file.field_lengths[i]
            for j in range(field_length):
                data.append(orig_file.data[:,orig_field_location[i]-field_length+j])
        else:
            # Otherwise fill with zeros
            for j in range(key_lengths[key]):
                data.append(np.zeros(ndata))
            
    #Turn back into numpy array for writing to file
    data=np.stack(data,axis=0).transpose()

    #Replace time for cosmo simulations
    active_keys=np.array(active_keys)
    time_index=int(np.where(active_keys=='time')[0])
    aexp=data[:,int(np.where(active_keys=='aexp')[0])]

    #Recalculate the time for cosmological simulations
    if np.any(aexp<1):
        print("Cosmo run! Recalculating time from expansion.")
        try:
            self.ds=ytutils.find_first_ds(folder,silent=not(verbose))
            cosmo=FlatLambdaCDM(H0=100*ds.hubble_constant,Om0=ds.omega_matter)
        except:
            cosmo=WMAP9
        data[:,time_index]=cosmo.age(1/aexp-1).value*1E9  #In Gyr


    with open(filename,'w') as f:
        writer=csv.writer(f)
        #Write title
        writer.writerow(["## FIELD NUMBER:",len(active_keys)])
        for field in active_keys:
            writer.writerow(['# {0} {1}'.format(field,key_lengths[field])])
        for row in data:
            writer.writerow(row)

    np.save(filename[:-4],data)
    print("Data written")
    return




class Sinkfile(object):
    def __init__(self,folder=".",correct_msun=False,nsink=1,verbose=True):
        try:
            ds=ytutils.find_first_ds(folder,silent=not(verbose))
        except:
            ds=None

        sink_folders=[sink_folder for sink_folder in glob.glob("{0}/SINK*".format(folder))]
        if not(sink_folders):
            print("Warning: you are in the WRONG LOCATION")
            print("============================")
            sys.exit(1)
        raw_data={}
        fields=[]
        field_lengths=[]
        for i in range(len(sink_folders)):
            filename="{0}/sink_{1}.txt".format(sink_folders[i],str(nsink).zfill(5))
            if verbose:
                print("Opening",filename)

            if i==0:
                with open(filename) as f:
                    reader=csv.reader(f,delimiter='\t')
                    reached_data=False

                    while not(reached_data):
                        row=next(reader)
                        if row[0][1]=='#':
                            label=row[0].split()

                            if label[1] in ['mean','relative']:
                                label[1]=label[2]
                            if label[1] in renaming.keys():
                                label[1]=renaming[label[1]]  #Rename from old conventions
                            try:
                                field_lengths.append(int(float(label[2])))
                            except:
                                field_lengths.append(key_lengths[label[1]])
                            #    field_lengths.append(1)
                            fields.append(label[1])
                        else:
                            reached_data=True

                data=np.genfromtxt(filename)
            else:
                data=np.concatenate((data,np.genfromtxt(filename)))

        #Remove duplicate steps
        _,ind=np.unique(data[:,0],return_index=True)
        data=data[np.isin(np.array(list(range(len(data)))),ind)]

        #Sort the array by finestep values (just in case order gets messed up)
        data=data[data[:,0].argsort()]

        #Shorten if it needs shortening
        Nvalues=5000000
        nsample=int(np.ceil(len(data)/Nvalues))
        if nsample > 1:
            print("Sampling every",nsample,"datapoints")
        data=data[::nsample]

        self.data=data
        self.n_fields=len(fields)
        self.fields=np.array(fields)
        self.field_lengths=np.array(field_lengths)


    def getattr(self,attribute):
        attributes=self.data[0].__dict__.keys()
        if not attribute in attributes:
            raise utils.EmptyException("This is not a valid attribute, choose one of {0}".format(attributes))
        else:
            return np.array([getattr(item,attribute) for item in self.data])

