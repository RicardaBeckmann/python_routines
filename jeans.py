#########################################################
# A collection of functions used to calculate n_star, 
# the star formation density of ramses, that is to be 
# specified in the namelist file.
#########################################################
import numpy as np
import utils

pc=3.08E16     #in m


def namelist_isolated(lvl,boxlen,tend_Gyr,m_gas_part,unit_rho_hcc=1,unit_l_kpc=1,njeans=1):
    ''' fuction to calculate the correct units, and various
    quantities needed for the namelist

    lvl: maximum level to be reached
    boxlen: boxlength of the box
    tend_Myr: runtime, in Gyr
    unit_rho_hcc: density unit, in Hcc
    unit_l_kpc: length unit, in kpc'''

    Msun=1.988E33

    unit_rho=unit_rho_hcc*1.6737236E-24
    print('units_density = {0:2.8e}'.format(float(unit_rho)))
    unit_l=unit_l_kpc*3.08567758128E21
    print('units_length = {0:2.8e}'.format(float(unit_l)))
    unit_t=calculate_unit_t(unit_rho)
    print('units_time = {0:2.8e}'.format(float(unit_t)))

    unit_m=unit_rho*(unit_l**3)

    #Calculate final time
    t_fin=tend_Gyr*1E9*(365*24*60*60)/unit_t  #final time in the correct time unit
    print('tend = {0}'.format(np.ceil(t_fin)))

    #Calculate star formation theshold
    n_star=n_jeans(level=lvl,unit_l=unit_l,njeans=njeans,boxlen=boxlen,verbose=False)/unit_rho_hcc

    #Calculate mass_sph
    mass_sph=m_gas_part*Msun/unit_m
    print('mass_sph = {0:2.2e}'.format(mass_sph))
    print('n_star = {0:2.2e}'.format(n_star))


def calculate_unit_t(unit_rho=1.6737236E-24):
    ''' Calculates the time unit, given the length and density unit
    such that G=1
    unit_rho: give in cgs

    Default: unit_rho=Hcc'''
    
    G=6.67259E-8   # in cgs, L^3/(M*T^2)
    unit_t=1/(G*unit_rho)**0.5
    return unit_t

def cl_threshold(dens,z):
    '''
    This function calculates the density_threshold adjusted for expansion
    '''
    aexp=1/(1.0+z)
    print("Calculating the density for aexp=",aexp )
    print("density_threshold",dens*(aexp**3))
    return

def n_100Msun(level,ds=None,unit_l=1,aexp=1.0,Mstar=100.):
    '''
    Parameters
    ------------
    level : int
      maximum refinenement level for star forming cells  
    ds  :  yt dataset object
      A preloaded yt dataset object, loads unit_l and exp automatically.
    unit_l : float
      as given in info file, or returned by unit_l()
    aexp : float, optional
      redshift at which unit_l is taken. Default = 1.0
    njeans : int, optional
      number of cells used to resolve jeans length. Default = 1
    Mstar  : float
      Mass of the star to be produced, in Msun. Default = 100 Msun.
'''
    #variables
    boxlen=1.0
    gamma=5./3
    unit_l=unit_l/100

    #Constants
    M_sol=1.988E30
    H_cc=1.67382E-21

    print("Mass of star:",Mstar,"Msun")
    Mstar=Mstar*M_sol

    if ds:
        unit_l=float(ds.length_unit.in_units('m'))
        aexp=ds.parameters['aexp']

    boxsize=unit_l*boxlen/aexp
    print("boxsize: ",boxsize/pc/1E6,"Mpc")
    dx=boxsize/(2**level)
    print("dx: ",dx/pc,"pc")

    dens=Mstar/(dx**3)/H_cc
    print("n_star:",dens,"Hcc")
    return dens


def boxsize(boxsize):
    '''This function takes a boxsize in Mpc and returns the corresponding unit_l in cm

    Parameters
    ------------
    boxsize : float, in Mpc

    Returns
    -------
    unit_l : float, in cm
        To be used as input for n_star
'''
    unit_l=boxsize*pc*1E6
    return unit_l

def n_jeans(level,ds=None,unit_l=1,aexp=1.0,njeans=1,T=2.0E2,verbose=True,boxlen=1.0):
    '''This function calculates the star formation density in ramses, n_star,
    to be put into the input file

    Parameters
    ------------
    level : int
      maximum refinenement level for star forming cells  
    ds  :  yt dataset object
      A preloaded yt dataset object,loads unit_l and exp automatically. 
    unit_l : float
      as given in info file, or returned by unit_l()
    aexp : float, optional
      redshift at which unit_l is taken. Default = 1.0
    njeans : int, optional
      number of cells used to resolve jeans length. Default = 1
    T : float, optional
      temperature at which jeans support is calculated, Defailt = 2E2
    verbose:  boolean, optional
      controls if values along the way are printed
'''

#variables
    gamma=5./3

#constants in cgs
    G=6.67e-8    
    m_H=1.67e-24   
    k_B=1.38e-16
    mu=0.6
    M_sol=1.99e33
    pc=3.08E18  # in cm

    if ds:
        unit_l=float(ds.length_unit.in_units('cm'))
        aexp=ds.parameters['aexp']
        boxlen=ds.parameters['boxlen']

    boxsize=unit_l*boxlen/aexp
    if(verbose):
        print("boxsize: ",boxsize/pc/1E6,"Mpc")
    delta_x=boxsize/(2**level)
    if(verbose):
        print("dx: ",delta_x/pc,"pc")
        print("jeans lenght=",njeans,"* dx: ",delta_x*njeans/pc,"pc", delta_x*njeans,"m")

    rho_jeans=(np.pi*k_B*T)/((njeans*delta_x)**2*G*m_H*mu)
    
    if(verbose):
        print("rho_jeans [g/cm**3]", rho_jeans)
        print("rho_jeans [H_cc]",rho_jeans/m_H)
        print("m_star [Msun]",utils.sci(rho_jeans*delta_x**3/M_sol,printit=False))

    #if ds:
    #    print("In code units",rho_jeans/ds.density_unit.in_base('cgs').v)
    #mass_sph=rho_jeans*((njeans*delta_x)**3)/M_sol/1E9
    #print("m_sph",mass_sph)
    return(rho_jeans)






